* [Dell inspiron-17-3721](http://www.dell.com/ru/p/inspiron-17-3721/pd?refid=inspiron-17-3721) + [ru_video](https://www.youtube.com/watch?v=7XhdgpElM_g)
  * Screen 17.3"1600x900
  * CPU i3-3217U 1800Mhz (3rd Generation Intel® Core™ i3-3227U Processor (3M Cache, 1.9 GHz))
  * RAM 4096Mb (4GB 1 DIMM (1X4GB) DDR3L 1600Mhz)
  * VIDEO Intel HD Graphics 4000
  * AUDIO Intel Corporation Panther Point HDA
  * HDD WD 500Gb
  * ETH 10/100 RTL8101E/RTL8102E 
  * WiFi Atheros AR9485 Wireless Network Adapter
  * Homepage/desc: [RU](http://www.dell.com/ru/p/inspiron-17-3721/pd?refid=inspiron-17-3721), [UK](https://www.dell.com/en-uk/shop/cty/pdp/spd/inspiron-17-3721)

* Second Monitor: 19"1280x1024 [LG L1952HQ-BF VGA+DVI](https://www.nix.ru/autocatalog/lcd_lg/19-ZHK-monitor-LG-L1952HQ-BF-Flatron-Black-LCD-1280x1024-D-Sub-DVI_58998.html)

* SSD_2.5: 128Gb Plextor M5 Pro - [PX-128M5Pro](https://www.dns-shop.ru/product/a97daf17ed968499/128-gb-ssd-nakopitel-plextor-m5-pro-px-128m5p/)
* HDD_2.5: 1 Tb SATA-II 300 Seagate / Samsung Momentus / Spinpoint < ST1000LM024 / HN-M101MBB> 2.5" 5400rpm 8Mb

* RAM: [SO-DIMM DDR3 Corsair 8GB Kit (2 x 4GB) - PC3-12800 (1600MHz)](http://www.corsair.com/ru/vengeance-8gb-high-performance-sodimm-memory-upgrade-kit-cmsx8gx3m2a1600c9)

* Mice: Logitech [G403 Prodigy Wireless](https://www.logitechg.com/ru-ru/products/gaming-mice/g403-prodigy-wired-gaming-mouse.html)
* Keyboard: Genius [LuxeMate i202](https://www.mvideo.ru/products/klaviatura-provodnaya-genius-luxemate-i202-bl-50035885/specification)
* Gamepad: Microsoft [Xbox 360 Controller for Windows](https://www.microsoft.com/accessories/ru-ru/d/xbox-360-controller-for-windows)

History:

