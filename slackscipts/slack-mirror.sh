#!/bin/bash

# \"SlackWORLD.sh\" - this is script for Slackware32/64 since 14.1
# sources, release, current and someVER any projects

# OUTPUT=slackware${ARCH}
# slackware/ 	sources: patches, SlackBuilds
# slackware32/ 	x86: repositories packages for slackware
# slackware64/ 	x86_64: slackware64

###
##
#
#
# Example, used for all
# ARCH=32 /Full/PATH/slack-mirror.sh -m /Full/PATH/slackmirror -v 14.2 -d all-all
#
# works w/o ARCH: $ARCH autodetect from $(uname -m)
# /Full/PATH/slack-mirror.sh -m /Full/PATH/slackmirror -v 14.2 -d all-all
#
# -v {current,14.1,src}
# for v in 14.1 current src ; do "`dirname $0`/slack-mirror.sh" -m $DIRM -d all -v $v ; done
#
# ./slack-mirror.sh -m /PATH/to/DIRMIR -d all -v `. /etc/os-release ; echo $VERSION`"
# ./slack-mirror.sh -m /home/ftp/slackmirror -v src -d all
#
#
##
###



# only-current-user
if test $(id -u) -eq 0; then
 echo "You must not launch as root, it is INSECURE"
 exit
else
 set -e 
fi


if [ -f /tmp/slack-mirror-error.log ]; then
 cat /tmp/slack-mirror-error.log
 rm -v /tmp/slack-mirror-error.log
fi


# Options
while getopts "m:d:v:" Option
do
  case $Option in
    m ) DIRMIR=${OPTARG}
        ;;
    d ) DISTRO=${OPTARG}
        ;;
    v ) VER=${OPTARG}
        ;;
    *)  echo -e "\nUnknown error while processing options"
        exit 1
        ;;
  esac
done


if [[ ! -d "$DIRMIR" ]]; then
 echo -e "\n> DIRMIR not found - 1st creat $DIRMIR"
 exit 1
elif [[ -z "$VER" ]]; then
 echo -e "\n> VER? Please, enter version: release.version or current"
 exit 1
fi

# Arch-detected
d_arch(){
 x32 (){ ARCH="32" ; DARCH=""	; FARCH="x86" 	 ; iARCH="i486" 	; }
 x64 (){ ARCH="64" ; DARCH="64" ; FARCH="x86_64" ; iARCH="x86_64" 	; }
 if [[ "$ARCH" == "64" ]]; then
  x64
 elif [[ "$ARCH" == "32" ]]; then
  x32
 elif  [ "$VER" == "src" ]; then
  ARCH=""
 else
  [[ "`uname -m`" == "x86_64" ]] && x64 ||x32
 fi
 KEYARCH="-av " ; [ "$ARCH" == "64" ] \
 && KEYARCH+="--exclude=x86" \
 || KEYARCH+="--exclude=x86_64"
}
d_arch

DIRMIR+=${ARCH}
if [[ ! -d $DIRMIR ]]; then
 mkdir $DIRMIR
elif [[ ! -L $DIRMIR ]]; then
 echo -e "\n\nNot found: $DIRMIR\n"
 exit 1
fi


echo -e "\n\n"
echo " ARCH = $ARCH"
echo "FARCH = $FARCH"
echo "iARCH = $iARCH"
echo "DARCH = $DARCH"
echo -e "\n\n"


# rsync-keys
KEY="-r --delete --progress -vh"
KEYDIROFF="--exclude=/*/"
KEYDIR="-r --delete-excluded"


#--include --include=*source/x/x11/** 
KEYFONTS="\
--include=*source/x/x11/src/font/** \
--include=*source/x/fontconfig/** \
--include=*source/l/freetype/** \
--include=*source/l/cairo/** "
# $KEYFONTS
#  
KEYSC="\
--exclude=*source/x/x11/src/*.tar.xz "
#--include=*source/x/x11/src/driver/xf86-video-intel-* \
#--include=*source/xap/mozilla-firefox/** \


KEYLANG="--include=*-dict-ru-* --include=*-dict-en-* --exclude=*-dict-* \
--exclude=*-l10n-ru-*.tar.xz --include=*-l10n-ru* --exclude=*-l10n-* \
--exclude=*-help-ru-*.tar.xz --include=*-help-ru* --exclude=*-help-*"


KEYSRC=" --exclude=*source/**.zip --exclude=*source/**.tar.bz2 "
KEYSRC+="--exclude=*source/**.tar.gz --exclude=*source/**.tar.xz"

KEYSRC2=" --exclude=*.zip --exclude=*.tar.bz2 --exclude=*.tar.gz  --exclude=*.tar.xz "
KEYSRC2+="--exclude=*.jar --exclude=*.dat --exclude=*.cab "
KEYSRC2+="--exclude=*.msi --exclude=*.exe "
KEYSRC2+="--exclude=*.pdf --exclude=*.pdf.gz --exclude=*.dat.gz --exclude=*.dic.gz "
KEYSRC2+="--exclude=*.deb --exclude=*.rpm --exclude=*.xpi "
KEYSRC2+="--exclude=*.vbox-extpack --exclude=*.run --exclude=*.bin "
KEYSRC2+="--exclude=*.iso --exclude=*.iso.gz --exclude=*.img --exclude=*.img.gz "
KEYSRC2+="--exclude=*.ogg --exclude=*.h264 "

KEYSRC3="$KEYSRC2 --include=*diff* --include=*patch* "
KEYSRC3+="--exclude=*.tar --exclude=*.tgz --exclude=*.txz --exclude=*.tbz2 "
KEYSRC3+="--exclude=*.tbz --exclude=*.tar.Z --exclude=*.tar.gta "
KEYSRC3+="--exclude=*.tar.lzma --exclude=*.tar.lz "
KEYSRC3+="--exclude=*.epub --exclude=*.7z --exclude=*.part"


KEYCALLIGRA="--exclude=*calligra* --exclude=*calligra-l10n-*"
KEYXFCE="--exclude=*/xfce/ "
KEY64="--exclude=x86/"
KEYXBMCPI="--exclude=*pi/"
KEYK="--exclude=*/kde/ --exclude=*/kdei/"


# http://wiki.agilialinux.ru/devel/rsync_update
KEYAG="-avrh --delete --progress"

# http://alien.slackbook.org/ktown/current/latest/README
KEYALK32="-av --exclude=x86_64"
KEYALK64="-av --exclude=x86"

# http://mirrors.slackware.com/guidelines/
KEYSLACK="-aPSH --delete"

# http://slackware.no/slack-get.sh
KEYSLACKNO="-av --delete-after --delay-updates --hard-links --links -H --stats"

# http://docs.salixos.org/wiki/How_to_create_a_public_Salix_mirror
KEYSLAX="-a --del"

# http://slackware.ponce.cc/blog/2010/04/13/rsync-repository/
KEYPONCE="--delete-after -avP"


# mirrors-slackware
MIRRORYA="rsync://mirror.yandex.ru"
MIRRORSOU="rsync://slackware.org.uk"
MIRROROSU="rsync://rsync.osuosl.org"
MIRRORLIN="rsync://mirrors.kernel.org"


# mirrors-project
#MIRRORSALIX="rsync://mirror.inode.at/mirror/salix"

MIRRORSBO="rsync://slackbuilds.org"
MIRROR32="$MIRRORALIEN/people/alien/multilib/"
MIRRORMLED="$MIRRORALIEN/people/kikinovak/"


# src-git
get_git0(){
 echo -e "\n\n═════════════════════════════════════════════════"
 echo -e "${MIRRORGIT}\n$DIR\n\n"
}

get_git1 (){
 get_git0
 if [ ! -d "$DIR" ]; then
  git clone $MIRRORGIT $DIR
 else
  cd $DIR
 #git clean -fd && git pull
  git fetch origin && rm -rf * && sleep 2s && git reset --hard origin/master
  cd - &> /dev/null
 fi ;  echo ""
}

get_git2(){
 for MIRRORGIT in $MIRRORGIT1 $MIRRORGIT2 ; do
  get_git0
  if [ ! -d "$DIR/${MIRRORGIT/"$MIRRORGIT0"//}" ]; then
   git clone $MIRRORGIT $DIR/${MIRRORGIT/"$MIRRORGIT0/"//}
  else
   cd ${DIR}/${MIRRORGIT/"$MIRRORGIT0/"//}
  #git clean -fd && git pull
   git fetch origin && rm -rf * && sleep 2s && git reset --hard origin/master
   cd - &> /dev/null
  fi
 done ; echo ""
}


# distro-actions


#####
get_slack() {
MIRROR32="rsync://slackware.uk/slackware/slackware$DARCH-$VER"
MIRROR14="rsync://rsync.osuosl.org/slackware/slackware$DARCH-$VER"
[[ "$ARCH" == "64" ]] && MIRROR=$MIRROR14 || MIRROR=$MIRROR32
DIR="$DIRMIR/${VER}"
if [ "$VER" == "src" ]; then
 #
 ### iso-torrents
 MIRROR="rsync://bear.alienbase.nl/mirrors/slackware/torrents"
 rsync 	$KEYSLACK $KEY $KEYDIR  	$MIRROR/../torrents		$DIR/
 #
 ### slack_sources  #for r in 13.0 13.1 13.37 14.0 14.1 14.2 current ; do
 MIRROR="rsync://slackware.org.uk/slackware/slackware64"
 for r in 14.2 14.1 14.0 13.37 13.1 13.0 ; do
  rsync $KEY $KEYDIR $KEYSRC3 	$MIRROR-$r/source 		$DIR/$r/
  rsync $KEY $KEYDIR $KEYSRC3 	$MIRROR-$r/extra/source/ 	$DIR/$r/extra/
  rsync $KEY $KEYDIR $KEYSRC3 	$MIRROR-$r/pasture/source/ 	$DIR/$r/pasture/
  rsync $KEY $KEYDIR $KEYSRC3 	$MIRROR-$r/testing/source/ 	$DIR/$r/testing/
  if [ "$r" != "current" ]; then
   rsync $KEY $KEYDIR $KEYSRC3 	$MIRROR-$r/patches/source/ $DIR/$r/patches/
   rsync $KEY $KEYDIR $KEYSRC3 	$MIRROR-$r/testing/source/ $DIR/$r/testing/
  fi
 done
elif [ "$VER" == "current" ]; then
 #$KEYCALLIGRA
 rsync $KEYSLACK $KEY $KEYDIR $KEYLANG $KEYSRC --exclude=source/* $KEYSC 	$MIRROR/ 	$DIR
else
 #$KEYCALLIGRA $KEYFONTS $KEYK $KEYXFCE
 rsync $KEYSLACK $KEY $KEYDIR $KEYLANG $KEYSRC --exclude=source/* 		$MIRROR/ 	$DIR
fi
}


###
#
# WillySR
#
#
#####
get_csb(){
if [ "$VER" == "src" ]; then
 # https://github.com/willysr/csb
 DIR="$DIRMIR/src_willysr/csb"
 MIRRORGIT0="https://github.com/CinnamonSlackBuilds"
 MIRRORGIT1="$MIRRORGIT0/csb"
 MIRRORGIT2="$MIRRORGIT0/cinnamonslackbuilds.github.io"
 get_git2
else
 MIRROR="slackware.org.uk::csb"
 DIR="$DIRMIR/${VER}_willysr-csb"
 rsync 	$KEY $KEYDIROFF 	$MIRROR 		$DIR
 rsync 	$KEY $KEYDIR $KEYARCH	$MIRROR/${VER}/$FARCH 	$DIR
fi
}
#
#####
get_msb(){
if [ "$VER" == "src" ]; then
 DIR="$DIRMIR/src_willysr/msb"
 MIRRORGIT0="https://github.com/mateslackbuilds"
 MIRRORGIT1="$MIRRORGIT0/msb"
 MIRRORGIT2="$MIRRORGIT0/mateslackbuilds.github.com"
 get_git2
else
 MIRROR="slackware.org.uk::msb/"
 DIR="$DIRMIR/${VER}_willysr-msb"
 rsync 	$KEY $KEYDIROFF 	$MIRROR 		$DIR/
 rsync 	$KEY $KEYDIR $KEYARCH	$MIRROR/${VER}/ 	$DIR/packages/
fi
}

#
#####
get_willysr(){
#
MIRRORGIT="https://github.com/willysr/SlackHacks/"
DIR="$DIRMIR/src_willysr/SlackHacks" ; get_git1
#
MIRRORGIT="https://github.com/willysr/slackbasics-i18n"
DIR="$DIRMIR/src_willysr/slackbasics-i18n" ; get_git1
}
#
#####



#####
get_salix(){
MIRROR="slackware.org.uk::salix"
DIR="$DIRMIR/${VER}_salix"
if [ "$VER" == "src" ]; then
 rsync $KEYSLAX $KEY $KEYDIROFF 	$MIRROR/ 	$DIR/
 [[ ! -z "`uname -m | grep x86_64`" ]] && x64 ||x32
 for r in 13.37 14.0 14.1 14.2 ; do # +$iARCH/$r/source/: 13.0 13.1
  rsync	$KEY $KEYDIR $KEYLANG $KEYSRC3 --exclude=*.log 	$MIRROR/slkbuild/$r/ 			$DIR/slkbuilds/
  mkdir -p $DIR/sources/$r
  rsync	$KEY $KEYDIR $KEYLANG $KEYSRC3 --exclude=*.log 	$MIRROR/$iARCH/$r/source/ 		$DIR/sources/$r
  [ $r = 14.2 ] && \
  rsync	$KEY $KEYDIR $KEYLANG $KEYSRC3 --exclude=*.log 	$MIRROR/$iARCH/extra-$r/source/ 	$DIR/sources-extra$r
 done

elif [[ "$DISTRO" = "salix_extra" ]]; then
 if [ "$VER" == "14.2" ]; then
  rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIROFF $MIRROR/$iARCH/extra-$VER/ 	$DIR-extra
  rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIR	$MIRROR/$iARCH/extra-$VER/salix $DIR-extra
 else
  echo -e "\n\n\n\n> For this version ($VER) \n> extra_repository not found."
 fi

elif [[ "$DISTRO" = "salix" ]]; then
 #
 # salix.iso
 rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIROFF 	$MIRROR/ 			$DIR/
 rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIR	$MIRROR/$iARCH/$VER/iso/ 	$DIR/iso/
 #
 # salix
 rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIROFF 	$MIRROR/$iARCH/$VER/ 		$DIR/$iARCH/
 rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIR	$MIRROR/$iARCH/$VER/salix 	$DIR/$iARCH/
fi
}
get_salix_extra(){ get_salix; }

##########
#
# AlienNOW
# rsync://alien.slackbook.org/slackware
# rsync://taper.alienbase.nl/mirrors/ -> rsync://bear.alienbase.nl/mirrors/

MIRROR_alien="rsync://bear.alienbase.nl/mirrors"

#####
get_ktown() {
DIR="$DIRMIR/${VER}_alien"
MIRROR="${MIRROR_alien}/alien-kde"
if [ "$VER" == "src" ]; then
 # sources - SlackBuilds
 mkdir -p $DIR/ktown
 rsync 	$KEY $KEYDIROFF			$MIRROR/ 	$DIR/ktown
 rsync 	$KEY $KEYDIR $KEYSRC3 		$MIRROR/source 	$DIR/ktown
else
 # packages
 rsync 	$KEY $KEYDIROFF 		$MIRROR/	${DIR}-ktown/
 rsync 	$KEY $KEYDIR $KEYARCH $KEYLANG 	$MIRROR/$VER/ 	${DIR}-ktown/packages/
fi
}

#####
get_lib32 (){
MIRROR="${MIRROR_alien}/people/alien/multilib"
DIR="$DIRMIR/${VER}_alien"
if [ "$VER" == "src" ]; then
 mkdir -p $DIR/lib32
 rsync $KEY $KEYDIROFF 		$MIRROR/		$DIR/lib32
 rsync $KEY $KEYDIR 		$MIRROR/source 		$DIR/lib32
elif [ "$ARCH" == "64" ]; then
 rsync 	$KEY $KEYDIROFF 	$MIRROR/ 		$DIR-lib32
 rsync	$KEY $KEYDIR 		$MIRROR/$VER/ 	 	$DIR-lib32/multilib
fi
}

#####
get_sbrepos() {
MIRROR="${MIRROR_alien}/people/alien/sbrepos/"
DIR="$DIRMIR/${VER}_alien-sbrepos"
rsync 	$KEY $KEYDIROFF 		$MIRROR 		$DIR
rsync 	$KEY $KEYDIR $KEYLANG $KEYSRC 	$MIRROR/$VER/$FARCH 	$DIR
}

#####
get_sbrepos-restricted() {
MIRROR="${MIRROR_alien}/people/alien/restricted_sbrepos/"
DIR="$DIRMIR/${VER}_alien-sbrepos-restricted"
rsync 	$KEY $KEYDIROFF 		$MIRROR 		$DIR
rsync	$KEY $KEYDIR $KEYLANG $KEYSRC 	$MIRROR/$VER/$FARCH 	$DIR
}

#####
get_alien_src() {
MIRROR="${MIRROR_alien}/people/alien"
DIR="$DIRMIR/src_alien"
rsync 	$KEY $KEYDIROFF 					$MIRROR/ 			$DIR/
rsync 	$KEY $KEYDIR $KEYSRC3 					$MIRROR/archive/ 		$DIR/_archive
rsync 	$KEY $KEYDIR 						$MIRROR/tools/ 			$DIR/_tools
rsync 	$KEY $KEYDIR $KEYLANG $KEYSRC3 --exclude=*/pkg{,64}/ 	$MIRROR/slackbuilds/ 		$DIR/slackbuilds
rsync 	$KEY $KEYDIR $KEYLANG $KEYSRC3 --exclude=*/pkg{,64}/ 	$MIRROR/restricted_slackbuilds/ $DIR/slackbuilds_restricted
}
#####
##########


#####
get_slacky (){
MIRROR="slackware.uk::slacky"
DIR="$DIRMIR/${VER}_slacky"
if [ "$VER" == "src" ]; then
 KEYSRC3_s="$KEYSRC3 --exclude=*.txz --exclude=*.tgz --exclude=*.gtk.sh"
 rsync	$KEY $KEYDIR $KEYLANG ${KEYSRC3_s} 	$MIRROR/ 			$DIR
else
 rsync 	$KEY $KEYDIROFF 			$MIRROR/ 			$DIR
 rsync	$KEY $KEYDIR $KEYLANG $KEYSRC 		$MIRROR/slackware$DARCH-$VER 	$DIR
fi
}


#####
get_alphageek(){
MIRROR="slackware.uk::people/alphageek"
DIR="${DIRMIR}/src_alphageek"
rsync 	$KEY $KEYDIR $KEYLANG $KEYSRC3 	$MIRROR/ 	$DIR
}


#####
get_sbo(){
#
#for v in 11.0 12.0 12.1 12.2 13.0 13.1 13.37 14.0 14.1 ; do $MIRROR/slackbuilds/$v
MIRROR="rsync://slackbuilds.org"
DIR="$DIRMIR/src_sbo/"
mkdir -p $DIR/SBo{,_templates}
rsync	$KEY $KEYDIR 	$MIRROR/slackbuilds/ 	$DIR/SBo
rsync	$KEY $KEYDIR 	$MIRROR/templates/ 	$DIR/SBo_templates
#
# https://slackbuilds.org/cgit/slackbuilds/
MIRRORGIT="git://git.slackbuilds.org/slackbuilds.git"
DIR="$DIRMIR/src_sbo/SBo-master" ; get_git1
#
}

#####
get_ponce(){
MIRROR="rsync://ponce.cc/pub/"
DIR="$DIRMIR/${VER}_ponce"
if [ "$VER" == "src" ]; then
 rsync 	$KEY $KEYDIR $KEYSRC3 	$MIRROR/testing 		$DIR
 # http://cgit.ponce.cc/slackbuilds/
 MIRRORGIT="git://github.com/Ponce/slackbuilds.git"
 DIR="$DIR/sbo-current-unofficial" ; get_git1
else
 #--exclude=old.packages/
 rsync $KEY $KEYDIROFF 		$MIRROR 			$DIR
 rsync $KEY $KEYDIR 		$MIRROR/slackware$DARCH-$VER 	$DIR
fi
}

#####
get_slaxbmc(){
MIRROR="slackware.uk::slaxbmc"
DIR="$DIRMIR/${VER}_slaxbmc"
if [ "$VER" == "src" ]; then
 rsync 	$KEY $KEYLANG $KEYDIR $KEYSRC3 	$MIRRORXBMC/13.37 	$DIR/
 for ver in 14.0 14.1 14.2 ; do
  MIRROR="$MIRRORXBMC/$ver"
  [ ! -d $DIR/$ver ] && mkdir -pv $DIR/$ver
  rsync 	$KEY $KEYLANG $KEYDIR $KEYSRC3 	$MIRROR/slaxbmcsrc-$ver/ 	$DIR/$ver
 done
else
 rsync $KEY $KEYLANG $KEYDIR --exclude=slackware$DARCH/* 	$MIRROR/$VER/slaxbmc$DARCH-$VER/ 	$DIR
fi
}

#####
get_mled(){
if [ "$VER" == "src" ]; then
 #MIRRORGIT="https://github.com/kikinovak/slackware"
 #DIR="$DIRMIR/src_mled"
 #get_git1
 #
 #
 MIRRORGIT0="https://github.com/kikinovak"
 MIRRORGIT1="$MIRRORGIT0/slackware"
 MIRRORGIT2="$MIRRORGIT0/microlinux"
 DIR="$DIRMIR/src_mled"
 get_git2
 #
 #rsync 	$KEY $KEYDIR $KEYSRC3	$MIRROR/desktop-$VER-source 		$DIR/src/
 #rsync 	$KEY $KEYDIR $KEYSRC3	$MIRROR/extras-$VER-source 			$DIR/src/
 #rsync 	$KEY $KEYDIR $KEYSRC3	$MIRROR/server-$VER-source 			$DIR/src/
 #rsync 	$KEY $KEYDIR $KEYSRC3 	$MIRROR/testing-$VER-source 		$DIR/src/

 #rsync 	$KEY $KEYDIR			$MIRROR/Linux-HOWTOs 				$DIR
 #rsync 	$KEY $KEYDIR			$MIRROR/tools 						$DIR
 #rsync 	$KEY $KEYDIR			$MIRROR/template 					$DIR

 #rsync 	$KEY $KEYDIR			$MIRROR/stripslack-$VER/ 			$DIR/source/stripslack
 #rsync 	$KEY $KEYDIR			$MIRROR/recipes 					$DIR/source/
 #rsync 	$KEY $KEYDIR			$MIRROR/repository-layout 			$DIR/source/
else
 MIRROR="slackware.uk::microlinux"
 DIR="$DIRMIR/${VER}_mled"
 rsync $KEY $KEYDIROFF 		$MIRROR 	$DIR
 for MLED in desktop extras server ; do
   rsync $KEY $KEYDIR	$MIRROR/$MLED-$VER-${ARCH}bit/ 	$DIR/$MLED
 done
fi
}

#####
get_boost(){
#
#MIRROR="rsync://download.deepstyle.org.ua/slackware/slackboost${DARCH}-$VER/"
# http://www.slackware.ru/forum/viewtopic.php?p=15349&sid=b00109cd8ade484f9fa391d6c5758730#p15349
#+ftp://195.138.193.10/pub/ #+ftp://91.209.51.10/pub/
#
# http://slackboost.blogspot.com/2014/10/slackware.html
#lrwxrwxrwx             15 2015/12/08 14:01:29 slackboost64_alt -> slackboost64-03
#lrwxrwxrwx             15 2015/12/08 14:01:53 slackboost64_new -> slackboost64-02
#lrwxrwxrwx             15 2015/12/08 14:01:40 slackboost64_old -> slackboost64-01
MIRROR="rsync://91.209.51.10/slackware/slackboost${DARCH}-$VER"
DIR="$DIRMIR/${VER}_boost"
if  [ "$VER" == "src" ]; then
 [ ! -d $DIR ] && mkdir -pv $DIR
 rsync 	$KEY $KEYDIR 		 $MIRROR/../slackboost64-14.1/documentation/ 	$DIR/14.1_docs
 rsync 	$KEY $KEYDIR 		 $MIRROR/../slackboost64-14.2/docs/ 		$DIR/14.2_docs
 rsync 	$KEY $KEYDIR $KEYSRC3 	 $MIRROR/../slackboost64-14.2/sourceboost_alt/ 	$DIR/14.2_alt/
 for v in '14.2' '14.1' ; do
  rsync 	$KEY $KEYDIR $KEYSRC3 	$MIRROR/../slackboost64-$v/sourceboost/ 	$DIR/${v}/
  rsync 	$KEY $KEYDIR 		$MIRROR/../slackboost64-$v/scripts/ 		$DIR/${v}_scripts/
 done
elif  [ "$DISTRO" == "boost_extra" ]; then mkdir -p $DIR-extra
 rsync $KEY $KEYDIR	$MIRROR/slackboost${DARCH}-01/ 		$DIR-extra/slackboost${DARCH}_01-old
 rsync $KEY $KEYDIR	$MIRROR/slackboost${DARCH}-02/ 		$DIR-extra/slackboost${DARCH}_02-new
 rsync $KEY $KEYDIR	$MIRROR/slackboost${DARCH}-03 		$DIR-extra/slackboost${DARCH}_03-alt
 rsync $KEY $KEYDIR	$MIRROR/slackboost${DARCH}_multiver/ 	$DIR-extra/slackboost${DARCH}_00-multiver
else
 rsync $KEY $KEYDIR	$MIRROR/slackboost${DARCH} 		$DIR/
fi
}
get_boost_extra(){ get_boost; }

#####
get_rw(){
MIRROR="slackware.uk::people/rlworkman/"
DIR="$DIRMIR/${VER}_rlworkman/"
#$ rsync -l rsync://slackware.uk/people/rlworkman
#lrwxrwxrwx 	rlworkman -> rworkman
if  [ "$VER" == "src" ]; then
 rsync 	$KEY $KEYDIROFF 		$MIRROR/ 					$DIR
 #for v in 11.0 12.0 12.1 12.2 13.0 13.1 13.37 14.0 14.1 14.2 current ; do    
 # rsync $KEY $KEYDIR $KEYSRC3	$MIRROR/sources/$v 			$DIR
 #done
 #rsync 	$KEY $KEYDIR $KEYSRC3	$MIRROR/14.1/testing/src/ 	$DIR/14.1_testing
 rsync $KEY $KEYDIR $KEYSRC3	$MIRROR/sources 			$DIR
else
 rsync $KEY $KEYDIROFF 	$MIRROR/		$DIR/
 rsync $KEY $KEYDIROFF 	$MIRROR/$VER/		$DIR/packages/
 rsync $KEY $KEYDIR 	$MIRROR/$VER/$iARCH 	$DIR/packages/
fi
}

#####
get_nk(){
MIRRORGIT="https://github.com/slacknk/slackware"
DIR="$DIRMIR/src_nk"
get_git1
}
#####

#####
get_sit(){
MIRRORGIT0="https://github.com/conraid"
MIRRORGIT1="$MIRRORGIT0/SlackBuilds"
MIRRORGIT2="$MIRRORGIT0/OlderSlackBuild"
DIR="$DIRMIR/src_slackers"
get_git2
}
#####
get_phx(){
MIRRORGIT="https://github.com/PhantomX/slackbuilds"
DIR="$DIRMIR/src_phantomx"
get_git1
}
#####
get_sware(){
MIRRORGIT="https://github.com/eviljames/studioware"
DIR="$DIRMIR/src_studioware"
get_git1
}
#####
get_bormant(){
MIRRORGIT="https://github.com/bormant/SB"
DIR="$DIRMIR/src_bormant"
get_git1
}
#####
get_fsleg(){
MIRRORGIT="https://github.com/fsLeg/SlackBuilds"
DIR="$DIRMIR/src_fsleg"
get_git1
}
#####
get_anddt(){
MIRRORGIT="https://github.com/AndDT/SlackBuilds"
DIR="$DIRMIR/src_anddt"
get_git1
}
#####
get_philipvdh(){
MIRRORGIT="https://github.com/philipvdh/slackbuilds"
DIR="$DIRMIR/src_philipvdh"
get_git1
}
#####
get_thorni(){
DIR="$DIRMIR/src_thorni"
MIRRORGIT0="https://github.com/Thorn-Inurcide"
MIRRORGIT1="$MIRRORGIT0/SlackBuilds"
MIRRORGIT2="$MIRRORGIT0/tde-slackbuilds"
get_git2
}
#####
get_ericff(){
for MIRRORGITDIR in \
 slackbuilds infinality-fonts \
 mate mate-gtk3 cinnamon xfce4 enlightenment \
 audio video multilibs scripts \
; do
 MIRRORGIT="https://github.com/ericfernandesferreira/$MIRRORGITDIR"
 DIR="$DIRMIR/src_ericff/$MIRRORGITDIR"
 get_git1
done
}
#####
get_nihilismus(){
MIRRORGIT="https://github.com/nihilismus/bob-infinality-bundle"
DIR="$DIRMIR/src_nihilismus"
get_git1
}
#####
get_ldepandis(){
MIRRORGIT="https://github.com/ldepandis/slackbuilds"
DIR="$DIRMIR/src_ldepandis"
get_git1
}
#####
# Slackware, man и UTF-8
# https://www.linux.org.ru/forum/talks/13409359
get_saahriktu(){
MIRRORGIT0="https://github.com/saahriktu"
MIRRORGIT1="$MIRRORGIT0/modified-slackbuilds"
MIRRORGIT2="$MIRRORGIT0/saahriktu-slackbuilds"
DIR="$DIRMIR/src_saahriktu"
get_git2
}
#####
get_sftp(){
MIRRORGIT="https://github.com/sftp/"
DIR="$DIRMIR/src_sftp/"
for r in slackbuilds slackware_ru slackware_init ; do
 MIRRORGIT+=$r DIR+=$r get_git1
done
}
#####
##
#


# Hello World!
hw(){
echo ""
echo " ═════════════════════════════════════════════════"
echo ""
echo "	$DISTRO - $ARCH - $VER"
echo ""
echo " ═════════════════════════════════════════════════"
echo ""
}
#hw

dv(){
 #reset
 #clear
 echo -e "\n ═════════════════════════════════════════════════\n"
 echo -e " $DISTRO - $ARCH - $VER"
 echo -e "\n ═════════════════════════════════════════════════\n\n\n\n\n\n\n\n"
 #lsusb | while read i; do ... done
 for d in $DISTRO ; do 
  reset
  echo -e "\n\n\n\n$(tput setaf 1)❯$(tput setaf 4)❯$(tput setaf 3)❯ $(tput sgr0)$d - $VER -Begin\n\n"
  #get_$d || echo $d >> /tmp/slack-mirror-error.log
  DISTRO=$d ; get_$DISTRO || exit 1
  echo -e "\n\n$(tput setaf 1)❯$(tput setaf 4)❯$(tput setaf 3)❯ $(tput sgr0)$d - $VER -End\n\n\n"
  sleep 2s
 done
}



##
###
####
 [ "$ARCH" = "64" ] && LIB32="lib32" || LIB32=""
 DR14_1=""
 DR14_1+="slack ktown sbrepos sbrepos-restricted "
 DR14_1+="boost salix mled slacky msb csb ponce slaxbmc rw $LIB32"
 
 DR14_2__1="slack sbrepos msb $LIB32 boost "
 DR14_2__2="csb ktown boost_extra salix_extra salix slacky "
 DR14_2__3="mled ponce rw sbrepos-restricted slaxbmc "
 
 DC="slack ktown sbrepos sbrepos-restricted msb $LIB32 "
 
 DS=""
 DS+="slack alien_src alphageek anddt boost bormant fsleg phx thorni ericff rw "
 DS+="mled salix sbo sit slacky sware philipvdh willysr msb csb slaxbmc saahriktu "
 DS+=""
#DS+="nk "
####
###
##
#


# Distro
if [[ "$DISTRO" = "all" ]]; then
 [[ $VER == "src"   ]] && DISTRO="$DS" dv
 [[ $VER == "14.1"  ]] && DISTRO="$DR14_1" dv
 [[ $VER == "14.2"  ]] && DISTRO="$DR14_2__1" dv
 [[ $VER == current ]] && DISTRO="$DC" dv

elif [[ "$DISTRO" = "all-all" ]]; then
 [[ $VER == "src"   ]] && DISTRO="$DS" dv
 [[ $VER == "14.1"  ]] && DISTRO="$DR14_1" dv
 [[ $VER == "14.2"  ]] && DISTRO="$DR14_2__2 $DR14_2__1 $DR14_2__3" dv
 [[ $VER == current ]] && DISTRO="$DC csb rw ponce " dv

elif [[ "$DISTRO" = "slack" ]]; then
 get_slack
 
elif [[ $VER == "src" ]] && [[ "$DISTRO" = "alien" ]] || [[ "$DISTRO" = "alien_src" ]]; then
 get_alien_src
 get_ktown
 get_lib32

elif [[ "$DISTRO" = "alien" ]]; then
 get_ktown
 get_sbrepos
 get_sbrepos-restricted
 get_lib32 

elif [[ "$DISTRO" = "sbo" ]]; then
 #dv || get_$DISTRO
  get_$DISTRO
elif [[ "$DISTRO" = "ktown" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "sbrepos" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "sbrepos-restricted" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "lib32" ]]; then
 get_$DISTRO

elif [[ "$DISTRO" = "msb" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "csb" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "willysr" ]]; then
 get_$DISTRO

elif [[ "$DISTRO" = "salix" ]] || [[ "$DISTRO" = "salix_extra" ]]; then
 get_salix
elif [[ "$DISTRO" = "boost" ]] || [[ "$DISTRO" = "boost_extra" ]]; then
 get_boost
 
elif [[ "$DISTRO" = "slacky" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "mled" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "slaxbmc" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "ponce" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "phx" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "sit" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "rw" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "sware" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "alphageek" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "bormant" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "anddt" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "fsleg" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "philipvdh" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "thorni" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "ericff" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "nihilismus" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "ldepandis" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "saahriktu" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "sftp" ]]; then
 get_$DISTRO

# SRC-git-BLOCK
elif [[ "$DISTRO" = "nk" ]]; then
 get_$DISTRO
else
 echo "-> and ?.. -d DISTRO?"
 exit 1

fi


# This is The End
C="$(tput setaf 1)❯$(tput setaf 4)❯$(tput setaf 3)❯ $(tput sgr0)"
echo -e "\n\n\n═════════════════════════════════════════════════\n$C FINISH !"



#xterm -e "sudo su -c 'printf 'y' | slackpkg update && slackpkg upgrade-all && read enter'"
  
  
#VER0=$VER
#if [ "$VER" != "current" ]; then
# VER=""
#fi
#VER=$VER0


#wget -r -k -l 7 -p -E -nc http://www.slackers.it/ -P //***FULL*PATH***//repository-mirror/slackware64/
#wget -r -k -l 7 -p -E -nc http://www.slackel.gr/ -P //***FULL*PATH***//repository-mirror/slackware64/
#wget -qO- http://sourceforge.net/projects/dropline-gnome/files/Dropline%20Packages/3.10/x86_64/ | grep '.txz/download' | cut -d\" -f2 | while read uri ; do desiredpath=$(dirname $uri | sed s/sourceforge/downloads.sourceforge/ | sed s/projects/project/ | sed 's/files\///') && wget $desiredpath ; done

 #
 # salix-slackware
 #rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYDIR $KEYSRC $KEYK $KEYLANG $KEYXFCE $MIRROR/$iARCH/slackware-$VER/ $DIR/${iARCH}-slackware/
 #rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYDIR $KEYSRC $KEYK 		   $MIRROR/$iARCH/slackware-$VER/ $DIR/${iARCH}-slackware/

# w/o comments // ChangeLog-mini
#$ tree -L 1
tree_l_1 (){
/
/slackware64 - 16 directories
├── 14.2
├── 14.2_alien-ktown
├── 14.2_alien-lib32
├── 14.2_alien-sbrepos
├── 14.2_alien-sbrepos-restricted
├── 14.2_boost
├── 14.2_boost-extra
├── 14.2_mled
├── 14.2_ponce
├── 14.2_rlworkman
├── 14.2_salix
├── 14.2_salix-extra
├── 14.2_slacky
├── 14.2_slaxbmc
├── 14.2_willysr-csb
└── 14.2_willysr-msb
/
/slackware32 - 15 directories
├── 14.2
├── 14.2_alien-ktown
├── 14.2_alien-sbrepos
├── 14.2_alien-sbrepos-restricted
├── 14.2_boost
├── 14.2_boost-extra
├── 14.2_mled
├── 14.2_ponce
├── 14.2_rlworkman
├── 14.2_salix
├── 14.2_salix-extra
├── 14.2_slacky
├── 14.2_slaxbmc
├── 14.2_willysr-csb
└── 14.2_willysr-msb





	
	
	
	
	
14.2
├── 14.2
├── 14.2_alien-ktown
├── 14.2_alien-lib32
├── 14.2_alien-sbrepos
├── 14.2_alien-sbrepos-restricted
├── 14.2_boost
├── 14.2_mled
├── 14.2_ponce
├── 14.2_rlworkman
├── 14.2_slacky
├── 14.2_slaxbmc
├── 14.2_willysr-csb
└── 14.2_willysr-msb



14.1
.
├── 14.1
├── 14.1_alien-ktown
├── 14.1_alien-restricted
├── 14.1_alien-sbrepos
├── 14.1_boost
├── 14.1_mled
├── 14.1_ponce
├── 14.1_rlworkman
├── 14.1_salix
├── 14.1_sbo
├── 14.1_slacky
├── 14.1_slaxbmc
├── 14.1_willysr-csb
├── 14.1_willysr-msb
├── current
├── current_alien-ktown
├── current_alien-restricted
├── current_alien-sbrepos
├── current_boost
├── current_mled
├── current_willysr-csb
├── current_willysr-msb
├── src
├── src_alien
├── src_alphageek
├── src_anddt
├── src_boost
├── src_bormant
├── src_mled
├── src_nk
├── src_phantomx
├── src_ponce
├── src_rlworkman
├── src_salix
├── src_sbo
├── src_slackers
├── src_slacky
├── src_slaxbmc
├── src_studioware
└── src_willysr

40 directories, 0 files




}




# 2015-18 Written by NK
# You are free to modify or redistribute this in any way you wish.
# Use at your own risk, YMMV (Your mileage may vary), and all that.
# Not affiliated with or endorsed by Slackware Linux, Patrick J. Volkerding, or the SlackBuilds.org project & etc.
# Repositories for slackpkgplus.conf < http://slakfinder.org/slackpkg+.html > .
