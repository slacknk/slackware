#!/bin/sh

# Slackware installation as DVD. 
#
# Contains: bootable INSTALL DVD (including /extra and /source)
#
# Command used to create the ISO's for this DVD:
# (see also /isolinux/README.TXT on the DVD you'll burn from the ISO)


# Written by NK
# You are free to modify or redistribute this in any way you wish.
#
# Idea and some code from < http://slackware.no/makeSlackISOs.sh >
# < http://slackware.com/~alien/tools/mirror-slackware-current.sh >


BUILDER=${BUILDER:-"$USER"}

#ARCH=${ARCH:-"x86_64"}
ARCH=${ARCH:-"x86"}

# 29_Авг_2012
#DATE=`date +"%d_%b_%Y"`

# 20120829
DATE=`date +"%Y%m%d"`


RELEASE=${RELEASE:-"current"}
SLACKRELEASE="slackware-${RELEASE}-${DATE}-${ARCH}"
BOOTLOADSIZE=${BOOTLOADSIZE:-4}
DVD_EXCLUDES=${DVD_EXCLUDES:-"-x ./testing  -x ./source  -x ./extra/source"}


# Options
while getopts "f:d:" Option
do
  case $Option in
    d) DIR=${OPTARG}
       ;;
    f) FINALDIR=${OPTARG}
       ;;
    *) echo -e "\nUnknown error while processing options"
       exit 1
       ;;
  esac
done


#DIR=${DIR:-}
#FINALDIR=${FINALDIR:-}

#DVDISO="${FINALDIR}/${SLACKRELEASE}-install-dvd.iso"
 DVDISO="${FINALDIR}/${SLACKRELEASE}.iso"


if [ ! -d $DIR ]; then
 echo "DIR: $DIR - Not Found!"
 exit 1
elif [ ! -d $FINALDIR ]; then
 echo "DIR for ISO: $FINALDIR - Not Found!"
 exit 1
fi


if [ -f "${DVDISO}" ]; then
 echo ""
 rm -v ${DVDISO}
 sleep 1s
 echo ""
fi


# DVD
cd $DIR

mkisofs -o ${DVDISO} \
	-R -J -V "Slackware-${RELEASE}-DVD" \
	-hide-rr-moved -hide-joliet-trans-tbl \
	-v -d -N -no-emul-boot -boot-load-size ${BOOTLOADSIZE} -boot-info-table \
	-sort isolinux/iso.sort \
	-b isolinux/isolinux.bin \
	-c isolinux/isolinux.boot \
	-preparer "Slackware-${RELEASE} build for ${ARCH} by ${BUILDER}" \
	-publisher "The Slackware Linux Project - http://www.slackware.com/" \
	-A "Slackware-${RELEASE} DVD - build $DATE" \
	${DVD_EXCLUDES} .

echo -e "\n\n$ isohybrid ${DVDISO}"
isohybrid ${DVDISO}

echo -e "\n\n$ ls -l ${DVDISO}"
ls -l ${DVDISO}

echo -e "\n\nSlackware-${RELEASE} DVD - build $DATE for ${ARCH} by ${BUILDER}"
echo -e "\n\nGo to root's terminal and type"
echo -e "\n# dd bs=4M of=/dev/sdc if=${DVDISO} \n# rm -rfv ${DVDISO}"

read enter
