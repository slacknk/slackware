#!/bin/sh


###
##
#
# Thx for Idea and some code
#
#  Alien's tools
# 	http://www.slackware.com/~alien/tools/gen_repos_files.sh
#
#  Jason Woodward / slapt-get
# 	http://software.jaos.org/git/slapt-get/plain/FAQ.html#slgFAQ17
#
#  Gapan/repo-scripts/salix
# 	https://github.com/gapan/repo-scripts/tree/master/salix
#
#  and bormant
# 	https://www.linux.org.ru/forum/general/10993930
#
##
### 


while getopts "d:" Option
do
  case $Option in
    d ) DIR=${OPTARG}
        ;;
    *)  echo "Unknown error while processing options"
        exit 1
        ;;
  esac
done

if [[ ! -d "$DIR" ]]; then
 echo -e "\n> DIR not found: Start with -d /PATH/to/local/repository/"
 exit 1
else
 cd $DIR
fi


echo -e "\n\n\n$DIR"


UPDATEDATE="$(LC_ALL=C date -u)"
PKGS="`find . -type f -regextype posix-egrep -regex '.*\.[tgblzikx]+$'`"
COMPEXE="xz"


# CHECKSUMS.md5
gen_md5sum (){
 find . \
	-type f \
	-regextype posix-egrep \
	-regex '.*\.[tgblzikx]+$' \
	-exec md5sum {} \; \
 > CHECKSUMS.md5
 #cat CHECKSUMS.md5 | gzip -9 -c - > CHECKSUMS.md5.gz
}
gen_md5sum


# PACKAGES.TXT
gen_packages_txt (){
 echo "" > PACKAGES.TXT
 #echo -e "PACKAGES.TXT;  $UPDATEDATE\n\n" 	> PACKAGES.TXT
 echo -e "\nDetected:"
 for PKG in $PKGS ; do
 	echo -e "$PKG"
 
 	NAME=$(echo $PKG | sed -re "s/(.*\/)(.*.t[blxg]z)$/\2/")
 	LOCATION=$(echo $PKG | sed -re "s/(.*)\/(.*.t[blxg]z)$/\1/")
 	
 	# Alien
 	SIZE=$(du -s $PKG | cut -f 1)
 	USIZE=""; cat $PKG | $COMPEXE -dc | dd 1> /dev/null 2> /tmp/.temp.uncomp.$$
 	USIZE="$(expr $(cat /tmp/.temp.uncomp.$$ | head -n 1 | cut -f1 -d+) / 2)"
 	rm /tmp/.temp.uncomp.$$
	
	# Salix OS
	#SIZE=$( expr `ls -l $PKG | awk '{print $5}'` / 1024 )
	#USIZE=$[$SIZE * 4 ]
	
	pkg_desc (){
 	 $COMPEXE -cd $PKG | tar xOf - install/slack-desc | sed -n '/^#/d;/:/p'
	}
	
 	echo "PACKAGE NAME:  $NAME" 					>> PACKAGES.TXT
 	echo "PACKAGE LOCATION:  $LOCATION" 			>> PACKAGES.TXT
 	echo "PACKAGE SIZE (compressed):  $SIZE K" 		>> PACKAGES.TXT
 	echo "PACKAGE SIZE (uncompressed):  $USIZE K" 	>> PACKAGES.TXT
 	echo "PACKAGE DESCRIPTION:" 					>> PACKAGES.TXT
 	pkg_desc 										>> PACKAGES.TXT
 	echo "" 										>> PACKAGES.TXT
 done
 echo ""
}
gen_packages_txt


# MANIFEST
gen_manifest (){
 if [ ! -f MANIFEST_null ]; then
  [ -f MANIFEST.bz2 ] && rm -v MANIFEST.bz2
  touch MANIFEST_null
  #echo "" > MANIFEST_null
  echo "" > MANIFEST
  bzip2 -9f MANIFEST
 fi
}
gen_manifest


# PS Because, gen.repository w/o GPG, 
# recomended use slackpkg update with GPG-off:
# > printf 'y' | slackpkg update -checkgpg=off
