#!/bin/bash

# SlackWORLD.sh
# this is script for slackware 
# sources, release, current and someVER any projects


###
##
#
#
# Example, used for all
#
#
# ./mirror_slackware.sh -m /PATH/to/DIRMIR -d all -v {current,14.1}
#
# for v in 14.1 current src ; do "`dirname $0`/slack-mirror.sh" -m $DIRM -d all -v $v ; done
#
# ./slack-mirror.sh -m /PATH/to/DIRMIR -d all -v `. /etc/os-release ; echo $VERSION`"
#
#
##
###


###
##
#
#
# Notes!
#
#
# -~= 2016 =~-
#
#
# 25 March, 2016
# taper -> bear
# http://alien.slackbook.org/blog/bear-is-live/
#
#
# 14 February, 2016
# taper.alienbase.nl mirror will lose rsync access
# http://alien.slackbook.org/blog/taper-alienbase-nl-mirror-will-lose-rsync-access/
# -=> rsync://alien.slackbook.org/alien/
# -=> rsync://slackware.uk/people/alien/
#
#
##
###



# only-current-user
if test $(id -u) -eq 0; then
 echo "You must not launch as root, it is INSECURE"
 exit
fi



set -e 


if [ -f /tmp/slack-mirror-error.log ]; then
 cat /tmp/slack-mirror-error.log
 rm -v /tmp/slack-mirror-error.log
fi


# Options
while getopts "m:d:v:" Option
do
  case $Option in
    m ) DIRMIR=${OPTARG}
        ;;
    d ) DISTRO=${OPTARG}
        ;;
    v ) VER=${OPTARG}
        ;;
    *)  echo ""
        echo "Unknown error while processing options"
        exit 1
        ;;
  esac
done


if [[ ! -d "$DIRMIR" ]]; then
 echo ""
 echo "> DIRMIR not found - 1st creat $DIRMIR"
 exit 1
fi

if [[ -z "$VER" ]]; then
 echo ""
 echo "> VER? Please, enter version: release.version or current"
 exit 1
fi

# Arch-detected
d_arch(){
 x32 (){ ARCH="32" ; DARCH=""	; FARCH="x86" 	 ; iARCH="i486" 	; }
 x64 (){ ARCH="64" ; DARCH="64" ; FARCH="x86_64" ; iARCH="x86_64" 	; }
 if [[ "$ARCH" == "64" ]]; then
  x64
 elif [[ "$ARCH" == "32" ]]; then
  x32
 else
  [[ ! -z "`uname -m | grep x86_64`" ]] && x64 ||x32
 fi
}
d_arch

echo ""
echo ""
echo " ARCH = $ARCH"
echo "FARCH = $FARCH"
echo "iARCH = $iARCH"
echo "DARCH = $DARCH"
echo ""
echo ""


# rsync-keys
KEY="-r --delete --progress -vh"
KEYDIROFF="--exclude=/*/"
KEYDIR="-r --delete-excluded"


#--include --include=*source/x/x11/** 
KEYFONTS="\
--include=*source/x/x11/src/font/** \
--include=*source/x/fontconfig/** \
--include=*source/l/freetype/** \
--include=*source/l/cairo/** "
# $KEYFONTS
#  
KEYSC="\
--exclude=*source/x/x11/src/*.tar.xz "
#--include=*source/x/x11/src/driver/xf86-video-intel-* \
#--include=*source/xap/mozilla-firefox/** \


KEYLANG="--include=*-dict-ru-* --include=*-dict-en-* --exclude=*-dict-* \
--exclude=*-l10n-ru-*.tar.xz --include=*-l10n-ru* --exclude=*-l10n-* \
--exclude=*-help-ru-*.tar.xz --include=*-help-ru* --exclude=*-help-*"

KEYSRC="--exclude=*source/**.zip  --exclude=*source/**.tar.bz2  --exclude=*source/**.tar.gz  --exclude=*source/**.tar.xz"
KEYSRC2="--exclude=*.zip --exclude=*.tar.bz2 --exclude=*.tar.gz  --exclude=*.tar.xz --exclude=*.jar --exclude=*.dat --exclude=*.deb --exclude=*.rpm --exclude=*.vbox-extpack --exclude=*.run --exclude=*.bin --exclude=*.iso --exclude=*.iso.gz --exclude=*.cab --exclude=*.msi --exclude=*.exe --exclude=*.pdf --exclude=*.pdf.gz --exclude=*.dic.gz --exclude=*.img --exclude=*.img.gz --exclude=*.ogg"
KEYSRC3="$KEYSRC2 --exclude=*.tar --exclude=*.tgz --exclude=*.txz --exclude=*.tbz2 --exclude=*.tbz --exclude=*.tar.Z --exclude=*.tar.lzma --exclude=*.h264 "
KEYSRC3+="--include=*diff* --include=*patch*"

KEYCALLIGRA="--exclude=*calligra* --exclude=*calligra-l10n-*"
KEYXFCE="--exclude=*/xfce/ "
KEY64="--exclude=x86/"
KEYXBMCPI="--exclude=*pi/"
KEYK="--exclude=*/kde/ --exclude=*/kdei/"


# http://wiki.agilialinux.ru/devel/rsync_update
KEYAG="-avrh --delete --progress"

# http://alien.slackbook.org/ktown/current/latest/README
KEYALK32="-av --exclude=x86_64"
KEYALK64="-av --exclude=x86"

# http://mirrors.slackware.com/guidelines/
KEYSLACK="-aPSH --delete"

# http://slackware.no/slack-get.sh
KEYSLACKNO="-av --delete-after --delay-updates --hard-links --links -H --stats"

# http://docs.salixos.org/wiki/How_to_create_a_public_Salix_mirror
KEYSLAX="-a --del"

# http://slackware.ponce.cc/blog/2010/04/13/rsync-repository/
KEYPONCE="--delete-after -avP"


# mirrors-slackware
MIRRORYA="rsync://mirror.yandex.ru"
MIRRORSOU="rsync://slackware.org.uk"
MIRROROSU="rsync://rsync.osuosl.org"
MIRRORLIN="rsync://mirrors.kernel.org"

 MIRRORALIEN="rsync://taper.alienbase.nl/mirrors/"
#MIRRORALIEN="rsync://alien.slackbook.org/alien/ktown/current/5 


# mirrors-project

MIRRORSALIX="slackware.org.uk::salix"
#MIRRORSALIX="rsync://mirror.inode.at/mirror/salix"

MIRRORSBO="rsync://slackbuilds.org"
MIRRORMSB="slackware.org.uk::msb/"
MIRRORCSB="slackware.org.uk::csb/"
MIRRORSEU="slackware.org.uk::slacky/"
MIRRORXBMC="slackware.org.uk::slaxbmc"
MIRRORPONCE="rsync://ponce.cc/pub/"
MIRROR32="$MIRRORALIEN/people/alien/multilib/"
MIRRORMLED="$MIRRORALIEN/people/kikinovak/"
MIRRORRW="rsync://slackware.org.uk/people/rlworkman/"


# src-git
get_git0(){
echo -e "\n\n═════════════════════════════════════════════════"
echo -e "${MIRRORGIT}\n$DIR\n\n"
}
get_git1 (){
get_git0
if [ ! -d "$DIR" ]; then
 git clone $MIRRORGIT $DIR
else
 cd $DIR
  #git clean -fd && git pull
  git fetch origin && rm -rf * && sleep 2s && git reset --hard origin/master
 cd - &> /dev/null
fi
echo ""
}
get_git2(){
for MIRRORGIT in $MIRRORGIT1 $MIRRORGIT2 ; do
get_git0
if [ ! -d "$DIR/${MIRRORGIT/"$MIRRORGIT0"//}" ]; then
 git clone $MIRRORGIT $DIR/${MIRRORGIT/"$MIRRORGIT0/"//}
else
 cd ${DIR}/${MIRRORGIT/"$MIRRORGIT0/"//}
  #git clean -fd && git pull
  git fetch origin && rm -rf * && sleep 2s && git reset --hard origin/master
 cd - &> /dev/null
fi
done
echo ""
}


#####
get_csb(){
if [ "$VER" == "src" ]; then
 MIRRORGIT="https://github.com/willysr/csb"
 DIR="$DIRMIR/src_willysr/csb"
 get_git1
else
 MIRROR="$MIRRORCSB"
 DIR="$DIRMIR/${VER}_willysr-csb/"
 rsync 	$KEY $KEYDIROFF 	$MIRRORCSB 					$DIR
 rsync 	$KEY $KEYDIR		$MIRRORCSB/$VER/$FARCH/ 	$DIR/pkg
fi
}

#####
get_msb(){
MIRROR="$MIRRORMSB"
if [ "$VER" == "src" ]; then
 DIR="$DIRMIR/src_willysr/msb"
 # src-msb
 MIRRORGIT0="https://github.com/mateslackbuilds"
 MIRRORGIT1="$MIRRORGIT0/msb"
 MIRRORGIT2="$MIRRORGIT0/mateslackbuilds.github.com"
 get_git2
else
 DIR="$DIRMIR/${VER}_willysr-msb/"
 rsync 	$KEY $KEYDIROFF 		$MIRRORMSB 						$DIR
 if [ "$VER" == "current" ]; then
   rsync 	$KEY $KEYDIR 		$MIRRORMSB/testing/1.12/$FARCH/ 	 $DIR/1.12_${ARCH}
   rsync 	$KEY $KEYDIR 		$MIRRORMSB/testing/1.14/$FARCH/ 	 $DIR/1.14_${ARCH}
   rsync 	$KEY $KEYDIR 		$MIRRORMSB/testing/1.14-gtk3/$FARCH/ $DIR/1.14-gtk3_${ARCH}
   cd $DIR
   #[ -L latest ] && rm -v latest
   #ln -svf 1.14_${ARCH} 		latest
    ln -svf 1.14-gtk3_${ARCH} 	latest
   cd -
  #rsync $KEY $KEYDIROFF 		$MIRRORMSB/testing/ 			$DIR/
  #rsync $KEY $KEYDIR $KEYALK32	$MIRRORMSB/testing 				$DIR/
 #else
 elif [ "$VER" == "14.1" ]; then
  rsync 	$KEY $KEYDIR		$MIRRORMSB/$VER/1.6/$FARCH/ 	$DIR/1.6_${ARCH}
  rsync 	$KEY $KEYDIR		$MIRRORMSB/$VER/1.8/$FARCH/ 	$DIR/1.8_${ARCH}
  rsync 	$KEY $KEYDIR		$MIRRORMSB/$VER/1.10/$FARCH/ 	$DIR/1.10_${ARCH}
  rsync 	$KEY $KEYDIR		$MIRRORMSB/$VER/1.12/$FARCH/ 	$DIR/1.12_${ARCH}
  cd $DIR
   #[ -L latest ] && rm -v latest
   ln -svf 1.12_${ARCH} latest
  cd -
 fi
fi
}


# distro-actions


#####
get_slack() {
#MIRROR="rsync://slackware.org.uk/slackware/slackware$DARCH-$VER"
#MIRROR="$MIRRORYA/slackware/slackware$DARCH-$VER"
#MIRROR="rsync://bear.alienbase.nl/mirrors/slackware/slackware$DARCH-$VER"
###
#MIRROR="$MIRRORALIEN/slackware/slackware$DARCH-$VER"
#MIRROR="$MIRRORLIN/slackware/slackware$DARCH-$VER"
#MIRROR="$MIRRORSOU/slackware/slackware$DARCH-$VER"
#DIR="$DIRMIR/${VER}_slackware"
###
#MIRROR="rsync://mirrors.kernel.org/slackware/slackware$DARCH-$VER"
MIRROR="rsync://bear.alienbase.nl/mirrors/slackware/slackware$DARCH-$VER"
DIR="$DIRMIR/${VER}"
if [ "$VER" == "src" ]; then
 ### iso-torrents
 #MIRROR="$MIRRORALIEN/slackware/torrents"
 MIRROR="rsync://bear.alienbase.nl/mirrors/slackware/slackware$DARCH-$VER"
 rsync 	$KEYSLACK $KEY $KEYDIR  		$MIRROR/../torrents		$DIR/
 
 ### slack_sources
 [ ! -d "$DIR" ] && mkdir -pv $DIR
 for r in 13.0 13.1 13.37 14.0 14.1 current ; do
  MIRROR="rsync://slackware.org.uk/slackware/slackware64-$r"
 #MIRROR="$MIRRORYA/slackware/slackware64-$r"
  rsync 	$KEY $KEYDIR $KEYSRC3 	$MIRROR/source 				$DIR/$r
  rsync 	$KEY $KEYDIR $KEYSRC3 	$MIRROR/extra/source/ 		$DIR/$r/extra/
  rsync 	$KEY $KEYDIR $KEYSRC3 	$MIRROR/pasture/source/ 	$DIR/$r/pasture/
  if [ "$r" != "current" ]; then
   rsync 	$KEY $KEYDIR $KEYSRC3 	$MIRROR/patches/source/ 	$DIR/$r/patches/
  fi
 done

elif [ "$VER" == "current" ]; then
  rsync $KEYSLACK $KEY $KEYDIR $KEYLANG $KEYSC $KEYSRC --exclude=source/* 	$MIRROR/ 	$DIR
 #rsync $KEYSLACK $KEY $KEYDIR $KEYLANG $KEYSC $KEYSRC  					$MIRROR/ 	$DIR
 #rsync $KEYSLACK $KEY $KEYDIR $KEYCALLIGRA $KEYLANG $KEYSC $KEYSRC  		$MIRROR/ 	$DIR
 
else
  rsync $KEYSLACK $KEY $KEYDIR $KEYLANG $KEYSRC --exclude=source/*			$MIRROR/ 	$DIR
 #rsync $KEYSLACK $KEY $KEYDIR $KEYLANG $KEYSRC 							$MIRROR/ 	$DIR
 #rsync $KEYSLACK $KEY $KEYDIR $KEYCALLIGRA $KEYLANG $KEYSRC 				$MIRROR/ 	$DIR
 #rsync $KEYSLACK $KEY $KEYDIR $KEYCALLIGRA $KEYLANG $KEYSRC $KEYFONTS 		$MIRROR/ 	$DIR
 #rsync $KEYSLACK $KEY $KEYDIR $KEYCALLIGRA $KEYLANG $KEYSRC 				$MIRROR 	$DIR
 #rsync $KEYSLACK $KEY $KEYDIR $KEYCALLIGRA $KEYXFCE $KEYLANG $KEYSRC 		$MIRROR/ 	$DIR
 #rsync $KEYSLACK $KEY $KEYDIR $KEYCALLIGRA $KEYK $KEYLANG $KEYSRC 			$MIRROR 	$DIR

fi
}


#####
get_salix(){
if [ "$VER" == "src" ]; then
 MIRROR="rsync://slackware.org.uk/salix/"
 DIR="$DIRMIR/src_salix/"
 mkdir -pv $DIRMIR/src_salix/{slkbuilds,sources}

 for r in 13.0 13.1 13.37 14.0 14.1 14.2 ; do
  rsync	$KEY $KEYDIR $KEYLANG $KEYSRC3 --exclude=*.log 	\
 	$MIRROR/$iARCH/$r/source/ 	$DIR/sources/$r
 done

 for r in 13.37 14.0 14.1 14.2 ; do
  rsync	$KEY $KEYDIR $KEYLANG $KEYSRC3 --exclude=*.log 	\
 	$MIRROR/slkbuild/$r 		$DIR/slkbuilds/
 done

else

 MIRROR="$MIRRORSALIX"
 DIR="$DIRMIR/${VER}_salix/"
 rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIROFF 	$MIRROR/ 						$DIR/
 # salix
 rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIROFF 	$MIRROR/$iARCH/$VER/ 			$DIR/$iARCH/
 rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIR		$MIRROR/$iARCH/$VER/salix/ 		$DIR/$iARCH/salix/
 rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIR		$MIRROR/$iARCH/$VER/iso/ 		$DIR/iso/
 #rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIR	$MIRROR/sbo/$VER/ 				$DIR/sbo/
 #rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC2 $KEYDIR	$MIRROR/slkbuild/$VER/ 			$DIR/slkbuild/
 #rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC3 $KEYDIR	$MIRROR/$iARCH/$VER/source/ 	$DIR/source/
 # salix-slackware
 #rsync $KEYSLAX $KEY $KEYCALLIGRA $KEYLANG $KEYSRC $KEYXFCE $KEYK $KEYDIR 	$MIRROR/$iARCH/slackware-$VER/ 	$DIR/${iARCH}-slackware/
 #rsync $KEYSLAX $KEY $KEYDIR $KEYSRC $KEYCALLIGRA $KEYK 					$MIRROR/$iARCH/slackware-$VER/ 	$DIR/${iARCH}-slackware/
 
fi
}


#####
get_ktown() {
#MIRROR="rsync://alien.slackbook.org/alien/ktown/"
#MIRROR="$MIRRORALIEN/alien-kde/"
#MIRROR="rsync://rsync.slackware.org.uk/people/alien-kde/"
#MIRROR="$MIRRORSOU/people/alien-kde/"
#DIR="$DIRMIR/alien-ktown-$VER"
#rsync 	$KEY $KEYDIROFF 					$MIRROR/$VER/latest/ 			$DIR
#rsync 	$KEY $KEYDIR $KEYCALLIGRA $KEYLANG 	$MIRROR/$VER/latest/$FARCH/  	$DIR/$FARCH/
#rsync 	$KEY $KEYDIR $KEYSRC3 				$MIRROR/source/latest/ 			${DIR/$VER/source/}
# packages - Release and Current - Latest & Testing
#if [ "$VER" == "current" ]; then
 #[ ! -d $DIR ] && mkdir -p $DIR
 #rsync 	$KEY $KEYDIR $KEYCALLIGRA $KEYLANG $KEYALK32 	$MIRROR/$VER/latest/  			$DIR/latest
 #rsync 	$KEY $KEYDIR $KEYLANG $KEYALK32 				$MIRROR/$VER/latest/  			$DIR/latest
 #rsync 	$KEY $KEYDIR $KEYSRC3 							$MIRROR/source/latest/ 			$DIR/latest-src
 #rsync 	$KEY $KEYDIR $KEYLANG 				$MIRROR/$VER/latest/$FARCH/  	${DIR}/pkg
 #rsync 	$KEY $KEYDIR $KEYCALLIGRA $KEYLANG 	$MIRROR/$VER/latest/$FARCH/  	${DIR}/pkg
 #rsync 	$KEY $KEYDIR $KEYSRC3 				$MIRROR/source/latest/ 			${DIR}/src
#
MIRROR="rsync://bear.alienbase.nl/mirrors/alien-kde"
if [ "$VER" == "src" ]; then
 # sources - SlackBuilds
 DIR="$DIRMIR/${VER}_alien/ktown"
 rsync 		$KEY $KEYDIROFF					$MIRROR/ 						$DIR
 rsync 		$KEY $KEYDIR $KEYSRC3 			$MIRROR/source 					$DIR
else
 # packages
 DIR="$DIRMIR/${VER}_alien-ktown"
 rsync 		$KEY $KEYDIROFF 				$MIRROR/						${DIR}/
 rsync 		$KEY $KEYDIROFF 				$MIRROR/$VER/latest/ 			${DIR}/latest/
 rsync 		$KEY $KEYDIR $KEYLANG 			$MIRROR/$VER/latest/$FARCH  	${DIR}/latest/
fi
}

#####

get_ktown_testing() {
 #MIRROR="rsync://alien.slackbook.org/alien/ktown/"
 #rsync $KEY $KEYDIR $KEYLANG $KEYALK32 	$MIRROR/$VER/testing/ 			$DIR/testing
 #rsync $KEY $KEYDIR $KEYLANG $KEYALK32 	$MIRROR/$VER/testing/ 			$DIR/testing
 #rsync $KEY $KEYDIR $KEYSRC3 				$MIRROR/source/testing/ 		$DIR/testing-src
# sources code-UP
if [ "$VER" == "current" ]; then
 MIRROR="rsync://bear.alienbase.nl/mirrors/alien-kde"
 DIR="$DIRMIR/${VER}_alien-ktown"
 [ ! -d "$DIR/testing" ] && mkdir -pv $DIR/testing
 rsync 	$KEY $KEYDIROFF 					$MIRROR/						${DIR}/
 rsync 	$KEY $KEYDIROFF 					$MIRROR/$VER/testing/ 			${DIR}/testing/
 rsync 	$KEY $KEYDIR $KEYLANG 				$MIRROR/$VER/testing/$FARCH  	${DIR}/testing/
fi
}

##########
#####
get_sbrepos() {
 MIRROR="rsync://bear.alienbase.nl/mirrors/people/alien/sbrepos/"
#MIRROR="rsync://alien.slackbook.org/alien/sbrepos/"
#MIRROR="$MIRRORALIEN/people/alien/sbrepos/"
#MIRROR="rsync://rsync.slackware.org.uk/people/alien/sbrepos/"
DIR="$DIRMIR/${VER}_alien-sbrepos/"
rsync 	$KEY $KEYDIROFF 										$MIRROR							$DIR
rsync	$KEY $KEYDIR $KEYLANG $KEYSRC 							$MIRROR/$VER/$FARCH 			$DIR/
#rsync 	$KEY $KEYDIR $KEYLANG $KEYSRC3 --exclude=*/pkg{,64}/  	${MIRROR/sbrepos/slackbuilds}/	$DIR/src
}

#####
get_restricted() {
 MIRROR="rsync://bear.alienbase.nl/mirrors/people/alien/restricted_sbrepos/"
#MIRROR="rsync://slackware.uk/people/alien/restricted_sbrepos/"
#MIRROR="$MIRRORALIEN/people/alien/restricted_sbrepos/"
DIR="$DIRMIR/${VER}_alien-restricted/"
rsync 	$KEY $KEYDIROFF 										$MIRROR							$DIR
rsync	$KEY $KEYDIR $KEYLANG $KEYSRC 							$MIRROR/$VER/$FARCH				$DIR/
#rsync 	$KEY $KEYDIR $KEYLANG $KEYSRC3 --exclude=*/pkg{,64}/  	${MIRROR/sbrepos/slackbuilds}/	$DIR/src
}

#####
get_lib32 (){
 MIRROR="rsync://bear.alienbase.nl/mirrors/people/alien/multilib"
#MIRROR="rsync://alien.slackbook.org/alien/multilib"
#MIRROR="$MIRROR32"
if [ "$VER" == "src" ]; then
 DIR="$DIRMIR/${VER}_alien/lib32"
 rsync $KEY $KEYDIROFF 		$MIRROR/						$DIR/
 rsync $KEY $KEYDIR 		$MIRROR/source 					$DIR/
 #rsync $KEY $KEYDIROFF 	$MIRROR/source/					$DIR/source
 #rsync	$KEY $KEYDIR 		$MIRROR/source/$VER 			$DIR/source
 #rsync	$KEY $KEYDIR 		$MIRROR/source/compat32-tools 	$DIR/source
elif [ "$ARCH" == "64" ]; then
 DIR="$DIRMIR/${VER}_alien-lib32"
 rsync 	$KEY $KEYDIROFF 	$MIRROR							$DIR
 rsync	$KEY $KEYDIR 		$MIRROR/$VER	 				$DIR
fi
}

#####
get_alien_src() {
#MIRROR="$MIRRORSOU/people/alien/tools/"
#MIRROR="$MIRRORALIEN/people/alien/slackbuilds/"
#MIRROR="$MIRRORALIEN/people/alien/restricted_slackbuilds/"
#MIRROR0="rsync://alien.slackbook.org/alien"
DIR="$DIRMIR/src_alien"
[ ! -d $DIR ] && mkdir -pv $DIRMIR/src_alien
#MIRROR="$MIRRORSOU/people/alien/
 MIRROR="rsync://bear.alienbase.nl/mirrors/people/alien/archive/"
  rsync 	$KEY $KEYDIR $KEYSRC3 									$MIRROR		$DIR/_archive
 MIRROR="rsync://bear.alienbase.nl/mirrors/people/alien/tools/"
  rsync 	$KEY $KEYDIR  											$MIRROR		$DIR/_tools

#MIRROR="rsync://alien.slackbook.org/alien/
 MIRROR="rsync://bear.alienbase.nl/mirrors/people/alien/slackbuilds/"
  rsync 	$KEY $KEYDIR $KEYLANG $KEYSRC3 --exclude=*/pkg{,64}/  	$MIRROR		$DIR/slackbuilds
 MIRROR="rsync://bear.alienbase.nl/mirrors/people/alien/restricted_slackbuilds/"
  rsync 	$KEY $KEYDIR $KEYLANG $KEYSRC3 --exclude=*/pkg{,64}/  	$MIRROR		$DIR/slackbuilds_restricted

}
#####
##########


#####
get_slacky (){
if [ "$VER" == "src" ]; then
 MIRROR="$MIRRORSEU"
 DIR="$DIRMIR/src_slacky/"
 for r in 11.0 12.0 12.1 12.2 13.0 13.1 13.37 14.0 14.1 ; do
  rsync	$KEY $KEYDIR $KEYLANG $KEYSRC3 	$MIRROR/slackware-$r 			$DIR/
  rsync	$KEY $KEYDIR $KEYLANG $KEYSRC3 	$MIRROR/slackware64-$r 			$DIR/
 done
else
 MIRROR="$MIRRORSEU"
 DIR="$DIRMIR/${VER}_slacky/"
 rsync 	$KEY $KEYDIROFF 				$MIRROR/ 						$DIR/
 rsync	$KEY $KEYDIR $KEYLANG $KEYSRC 	$MIRROR/slackware$DARCH-$VER/ 	$DIR/slackware/
fi
}

#####
get_alphageek(){
MIRROR="rsync://rsync.slackware.org.uk/people/alphageek/"
DIR="${DIRMIR}/src_alphageek/"
rsync	$KEY $KEYDIR $KEYLANG $KEYSRC3 	$MIRROR/ 	$DIR/
}
#####
get_sbo(){
MIRROR="$MIRRORSBO"
DIR="$DIRMIR/src_sbo/"
# release
#for r in templates slackbuilds ; do
# if [ ! -d "$DIR/$r" ]; then
#  mkdir -p $DIR/$r
# fi
#done 
#rsync	$KEY $KEYDIR 	$MIRROR/templates/			$DIR/templates
#rsync	$KEY $KEYDIR 	$MIRROR/slackbuilds/$VER/	$DIR/slackbuilds
# all


#rsync	$KEY $KEYDIR 	$MIRROR/templates/			$DIR/templates/
#for v in 11.0 12.0 12.1 12.2 13.0 13.1 13.37 14.0 14.1 ; do
# rsync	$KEY $KEYDIR 	$MIRROR/slackbuilds/$v 		$DIR/
#done

# https://slackbuilds.org/cgit/slackbuilds/
MIRRORGIT="git://slackbuilds.org/slackbuilds.git"
DIR="$DIRMIR/src_sbo/current-master"
get_git1

# http://cgit.ponce.cc/slackbuilds/
MIRRORGIT="git://github.com/Ponce/slackbuilds.git"
DIR="$DIRMIR/src_sbo/current-ponce"
get_git1

echo ""
}

get_ponce(){
MIRROR="$MIRRORPONCE"
DIR="$DIRMIR/${VER}_ponce"
if [ "$VER" == "src" ]; then
 rsync 	$KEY $KEYDIR $KEYSRC3 				$MIRROR/testing/ 				$DIR
else
 #rsync $KEYPONCE $KEY $KEYDIR				$MIRROR/ 						$DIR
 #rsync $KEY $KEYDIR						$MIRROR/ 						$DIR
 rsync $KEY $KEYDIR --exclude=old.packages/	$MIRROR/slackware$DARCH-$VER/ 	$DIR
fi
}
#####
get_xbmc(){
DIR="$DIRMIR/${VER}_slaxbmc/"
if [ "$VER" == "src" ]; then
 rsync 	$KEY $KEYLANG $KEYDIR $KEYSRC3 	$MIRRORXBMC/13.37 	$DIR/
 for ver in 14.0 14.1 ; do
  MIRROR="$MIRRORXBMC/$ver"
  [ ! -d $DIR/$ver ] && mkdir -pv $DIR/$ver
  rsync 	$KEY $KEYLANG $KEYDIR $KEYSRC3 	$MIRROR/slaxbmcsrc-$ver/ 	$DIR/$ver
 done
else
 MIRROR="$MIRRORXBMC/$VER"
 rsync $KEY $KEYLANG $KEYDIROFF 		$MIRROR/slaxbmc$DARCH-$VER/ 				$DIR/
 rsync $KEY $KEYLANG $KEYDIR 			$MIRROR/slaxbmc$DARCH-$VER/extra/ 			$DIR/extra/
 rsync $KEY $KEYLANG $KEYDIR 			$MIRROR/slaxbmc$DARCH-$VER/patches/ 		$DIR/patches/
 rsync $KEY $KEYLANG $KEYDIROFF 		$MIRROR/slaxbmc$DARCH-$VER/slackware/ 		$DIR/slackware/
 rsync $KEY $KEYLANG $KEYDIR 			$MIRROR/slaxbmc$DARCH-$VER/slackware/xbc/ 	$DIR/slackware/xbc
 rsync $KEY $KEYLANG $KEYDIR 			$MIRROR/slaxbmc$DARCH-$VER/slackware/xsc/ 	$DIR/slackware/xsc
 #rsync $KEY $KEYLANG $KEYDIR $KEYSRC2 	$MIRROR/slaxbmcsrc-$VER/source/ 			$DIR/src/
fi


}
#####
get_mled(){
#MIRROR="$MIRRORMLED"
 MIRROR="rsync://slackware.org.uk/microlinux/"

if [ "$VER" == "src" ]; then
 #MIRRORGIT="https://github.com/kikinovak/slackware"
 #DIR="$DIRMIR/src_mled"
 #get_git1
 #
 #
 MIRRORGIT0="https://github.com/kikinovak"
 MIRRORGIT1="$MIRRORGIT0/slackware"
 MIRRORGIT2="$MIRRORGIT0/microlinux"
 DIR="$DIRMIR/src_mled"
 get_git2
 
else
 DIR="$DIRMIR/${VER}_mled/"
 rsync 	$KEY $KEYDIROFF 	$MIRROR 						$DIR
 
 for MLED in desktop extras server ; do
  #[  "$VER" == "current"  ] && ver=14.2 || ver=14.1
  #rsync 	$KEY $KEYDIR	$MIRROR/$MLED-$ver-${ARCH}bit 	$DIR/
   rsync 	$KEY $KEYDIR	$MIRROR/$MLED-$VER-${ARCH}bit 	$DIR/
 done
 
 if [ "$VER" == "14.1" ]; then
  rsync 	$KEY $KEYDIR 	$MIRROR/testing-$VER-${ARCH}bit $DIR/
 fi
 
 #rsync 	$KEY $KEYDIR			$MIRROR/desktop-$VER-${ARCH}bit 	$DIR/pkg/
 #rsync 	$KEY $KEYDIR			$MIRROR/extras-$VER-${ARCH}bit 		$DIR/pkg/
 #rsync 	$KEY $KEYDIR			$MIRROR/server-$VER-${ARCH}bit 		$DIR/pkg/
 #rsync 	$KEY $KEYDIR			$MIRROR/testing-$VER-${ARCH}bit 	$DIR/pkg/

 #rsync 	$KEY $KEYDIR $KEYSRC3	$MIRROR/desktop-$VER-source 		$DIR/src/
 #rsync 	$KEY $KEYDIR $KEYSRC3	$MIRROR/extras-$VER-source 			$DIR/src/
 #rsync 	$KEY $KEYDIR $KEYSRC3	$MIRROR/server-$VER-source 			$DIR/src/
 #rsync 	$KEY $KEYDIR $KEYSRC3 	$MIRROR/testing-$VER-source 		$DIR/src/

 #rsync 	$KEY $KEYDIR			$MIRROR/Linux-HOWTOs 				$DIR
 #rsync 	$KEY $KEYDIR			$MIRROR/tools 						$DIR
 #rsync 	$KEY $KEYDIR			$MIRROR/template 					$DIR

 #rsync 	$KEY $KEYDIR			$MIRROR/stripslack-$VER/ 			$DIR/source/stripslack
 #rsync 	$KEY $KEYDIR			$MIRROR/recipes 					$DIR/source/
 #rsync 	$KEY $KEYDIR			$MIRROR/repository-layout 			$DIR/source/
fi
}

#####
get_boost(){
MIRROR="rsync://download.deepstyle.org.ua/slackware/slackboost${DARCH}-$VER/"
DIR="$DIRMIR/${VER}_boost/"
if  [ "$VER" == "src" ]; then
 [ ! -d $DIR ] && mkdir -pv $DIR
 rsync 	$KEY $KEYDIR $KEYSRC3 --exclude="_alt" 	$MIRROR/../slackboost64-14.1/sourceboost/ 		$DIR/14.1
 rsync 	$KEY $KEYDIR $KEYSRC3 					$MIRROR/../slackboost64-14.1/sourceboost_alt/ 	$DIR/14.1_alt
 rsync 	$KEY $KEYDIR 		 					$MIRROR/../slackboost64-14.1/scripts/			$DIR/14.1_scripts
 rsync 	$KEY $KEYDIR 		 					$MIRROR/../slackboost64-14.1/documentation/		$DIR/14.1_documentation
 rsync 	$KEY $KEYDIR $KEYSRC3 					$MIRROR/../slackboost64-current/sourceboost/ 	$DIR/current
 rsync 	$KEY $KEYDIR 		 					$MIRROR/../slackboost64-current/scripts/		$DIR/current_scripts
else
 rsync 	$KEY $KEYDIR							$MIRROR/slackboost${DARCH} 						$DIR/
 #mkdir -p $DIR/{slackboost_old,slackboost_new,slackboost_alt}
 #mkdir -p $DIR/slackboost${DARCH}_{old,new,alt}
 #rsync $KEY $KEYDIR $KEYSRC3               	$MIRROR/../slackboost64-$VER/sourceboost 		$DIR/
 #rsync $KEY $KEYDIR							$MIRROR/slackboost/ 							$DIR/packages/
 rsync 	$KEY $KEYDIR 							$MIRROR/slackboost${DARCH}-01/ 					$DIR/slackboost${DARCH}_old
 rsync 	$KEY $KEYDIR							$MIRROR/slackboost${DARCH}-02/ 					$DIR/slackboost${DARCH}_new
 rsync 	$KEY $KEYDIR							$MIRROR/slackboost${DARCH}-03/ 					$DIR/slackboost${DARCH}_alt
 if  [ "$VER" == "14.1" ]; then
  #rsync $KEY $KEYDIR							$MIRROR/scripts 								$DIR/
   rsync $KEY $KEYDIR							$MIRROR/slackboost_multiver 					$DIR/
 fi
fi
}

#####
get_rw(){
MIRROR="$MIRRORRW"
DIR="$DIRMIR/${VER}_rlworkman/"
#$ rsync -l rsync://slackware.uk/people/rlworkman
#lrwxrwxrwx 	rlworkman -> rworkman
if  [ "$VER" == "src" ]; then
 rsync 	$KEY $KEYDIROFF 		$MIRROR/ 					$DIR
 for v in 11.0 12.0 12.1 12.2 13.0 13.1 13.37 14.0 14.1 ; do    
  rsync $KEY $KEYDIR $KEYSRC3	$MIRROR/sources/$v 			$DIR
 done
 rsync 	$KEY $KEYDIR $KEYSRC3	$MIRROR/14.1/testing/src/ 	$DIR/14.1_testing
else
 rsync 	$KEY $KEYDIROFF 		$MIRROR/					$DIR/
 rsync 	$KEY $KEYDIROFF 		$MIRROR/$VER/				$DIR/packages/
 rsync 	$KEY $KEYDIR  			$MIRROR/$VER/$iARCH  		$DIR/packages/
 #rsync $KEY $KEYDIR $KEYALK32 	$MIRROR/$VER/ 				$DIR/pkgs/
 #rsync $KEY $KEYDIR $KEYSRC3	$MIRROR/sources/$VER/ 		$DIR/src/
fi
}
#####
get_sit(){
MIRRORGIT0="https://github.com/conraid"
MIRRORGIT1="$MIRRORGIT0/SlackBuilds"
MIRRORGIT2="$MIRRORGIT0/OlderSlackBuild"
DIR="$DIRMIR/src_slackers"
get_git2
}
#####
get_phx(){
MIRRORGIT="https://github.com/PhantomX/slackbuilds"
DIR="$DIRMIR/src_phantomx"
get_git1
}
#####
get_sware(){
MIRRORGIT="https://github.com/eviljames/studioware"
DIR="$DIRMIR/src_studioware"
get_git1
}
#####
get_willysr(){
MIRRORGIT="https://github.com/willysr/SlackHacks/"
DIR="$DIRMIR/src_willysr/SlackHacks"
get_git1
}
#####
get_bormant(){
MIRRORGIT="https://github.com/bormant/SB"
DIR="$DIRMIR/src_bormant"
get_git1
}
#####
get_nk(){
MIRRORGIT="https://github.com/slacknk/slackware"
DIR="$DIRMIR/src_nk"
get_git1
}
#####
get_anddt(){
MIRRORGIT="https://github.com/AndDT/SlackBuilds"
DIR="$DIRMIR/src_anddt"
get_git1
}
#####
##
#


# Hello World!
hw(){
echo ""
echo " ═════════════════════════════════════════════════"
echo ""
echo "	$DISTRO - $ARCH - $VER"
echo ""
echo " ═════════════════════════════════════════════════"
echo ""
}
#hw

dv(){
#reset
#clear
echo ""
echo " ═════════════════════════════════════════════════"
echo ""
echo " $DISTRO - $ARCH - $VER"
echo ""
echo " ═════════════════════════════════════════════════"
echo ""
echo -e "\n\n\n\n\n\n\n"
  #lsusb | while read i; do ... done
  for d in $DISTRO ; do 
     reset
   echo -e "\n\n\n\n$(tput setaf 1)❯$(tput setaf 4)❯$(tput setaf 3)❯ $(tput sgr0)$d - $VER -Begin\n\n"
    #get_$d || echo $d >> /tmp/slack-mirror-error.log
     get_$d || exit 1
   echo -e "\n\n$(tput setaf 1)❯$(tput setaf 4)❯$(tput setaf 3)❯ $(tput sgr0)$d - $VER - End\n\n\n"
    sleep 2s
  done
}


#
##
###
####
 [ "$ARCH" = "64" ] && LIB32="lib32" || LIB32=""
#DR="slack ktown sbrepos restricted boost salix mled slacky msb csb ponce xbmc rw " #$LIB32"
 DR="slack ktown sbrepos restricted boost salix mled slacky msb csb ponce xbmc rw " #$LIB32"
 DC="slack ktown ktown_testing sbrepos restricted msb " #$LIB32"
 DS="slack alien_src alphageek anddt boost bormant nk mled phx salix sbo sit slacky sware willysr xbmc"
####
###
##
#


# Distro
if [[ "$DISTRO" = "all" ]]; then
 [[ $VER == "current" ]] && DISTRO="$DC" dv
 [[ $VER == "14.1" ]] 	 && DISTRO="$DR" dv
 [[ $VER == "src" ]] 	 && DISTRO="$DS" dv


elif [[ "$DISTRO" = "slack" ]]; then
 get_slack
 
 
elif [[ "$DISTRO" = "alien" ]] && [[ $VER == "src" ]]; then
 get_alien_src
 get_ktown
 get_lib32
elif [[ "$DISTRO" = "alien_src" ]] && [[ $VER == "src" ]]; then
 get_alien_src
 get_ktown
 get_lib32
elif [[ "$DISTRO" = "alien" ]]; then
 get_ktown
 get_ktown_testing
 get_sbrepos
 get_restricted
 get_lib32 

elif [[ "$DISTRO" = "ktown" ]]; then
 get_ktown
elif [[ "$DISTRO" = "ktown_testing" ]]; then
 get_ktown_testing
elif [[ "$DISTRO" = "sbrepos" ]]; then
 get_sbrepos
elif [[ "$DISTRO" = "restricted" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "lib32" ]]; then
 get_$DISTRO



elif [[ "$DISTRO" = "msb" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "csb" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "willysr" ]]; then
 get_$DISTRO


elif [[ "$DISTRO" = "salix" ]]; then
 get_$DISTRO


elif [[ "$DISTRO" = "slacky" ]]; then
 get_$DISTRO
 
 
elif [[ "$DISTRO" = "mled" ]]; then
 get_$DISTRO
 
 
elif [[ "$DISTRO" = "sbo" ]]; then
 dv || get_$DISTRO
elif [[ "$DISTRO" = "xbmc" ]]; then
 get_xbmc
elif [[ "$DISTRO" = "ponce" ]]; then
 get_ponce
elif [[ "$DISTRO" = "phx" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "sit" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "rw" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "sware" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "alphageek" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "bormant" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "nk" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "anddt" ]]; then
 get_$DISTRO
elif [[ "$DISTRO" = "boost" ]]; then
 get_$DISTRO
 
 
else

 echo "-> and ?.."
 exit 1
 
fi


# This is The End
C="$(tput setaf 1)❯$(tput setaf 4)❯$(tput setaf 3)❯ $(tput sgr0)"
echo -e "\n\n\n═════════════════════════════════════════════════\n$C FINISH !"



#xterm -e "sudo su -c 'printf 'y' | slackpkg update && slackpkg upgrade-all && read enter'"
  
  
#VER0=$VER
#if [ "$VER" != "current" ]; then
# VER=""
#fi
#VER=$VER0


#wget -r -k -l 7 -p -E -nc http://www.slackers.it/ -P //***FULL*PATH***//repository-mirror/slackware64/
#wget -r -k -l 7 -p -E -nc http://www.slackel.gr/ -P //***FULL*PATH***//repository-mirror/slackware64/
#wget -qO- http://sourceforge.net/projects/dropline-gnome/files/Dropline%20Packages/3.10/x86_64/ | grep '.txz/download' | cut -d\" -f2 | while read uri ; do desiredpath=$(dirname $uri | sed s/sourceforge/downloads.sourceforge/ | sed s/projects/project/ | sed 's/files\///') && wget $desiredpath ; done



# w/o comments

tree_l_1 (){
#$ tree -L 1
.
├── 14.1
├── 14.1_alien-ktown
├── 14.1_alien-restricted
├── 14.1_alien-sbrepos
├── 14.1_boost
├── 14.1_mled
├── 14.1_ponce
├── 14.1_rlworkman
├── 14.1_salix
├── 14.1_sbo
├── 14.1_slacky
├── 14.1_slaxbmc
├── 14.1_willysr-csb
├── 14.1_willysr-msb
├── current
├── current_alien-ktown
├── current_alien-restricted
├── current_alien-sbrepos
├── current_boost
├── current_mled
├── current_willysr-csb
├── current_willysr-msb
├── src
├── src_alien
├── src_alphageek
├── src_anddt
├── src_boost
├── src_bormant
├── src_mled
├── src_nk
├── src_phantomx
├── src_ponce
├── src_rlworkman
├── src_salix
├── src_sbo
├── src_slackers
├── src_slacky
├── src_slaxbmc
├── src_studioware
└── src_willysr

40 directories, 0 files
}




# 2015-16 Written by NK
# You are free to modify or redistribute this in any way you wish.
# Use at your own risk, YMMV (Your mileage may vary), and all that.
# Repositories for slackpkgplus.conf < http://slakfinder.org/slackpkg+.html > .
