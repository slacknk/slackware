#!/bin/bash
 
DIRM="/home/ftp/slackmirror"
SLM="`dirname $0`/slack-mirror.sh"


# v: sources current 14.1
#if [ -d $DIRM ] && [ -x $SLM ]; then
# for v in src 14.2 ; do $SLM -m $DIRM -d all-all -v $v ; done
#else

#ARCH=32 $SLM -m ${DIRM}32/ -d slack -v current
#$SLM -m $DIRM$ARCH/ -d all-all -v 14.2


if [ -d $DIRM ] && [ -x $SLM ]; then

 # 0 - slack32 - 14.2
 ARCH=32 $SLM -m ${DIRM} -d slack -v 14.2

 # 1 - current32
 #ARCH=32 $SLM -m ${DIRM} -d slack -v current
 
 # 2 - sources
 $SLM -m $DIRM -d all -v src
 
 # 3 - release
 $SLM -m $DIRM -d all-all -v 14.2
 
else
 echo -e "Not Found, down\n" ; ls -d $DIRM 
 echo -e "" ; ls -ld $SLM
fi
sleep 15s
