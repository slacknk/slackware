#!/bin/sh

# 2016 Slackware build script written by NK
# You are free to modify or redistribute this in any way you wish.


for p in nk _SBo ; do
 date "+%Y-%m-%d %H:%M" > /tmp/packages.$p
 echo >> /tmp/packages.$p
 find /var/log/packages/ -name "*$p" | sed 's_/var/log/packages/__' | sort >> /tmp/packages.$p
done
