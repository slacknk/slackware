## 14.2 > Games > [mame](https://slackbuilds.org/repository/14.2/games/mame/): 0.185 | SlackBuild: [games/mame: 0.185](https://git.slackbuilds.org/slackbuilds/commit/?id=440e47959af816026ae14410b1e4263c08b92cbd)

* Source Downloads: [mame-mame0185.tar.gz](https://github.com/mamedev/mame/archive/mame0185/mame-mame0185.tar.gz) (5eed1512cb8323f03779954b47b80b2e)

This requires: SDL2_ttf -> SDL2
***

#### 14.2 > Libraries > [SDL2 (2.0.8)](https://slackbuilds.org/repository/14.2/libraries/SDL2/)

* Source Downloads: [SDL2-2.0.8.tar.gz](https://www.libsdl.org/release/SDL2-2.0.8.tar.gz) (3800d705cef742c6a634f202c37f263f)

#### 14.2 > Libraries > [SDL2_ttf (2.0.14)](https://slackbuilds.org/repository/14.2/libraries/SDL2_ttf/)

* Source Downloads: [SDL2_ttf-2.0.14.tar.gz](https://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-2.0.14.tar.gz) (e53c05e1e7f1382c316afd6c763388b1)

***


## 14.2 > Audio > [clementine](https://slackbuilds.org/repository/14.2/audio/clementine/): 1.3.1

* Source Downloads: [1.3.1.tar.gz](https://github.com/mamedev/mame/archive/mame0185/mame-mame0185.tar.gz) (04ce1c102841282e620d4caae2a897a2)

* SlackBuild: [audio/clementine: 1.3.1](https://git.slackbuilds.org/slackbuilds/commit/?id=7540b0eef08bded1a383e92c95c02739b2b8403a)


## 14.2 > System > [virtualbox-kernel](https://slackbuilds.org/repository/14.2/system/virtualbox-kernel/): 5.0.40 | SlackBuild: [system/virtualbox-kernel](https://slackbuilds.org/slackbuilds/14.2/system/virtualbox-kernel.tar.gz)

* Source Downloads: [virtualbox-kernel-5.0.40.tar.xz](http://www.liwjatan.at/files/src/virtualbox-kernel/virtualbox-kernel-5.0.40.tar.xz) (475de2d0a915ce7c0c623ac50696bb79)
