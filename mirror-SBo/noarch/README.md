```
sbopkg -V 14.1 -Bi numix-icon-theme-bevel
sbopkg -V 14.1 -Bi gnome-colors
sbopkg -V 14.0 -Bi cheser-icon-theme
sbopkg -V 14.0 -Bi rodent-icon-theme
sbopkg -V 13.0 -Bi industrial-cursor-theme
sbopkg -V 12.0 -Bi tango-icon-theme
sbopkg -V 11.0 -Bi tango-icon-theme
```

## 14.1 > Desktop > [numix-icon-theme-bevel (1.0.20141021)](https://slackbuilds.org/repository/14.1/desktop/numix-icon-theme-bevel/)

* Source Downloads: [numix-icon-theme-bevel_1.0+201410212340~8~ubuntu14.10.1.tar.xz](https://launchpad.net/~numix/+archive/ubuntu/ppa/+files/numix-icon-theme-bevel_1.0+201410212340~8~ubuntu14.10.1.tar.xz) (9d2554321cb70405cdb84616a256056b)

* mirror - numix-icon-theme-bevel_1.0+201410212340~8~ubuntu14.10.1.tar.xz: [Packages in “PPA for the Numix Project Ltd.”](https://code.launchpad.net/~numix/+archive/ubuntu/ppa/+packages)


## 14.1 > Desktop > [gnome-colors (5.5.1)](https://slackbuilds.org/repository/14.1/desktop/gnome-colors/)

* Source Downloads: [gnome-colors-5.5.1.tar.gz](http://gnome-colors.googlecode.com/files/gnome-colors-5.5.1.tar.gz) (8ec81b556bac351817bd56a1701dbbfb)
  * [gnome-colors-extras-5.5.1.tar.gz](http://gnome-colors.googlecode.com/files/gnome-colors-extras-5.5.1.tar.gz) (02132ab0483c54a45ac49df22f90c163)
  * [shiki-colors-4.6.tar.gz](http://gnome-colors.googlecode.com/files/shiki-colors-4.6.tar.gz) (b0e5e6d20132ae46a3114ee357f9ec24)

* mirror: gnome-colors-5.5.1.tar.gz: [OSDN](https://osdn.net/projects/sfnet_openiconlibrary/downloads/3rd_party/gnome-colors-5.5.1.tar.gz/) -> [Open Icon Library - 3rd_party](https://sourceforge.net/projects/openiconlibrary/files/3rd_party/)
  * gnome-colors-extras-5.5.1.tar.gz: [www7.frugalware.org](http://www7.frugalware.org/pub/frugalware/frugalware-stable/source/gnome-extra/gnome-colors-extras-icon-theme/) + [ftp.fr.freebsd.org](http://ftp.fr.freebsd.org/pub/frugalware/frugalware-current/source/gnome-extra/gnome-colors-extras-icon-theme/)
  * shiki-colors-4.6.tar.gz: [The Core Project - Tiny Core Linux](http://tinycorelinux.net/2.x/tcz/src/shiki-colors/)


## 14.0 > Desktop > [cheser-icon-theme (3.8.0)](https://slackbuilds.org/repository/14.0/desktop/cheser-icon-theme/)

* Source Downloads: [cheser-icon-theme-3.8.0.tar.gz](http://ponce.cc/slackware/sources/repo/cheser-icon-theme-3.8.0.tar.gz) (0e86e06e37bb4f2850bee7ed1226c4f0)

* mirror - cheser-icon-theme-3.8.0.tar.gz: [Ponce's repo](http://ponce.cc/slackware/sources/repo/)


## 14.0 > Desktop > [rodent-icon-theme (5.0)](https://slackbuilds.org/repository/14.0/desktop/rodent-icon-theme/)

* Source Downloads: [rodent-icon-theme-5.0.tar.gz](http://downloads.sourceforge.net/xffm/rodent-icon-theme-5.0.tar.gz) (4b097d344a160d6497c6498985c8df15)

* mirror - rodent-icon-theme-5.0.tar.gz: [Rodent applications](https://sourceforge.net/projects/xffm/files/rodent-icon-theme/)


## 13.0 > Desktop > [industrial-cursor-theme (0.6.1.3)](https://slackbuilds.org/repository/13.0/desktop/industrial-cursor-theme/)

* Source Downloads: [industrial-cursor-theme_0.6.1.3.tar.gz](http://ftp.de.debian.org/debian/pool/main/i/industrial-cursor-theme/industrial-cursor-theme_0.6.1.3.tar.gz) (c53c19b134dcb9c0b0c22f70832668d7)

* mirror - industrial-cursor-theme_0.6.1.3.tar.gz: [repository for old Debian releases](http://archive.debian.org/debian/pool/main/i/industrial-cursor-theme/)


## Desktop > [tango-icon-theme](https://slackbuilds.org/result/?search=tango-icon-theme&sv=) | mirror: [Tango Desktop Project](http://tango.freedesktop.org/releases/)

* 12.0 > Desktop > [tango-icon-theme (0.8.1)](https://slackbuilds.org/repository/12.0/desktop/tango-icon-theme/)
  * Source Downloads: [tango-icon-theme-0.8.1.tar.gz](http://tango-project.org/releases/tango-icon-theme-0.8.1.tar.gz) (32d5258f448b5982af9cfa4364f31d41)

* 11.0 > Desktop > [tango-icon-theme (0.8.8)](https://slackbuilds.org/repository/11.0/desktop/tango-icon-theme/)
  * Source Downloads: [tango-icon-theme-0.8.0.tar.gz](http://tango-project.org/releases/tango-icon-theme-0.8.0.tar.gz) (feb1870e84b99ea41db9b51a448c59f8)

***
