```
sbopkg -V 13.37 -Bi nestopia
```

## 13.37 > Games > [nestopia (1.40h)](https://slackbuilds.org/repository/13.37/games/nestopia/)

* Source Downloads: 
  * [Nestopia140src.zip](http://downloads.sourceforge.net/project/nestopia/Nestopia/v1.40/Nestopia140src.zip) (526c99a06d2b257135e7047b0ed95ae0)
  * [nst140_lnx_release_h.zip](http://rbelmont.mameworld.info/nst140_lnx_release_h.zip) (f9a9a905bada67e11dac1364612d0b35)
