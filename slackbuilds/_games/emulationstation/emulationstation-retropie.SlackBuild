#!/bin/sh

# Slackware build script for EmulationStation/RetroPie

# Copyright 2018-19 NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


PRGNAM=emulationstation-retropie
#VERSION=${VERSION:-2.7.1}
#ARCH=noarch
BUILD=${BUILD:-1}
TAG=${TAG:-nk}


if [ -z "$ARCH" ]; then
 case "$( uname -m )" in
   i686) ARCH=i686 ;;
   i?86) ARCH=i586 ;;
      *) ARCH=$( uname -m ) ;;
 esac
fi

if [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi


[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`
TMP=${TMP:-/tmp/SBo/$PRGNAM}
PKG=$TMP/package-$PRGNAM
rm -rf $PKG || sudo su -c "rm -rf $PKG" || /bin/su -c "rm -rf $PKG"
mkdir -p $PKG 

set -e
cd $TMP


get_git (){
 DIR=${PRGNAM_}.git ;  echo -e "\n\n$DIR"
 GIT=https://github.com/RetroPie/${PRGNAM_}
 [ -d "$TMP/$DIR" ] && cd $DIR && git fetch origin && rm -rf * && git reset --hard origin/master
 [ ! -d "$TMP/$DIR" ] && git clone $GIT $DIR && cd $DIR
}


#
# RetroPie/EmulationStation
# https://github.com/RetroPie/EmulationStation/
if [ -z $VERSION ]; then
 ESRP="$TMP/EmulationStation.git_submodule"
 rm -rf $ESRP
 if [ ! -d ${ESRP/_submodule/} ]; then
  git clone --recursive https://github.com/RetroPie/EmulationStation.git ${ESRP/_submodule/}
 fi
 cp -a ${ESRP/_submodule/} $ESRP
 cd $ESRP
 git submodule update --init
 GITVER=$(git describe --tags | sed -e 's/^v//' | sed -r 's/-.+//')
 GITREV=$(git describe --tags | sed -e "s/^.*-\([0-9]\+\)-.*$/\1/")
 GITDAT=$(git log -n1 --date=short --pretty=format:'%ad' | sort -r | sed 's/-//g')
 GITCOM=$(git rev-parse --short HEAD)
 VERSION="${GITVER}.${GITREV}_git${GITDAT}.${GITCOM}"
else
 SRCWWW=https://github.com/RetroPie/EmulationStation/archive
 SRCTAR=v$VERSION.tar.gz
 [ -f $SRCTAR ] || wget  --user-agent='WGET' -c $SRCWWW/$SRCTAR
 rm -rf EmulationStation-$VERSION
 tar xvf $SRCTAR
 cd EmulationStation-$VERSION
 if [ "$VERSION" = "2.7.1" ]; then
  rm -rf external/pugixml
  git clone https://github.com/zeux/pugixml.git external/pugixml
 fi
fi


if [ -f /var/log/packages/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG ]; then
 echo -e "\n\nNot found newest;"
 echo -e "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG - this version already installed\n"
 #rm -rfv $PKG ; exit 1
else
 echo -e "\n\n\n"; PKGLOG="/var/log/packages/$PRGNAM-*"
 if [ -f $PKGLOG ]; then
  echo -e "-`ls $PKGLOG | sed "s|/var/log/packages/$PRGNAM-||;s|-$ARCH-.*||"`"
 fi
 echo -e "+$VERSION"
 echo -e "\n\n\n"
fi


# Default 'Carbon' theme
# https://github.com/RetroPie/RetroPie-Setup/wiki/EmulationStation-Advanced-Theming
# https://github.com/RetroPie/RetroPie-Setup/wiki/Creating-Your-Own-EmulationStation-Theme
cd $TMP ; DIR=carbon ;  echo -e "\n\n$DIR"
GIT=https://github.com/RetroPie/es-theme-${DIR}
[ -d "$TMP/$DIR" ] && cd $DIR && git fetch origin && rm -rf * && git reset --hard origin/master
[ ! -d "$TMP/$DIR" ] && git clone $GIT $DIR && cd $DIR
ESRP_THEME_DATE=$(git log -n1 --date=short --pretty=format:'%ad' | sort -r | sed 's/-//g')
ESRP_THEME=${DIR}


# Icon for ES from RetroPie-Setup
if [ ! -f $TMP/rpsetup.png ]; then
 WWWRPs="https://raw.githubusercontent.com/RetroPie/RetroPie-Setup/master"
 wget -c ${WWWRPs}/scriptmodules/supplementary/retropiemenu/icons/rpsetup.png -P $TMP
fi



# permissions
cd $TMP ; test $(id -u) -eq 0 && chown -R root:root . ; find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;


#
# EmulationStation/RetroPie Theme
# 
cd $TMP/$ESRP_THEME
mkdir -p $PKG/etc/emulationstation/themes/${ESRP_THEME}_${ESRP_THEME_DATE}
install -Dm 644 readme.txt $PKG/usr/doc/$PRGNAM-$VERSION/THEME-${ESRP_THEME}_readme.txt
mv * $PKG/etc/emulationstation/themes/${ESRP_THEME}_${ESRP_THEME_DATE}


#
# EmulationStation/RetroPie
#
cd $ESRP
CMAKEPREFIX="`kde4-config --prefix`" || CMAKEPREFIX="/usr"
cmake \
 \
 -DCMAKE_C_FLAGS:STRING="$SLKCFLAGS" \
 -DCMAKE_CXX_FLAGS:STRING="$SLKCFLAGS" \
 \
 -DCMAKE_INSTALL_PREFIX=$CMAKEPREFIX \
 -DCMAKE_INSTALL_MANDIR=/usr/man \
 -DCMAKE_INSTALL_LIBDIR=lib${LIBDIRSUFFIX} \
 \
 -DCMAKE_BUILD_TYPE=Release . || exit 1

x=$(cat /proc/cpuinfo | grep processor | wc -l)
[ -n "$x" ] && let "NUMJOBS=x+1" || NUMJOBS=2
make -j${NUMJOBS} || exit 1


ESRP_DIR="$PKG/etc/emulationstation/retropie"
if [ "$VERSION" = "2.7.1" ]; then
 install -Dm755 "emulationstation" "$PKG/usr/bin/emulationstation-retropie"
else
 mkdir -p ${ESRP_DIR}
 install -Dm755 emulationstation ${ESRP_DIR}
 cp -a resources ${ESRP_DIR}
 # don't work from PATHs
 #mkdir -p $PKG/usr/bin/
 #ln -s /etc/emulationstation/reptropie/emulationstation $PKG/usr/bin/emulationstation-reptropie
fi

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true


#
# EmulationStation/RetroPie Desktop
#

#
# png
#cp -v $TMP/rpsetup.png ${ESRP_DIR}
#mkdir -p $PKG/usr/share/pixmaps
#ln -sv ${ESRP_DIR/$PKG/}/rpsetup.png $PKG/usr/share/pixmaps/emulationstation.png
install -Dm644 $TMP/rpsetup.png ${ESRP_DIR}/rpsetup.png

#
# desktop
mkdir -p $PKG/usr/share/applications
cat << EOF > $PKG/usr/share/applications/${PRGNAM}.desktop
[Desktop Entry]
Name=EmulationStation-RetroPie
GenericName=RetroPie/EmulationStation
Comment=A Fork of Emulation Station for RetroPie
Exec=${ESRP_DIR/$PKG/}/emulationstation
Icon=${ESRP_DIR/$PKG/}/rpsetup.png
Terminal=false
Type=Application
Categories=Game;Emulator;

[Desktop Action Windowed]
Name=Open as window
Exec=${PRGNAM} --windowed
OnlyShowIn=Unity;GNOME;

[Desktop Action NoExit]
Name=Don't display exit button
Exec=${PRGNAM} --no-exit
OnlyShowIn=Unity;GNOME;

[Desktop Action Vsync off]
Name=Open in standalone mode
Exec=${PRGNAM} -vsync 0
OnlyShowIn=Unity;GNOME;
EOF


#
# EmulationStation/RetroPie Docs
#
mkdir -pv $PKG/usr/doc/$PRGNAM-$VERSION ; D=""
D+="AUTHORS COPYING* CHANGELOG* copying* ChangeLog Changelog *changelog* CONTRIBUTORS "
D+="Credit* CREDIT* DEPENDENCIES FAQ faq* gpl* lgpl* HACKING HISTORY* INSTALL KDE4FAQ "
D+="LICENSE* *license* MAINTAINERS NEWS *Preview* *preview* PKG-INFO README* readme*  "
D+="Resources* THANKS* Thanks* TODO ToDo TRANSLATORS VERSION version* WhatsNew* "
D+="*.rules debian/changelog debian/control debian/copyright "
DOCS=""; for d in ${D}; do [ ! -z "`ls $d`" ] && DOCS+="$d "; done
[[ ! -z $DOCS ]] && mv -v $DOCS emulationstation.sh $PKG/usr/doc/$PRGNAM-$VERSION/ || exit 1
md="`ls *.md`" ; [ ! -z "$md" ] && mv -v $md $PKG/usr/doc/$PRGNAM-$VERSION/

cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
find $PKG/usr/doc/$PRGNAM-$VERSION/ -type f -exec chmod 644 {} \;
find $PKG/usr/doc/$PRGNAM-$VERSION/ -type d -exec chmod 755 {} \;



mkdir -p $PKG/install
cat << EOF > $PKG/install/slack-desc
       |-----handy-ruler------------------------------------------------------|
appname: emulationstation-retropie (A Fork of Emulation Station for RetroPie)
appname:
appname: This is a fork of EmulationStation for RetroPie. EmulationStation is
appname: a cross-platform graphical front-end for emulators with controller
appname: navigation.
appname:
appname: EmulationStation: https://emulationstation.org/
appname:  https://github.com/Aloshi/EmulationStation  
appname:
appname: RetroPie/EmulationStation: https://retropie.org.uk/
appname:  https://github.com/RetroPie/EmulationStation
EOF
sed -e '1d' -e 's/appname:/'${PRGNAM}':/' -i $PKG/install/slack-desc


# install/doinst.sh // if [[ -d $PKG/usr/share/icons/hicolor ]]; then
if [[ -f $PKG/usr/share/applications/${PRGNAM}.desktop ]]; then
 DOINST="$PKG/install/doinst.sh"
 echo -e "\nif [ -e usr/share/icons/hicolor/icon-theme.cache ]; then" >> $DOINST
 echo -e " if [ -x /usr/bin/gtk-update-icon-cache ]; then" >> $DOINST
 echo -e " /usr/bin/gtk-update-icon-cache -f usr/share/icons/hicolor >/dev/null 2>&1" >> $DOINST
 echo -e " fi\nfi\n" >> $DOINST
fi


#cd $PKG
#/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}


OUTPUT=${OUTPUT:-$CWD} ; [ $OUTPUT != $CWD ] && mkdir -pv $OUTPUT
PKGTYPE=${PKGTYPE:-txz}
PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
[ -f $PACKAGE ] && rm -vf $PACKAGE

ROOTCOMMAND1="cd $PKG ; /sbin/makepkg -l y -c n $PACKAGE"
ROOTCOMMAND2="cd $CWD ; /sbin/upgradepkg --install-new --reinstall $PACKAGE"
ROOTCOMMAND3="rm -rf $PKG"
ROOTCOMMAND4="[ "$TMP" != "/tmp" ] && rm -rf $TMP"

ROOTCOMMANDS="$ROOTCOMMAND1 && $ROOTCOMMAND2 && $ROOTCOMMAND3 && $ROOTCOMMAND4"
ROOTCOMMANDSU="cd $PKG ; chown -R root:root . && $ROOTCOMMANDS"

if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMANDS"
else
 for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ; echo
 echo -e "\n\n\n\e[1mPlease enter your admin password for makepkg and installpkg\e[0m"
#sudo su -c "$ROOTCOMMANDSU" || /bin/su -c "$ROOTCOMMANDSU"
 su_sudo (){ echo -e "\n1) Enter passw for sudo (sudo su -c)"; sudo su -c "$ROOTCOMMANDSU" ; }
 su_root (){ echo -e "\n2) Enter root's passw 	(/bin/su -c)"; /bin/su -c "$ROOTCOMMANDSU" ; }
 su_sudo || su_root
fi
