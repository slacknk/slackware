## NOTES!
### vbam_1.8.0.x

* Don't work save and load state in Fullscreen mode (F11)
* Don't work GBA-mode (Menu: Options - Emulated system) in GBC games with gba enhanced
  * Examplaes: Wendy - Every Witch Way, Shantae.
  * Solved: Menu: Options - Emulated system: Super Game Boy 2
