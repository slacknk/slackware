#!/bin/sh

# Slackware build script for ePSXe

# Copyright 2017-18 NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# Notes! _slack14.2 - epsxe_2.0.x
# shared libraries: libtinfo.so.5:
# slackpkg install alien:libtinfo


PRGNAM=epsxe
#VERSION=${VERSION:-2.0.5}
BUILD=${BUILD:-3}
TAG=${TAG:-nk}


if [ -z "$ARCH" ]; then
 case "$( uname -m )" in
   i?86) ARCH=x86 ;;
      *) ARCH=$( uname -m ) ;;
 esac
fi


if [ "$ARCH" = "x86_64" ]; then
 eARCH="_x64" || eARCH=""
 LIBDIRSUFFIX="64"
elif [ "$ARCH" = "x86" ]; then
 eARCH=""
 LIBDIRSUFFIX=""
else
 exit 1
fi


[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`
TMP=${TMP:-/tmp/SBo/$PRGNAM}
PKG=$TMP/package-$PRGNAM
rm -rf $PKG || sudo su -c "rm -rf $PKG" || /bin/su -c "rm -rf $PKG"
mkdir -p $PKG 

set -e
cd $TMP



if [ -z $VERSION ]; then
 WWWAAUR=https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h
 wget -c ${WWWAAUR}=${PRGNAM} -O ${PRGNAM}.PKGBUILD || exit 1
 . ./${PRGNAM}.PKGBUILD
 VERSION="${pkgver}"
 rm -fv "${PRGNAM}.PKGBUILD"
 [ -z $VERSION ] && exit 1
fi

if [ -f /var/log/packages/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG ]; then
 echo -e "\n\nNot found newest;"
 echo -e "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG - this version already installed\n"
 rm -rfv $PKG ; [ $TMP = /tmp/SBo/$PRGNAM ] && rm -rf $TMP
 exit 1
else
 echo -e "\n\n\n"; PKGLOG="/var/log/packages/$PRGNAM-*"
 if [ -f $PKGLOG ]; then
  echo -e "-`ls $PKGLOG | sed "s|/var/log/packages/$PRGNAM-||;s|-$ARCH-.*||"`"
 fi
 echo -e "+$VERSION"
 echo -e "\n\n\n"
fi

SRCWWW=http://www.epsxe.com/files
SRCTAR=ePSXe${VERSION//./}.zip
[ -f $SRCTAR ] || wget  --user-agent='WGET' -c $SRCWWW/$SRCTAR
rm -rf "$PRGNAM-$VERSION"_win
mkdir -p "$PRGNAM-$VERSION"_win
cd "$PRGNAM-$VERSION"_win
 set +e
  unzip ../$SRCTAR
  7z x ePSXe.exe -o$VERSION
 set -e
 mkdir -p $PKG/usr/share/icons/hicolor/{16x16,32x32}/apps
 convert ./$VERSION/.rsrc/1033/ICON/5.ico $PKG/usr/share/icons/hicolor/16x16/apps/epsxe.png
 convert ./$VERSION/.rsrc/1033/ICON/7.ico $PKG/usr/share/icons/hicolor/32x32/apps/epsxe.png
cd -
rm -rf "$PRGNAM-$VERSION"_win


SRCWWW=http://www.epsxe.com/files
SRCTAR=ePSXe${VERSION//./}linux${eARCH}.zip
[ -f $SRCTAR ] || wget  --user-agent='WGET' -c $SRCWWW/$SRCTAR
rm -rf $PRGNAM-$VERSION
mkdir -p $PRGNAM-$VERSION
cd $PRGNAM-$VERSION
unzip ../$SRCTAR



test $(id -u) -eq 0 && chown -R root:root .

find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;


mkdir -p $PKG/opt/$PRGNAM
mv -v * $PKG/opt/$PRGNAM
cd ..
rm -rf $PRGNAM-$VERSION

mv $PKG/opt/$PRGNAM/ePSXe${eARCH} $PKG/opt/$PRGNAM/${PRGNAM}${eARCH}
chmod +x $PKG/opt/$PRGNAM/${PRGNAM}${eARCH}

mkdir -p $PKG/usr/bin/
ln -sfv /opt/$PRGNAM/${PRGNAM}${eARCH} $PKG/usr/bin/

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp $PKG/opt/$PRGNAM/docs/*_linux* $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
find $PKG/usr/doc/$PRGNAM-$VERSION/ -type f -exec chmod 644 {} \;


# icon.desktop
mkdir -p $PKG/usr/share/applications || exit 1
cat << EOF > $PKG/usr/share/applications/$PRGNAM.desktop
[Desktop Entry]
Type=Application
Exec=/opt/$PRGNAM/${PRGNAM}${eARCH}
Icon=epsxe
Name=ePSXe
GenericName=Enhanced PSX emulator
Comment=Enhanced PSX Emulator
Categories=Game;Emulator;
EOF



# epsxe-plugin-gpu-soft
epsxe_plugin (){
mkdir -p $TMP/epsxe-plugin-gpu-soft
cd $TMP/epsxe-plugin-gpu-soft
for p in gpupeopssoftx118.tar.gz petegpucfg_V2-9_V1-77_V1-18.tar.gz ; do
 [ -f $p ] || wget  --user-agent='WGET' -c http://www.pbernert.com/$p -P $TMP
 tar xvf ../$p
done

srcdir=$TMP/epsxe-plugin-gpu-soft
pkgdir=$PKG

    install -d "$pkgdir"/opt/epsxe/{cfg,plugins}
    install -t "$pkgdir"/opt/epsxe/cfg $(find ./ -name 'cfg*' -not -type d -print) $(find ./ -name '*.cfg' -not -type d -print)
    install -t "$pkgdir"/opt/epsxe/plugins $(find ./ -name '*.so.*' -print)

    # permissions
    chmod 775 "$pkgdir"/opt/epsxe/{cfg,plugins}
    #chown root:games "$pkgdir"/opt/epsxe/cfg
    #chown root:games "$pkgdir"/opt/epsxe/plugins
    chmod 664 "$pkgdir"/opt/epsxe/cfg/gpuPeopsSoftX.cfg
    chmod 755 "$pkgdir"/opt/epsxe/cfg/cfgPeopsSoft
    chmod 755 "$pkgdir"/opt/epsxe/plugins/libgpuPeopsSoftX.so.1.0.18
}
#epsxe_plugin
#exit 1



mkdir -p $PKG/install
cat << EOF > $PKG/install/slack-desc
       |-----handy-ruler------------------------------------------------------|
appname: epsxe (ePSXe: A Sony PlayStation Emulator for the PC)
appname:
appname: ePSXe is a Sony Playstation emulator for your PC running under
appname: Android, Winxp/10, MacOSX or linux. It takes advantage of the
appname: popular PSEmu Pro plugin system.
appname:
appname: EPSXE Copyright 2000/2016 EPSXE team.
appname: Sony Playstation is registered trademark of Sony.
appname:
appname: Homepage: http://www.epsxe.com
appname:
EOF
sed -e '1d' -e 's/appname:/'${PRGNAM}':/' -i $PKG/install/slack-desc


#cd $PKG
#/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}


OUTPUT=${OUTPUT:-$CWD}
PKGTYPE=${PKGTYPE:-txz}
PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
[ -f $PACKAGE ] && rm -vf $PACKAGE

ROOTCOMMAND1="cd $PKG ; /sbin/makepkg -l y -c n $PACKAGE"
ROOTCOMMAND2="cd $CWD ; /sbin/upgradepkg --install-new --reinstall $PACKAGE"
ROOTCOMMAND3="rm -rf $PKG"
ROOTCOMMAND4="[ "$TMP" != "/tmp" ] && rm -rf $TMP"

ROOTCOMMANDS="$ROOTCOMMAND1 && $ROOTCOMMAND2 && $ROOTCOMMAND3 && $ROOTCOMMAND4"
ROOTCOMMANDSU="cd $PKG ; chown -R root:root . && $ROOTCOMMANDS"

if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMANDS"
else
 for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ; echo
 echo -e "\n\n\n\e[1mPlease enter your admin password for makepkg and installpkg\e[0m"
#sudo su -c "$ROOTCOMMANDSU" || /bin/su -c "$ROOTCOMMANDSU"
 su_sudo (){ echo -e "\n1) Enter passw for sudo (sudo su -c)"; sudo su -c "$ROOTCOMMANDSU" ; }
 su_root (){ echo -e "\n2) Enter root's passw 	(/bin/su -c)"; /bin/su -c "$ROOTCOMMANDSU" ; }
 su_sudo || su_root
fi
