# Slackware-version - category - appname - version in SBo

## 14.2
###### Desktop
* misc
  * arandr - 0.1.7.9
  * gxkb - 0.7.9

## 14.1
###### Desktop
* misc
  * arandr - 0.1.7.1
  * gxkb - 0.7.6
* kde4
  * plasma-adjustable-clock - 4.1.4
  * plasma-eyasdp - 1.2.0
  * plasma-geek-clock - 1.0
