#!/bin/sh

# Written by NK since 2016
# You are free to modify or redistribute this in any way you wish.

# *sh-script for auto build and upgrade packages
# of Nk's slackbuilds-repository


reset
CWD=`dirname $0`
CWD_=`dirname $0`

[ "`uname -m`" == "x86_64" ] && SLACKARCH="64" || SLACKARCH="32"
. /etc/os-release ; SLACKVER=$VERSION
OUTPUT=$CWD/../repository/2night_${SLACKVER}_${SLACKARCH}
mkdir -p  $OUTPUT
[ -d $OUTPUT ] && export OUTPUT="$OUTPUT" || exit 1

export TMP=/tmp/SBo/2night
mkdir -p $TMP

cd $TMP
pwd
echo ""


build_n (){
 cd $PRGDIR || exit 1
 echo -e "\n\n\n\n\n\n"
 if [ ! -z $PRGNsb ] && [ ! -z $PRGNAM ]; then
   echo -e "\n\n\n\n\n\n>>> ${PRGNsb}: ${PRGNAM}" && pwd
   PRGNAM=${PRGNAM} ./${PRGNsb}.SlackBuild
   PRGNsb=
   PRGNAM=
 elif [ ! -z $PRGNsb ]; then
  echo -e "\n\n\n\n\n\n>>> ${PRGNsb}" && pwd
  ./${PRGNsb}.SlackBuild
   PRGNsb=
 else
  echo -e "\n\n\n\n\n\n>>> ${PRGNAM}" && pwd
  if [ -x ${PRGNAM}.SlackBuild ]; then ./${PRGNAM}.SlackBuild
  else echo -e "> where ${PRGNAM}.SlackBuild ?\n"
  exit 1
  fi
  PRGNAM=
 fi
 echo -e "\nGo to and current dir:"
 #cd - && echo -e
 cd $TMP/.. && pwd && echo -e
}


PRGNAM=vertex-icon-theme 	PRGDIR=$CWD/_themes-icon/0noconts/ 		build_n

#####
###
# icons
PRGNAM=flat-remix-icon-theme 	PRGDIR=$CWD/_themes-icon/flat-remix-icon-theme 	build_n
PRGNAM=hedera-icon-theme 	PRGDIR=$CWD/_themes-icon/$PRGNAM 		build_n
PRGNAM=buuf-icon-theme 		PRGDIR=$CWD/_themes-icon/buuf-icon-theme 	build_n
PRGNAM=la-capitaine-icon-theme 	PRGDIR=$CWD/_themes-icon/$PRGNAM 		build_n
PRGNAM=nitrux-icon-theme-flattr-luv PRGDIR=$CWD/_themes-icon/_nitrux-icons/ 	build_n

# icons-distro
PRGNAM=manjaro-icons 		PRGDIR=$CWD/_themes-icon/_$PRGNAM 		build_n
PRGNAM=faenza-bunsen-icon-theme PRGDIR=$CWD/_themes-icon/_faenza-style 		build_n
PRGNsb=mint-icons PRGNAM=mint-x-icons PRGDIR=$CWD/_themes-icon/_faenza-style 	build_n

# icons-noconts
PRGNAM=vertex-icon-theme 	PRGDIR=$CWD/_themes-icon/0noconts/ 		build_n

#####
##
# media
PRGNAM=smplayer 		PRGDIR=$CWD/_media-video/smplayer 		build_n

#####
##
# games
PRGNAM=epsxe 			PRGDIR=$CWD/_games/$PRGNAM 			build_n
PRGNAM=antimicro 		PRGDIR=$CWD/_games/$PRGNAM  			build_n
PRGNAM=scummvm 			PRGDIR=$CWD/_games/$PRGNAM 			build_n
PRGNAM=puNES 			PRGDIR=$CWD/_games/$PRGNAM  			build_n
PRGNAM=mgba 			PRGDIR=$CWD/_games/$PRGNAM 			build_n

#####
##
# misc




#####################
#####################
#####################

#rm -rf $TMP
#rm -rf /tmp/SBo/2night

echo -e "\n\n\n\n\n\n\n\n"

cd $CWD_ ; rm -rf /tmp/SBo/2night
rmdir $OUTPUT || ls  $OUTPUT -1lh



#####################
#####################
#####################
##
## OldStuff
##
#####################
#####################
#####################
sbo_up (){
	
	
WWWARCH=https://projects.archlinux.org/svntogit/community.git/plain/trunk/PKGBUILD?h=packages
WWWAAUR=https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h
WWWLPAD=http://ppa.launchpad.net
	
	
	
	
	
	
	opera_b (){
 #
 # Opera stable | developer
 for o in next stable ; do
  OPERA=$o PRGNsb=opera PRGDIR=$CWD/_net-browser/opera build_n
 done
 #
 # Opera-ffmpeg-codecs
 PRGNAM=opera-ffmpeg-codecs-bin 	PRGDIR=$CWD/_net-browser/opera/ build_n
}
#opera_b
#OPERA=developer PRGNsb=opera 	PRGDIR=$CWD/_net-browser/opera 	build_n
	
	
	
	
	
# Thunar-git
echo -e "\n\n\n\n\n\n\n"
#cd $CWD/_xfce4/Thunar/ || exit 1
#pwd
#./Thunar.SlackBuild
	
	
	
	
b_qb (){
#for PRGNAM in libtorrent-rasterbar qbittorrent ; do
PRGNAM=qbittorrent
echo -e "\n\n\n\n${PRGNAM}"
VERSION=$(\
 wget -qO- \
 http://ppa.launchpad.net/qbittorrent-team/qbittorrent-stable/ubuntu/dists/devel/main/source/Sources.gz \
 | gzip -d | grep -A2 -x "Package: ${PRGNAM}" | grep "Version: " \
 | sed 's/Version: //'| sed -r 's/-.+//' \
)
if [ ! -z "`ls /var/log/packages/ | grep ${PRGNAM} | grep $VERSION`" ]; then
 echo -e "\nNot found newest:\n> $VERSION - this version already installed\n"
else
 echo -e "\n\n\n\n\nBuild $VERSION\n\n"
 cd $CWD/_net-bt/qbittorrent || exit 1
 pwd
 VERSION="$VERSION" ./${PRGNAM}.SlackBuild || exit 1
 cd -
fi
#done
}
#b_qb
	
	
	
# sardi-icons
echo -e "\n\n\n\n\n\n\n"
cd $CWD/_themes-icon/sardi-icon-theme/ || exit 1
pwd
./sardi-icon-theme.SlackBuild #|| exit 1
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
# flattr -> luv -icon-them
echo -e "\n\n\n\n\n\n\n"
cd $CWD/_themes-icon/luv_flattr-icon-theme/ || exit 1
pwd
#PRGNAM=flattr-icon-theme 	OUTPUT="$OUTPUT" ./luv-icon-theme.SlackBuild
 PRGNAM=luv-icon-theme 		./luv-icon-theme.SlackBuild

	
	
	
# snwh-icons
echo -e "\n\n\n\n\n\n\n"
cd $CWD/_themes-icon/snwh-icons/ || exit 1
pwd
for i in moka-icon-theme faba-icon-theme faba-mono-icons ; do
 echo -e "\n\n\n\n"
 PRGNAM=$i ./snwh-icons.SlackBuild
done
	
	
	
	
# numix-icons
echo -e "\n\n\n\n\n\n\n"
cd $CWD/_themes-icon/numix-icon-theme/ || exit 1
pwd
./numix-icon-theme.SlackBuild
	
	
	
	
# transmission
PRGNAM=transmission
echo -e "\n\n\n\n${PRGNAM}"
VERSION=$(\
 wget -qO- http://ppa.launchpad.net/transmissionbt/ppa/ubuntu/dists/devel/main/source/Sources.gz \
 | gzip -d | grep -A2 -x "Package: transmission"| grep "Version: " \
 | sed 's/Version: //'| sed -r 's/-.+//' \
)
[ -z $VERSION ] && exit 1
if [ ! -z "`ls /var/log/packages/ | grep ${PRGNAM} | grep $VERSION`" ]; then
 echo -e "\nNot found newest:\n> $VERSION - this version already installed\n"
else
 echo -e "\n\n\n\n\nBuild $VERSION\n\n"
 cd $CWD/transmission/2.92-2.84_SBo/ || exit 1
 pwd
 VERSION="$VERSION" ./${PRGNAM}.SlackBuild || exit 1
 cd -
fi
	
	
	
# viewnior
PRGNAM=viewnior
echo -e "\n\n\n\n${PRGNAM}"
wget -c $WWWARCH/${PRGNAM} -O ${PRGNAM}.PKGBUILD || exit 1
. ./${PRGNAM}.PKGBUILD
VERSION="${pkgver}"
rm -fv "${PRGNAM}.PKGBUILD"
[ -z $VERSION ] && exit 1
if [ ! -z "`ls /var/log/packages/ | grep ${PRGNAM} | grep $VERSION`" ]; then
 echo -e "\nNot found newest:\n> $VERSION - this version already installed\n"
else
 echo -e "\n\n\n\n\n\n\n"
 cd $CWD/_views/${PRGNAM}/ || exit 1
 pwd
 VERSION="$VERSION" ./${PRGNAM}.SlackBuild || exit 1
 cd -
fi
	
	
	
# djview4
PRGNAM=djview4
echo -e "\n\n\n\n${PRGNAM}"
wget -c $WWWARCH/${PRGNAM} -O ${PRGNAM}.PKGBUILD || exit 1
. ./${PRGNAM}.PKGBUILD
VERSION="${pkgver}"
rm -fv "${PRGNAM}.PKGBUILD"
[ -z $VERSION ] && exit 1
if [ ! -z "`ls /var/log/packages/ | grep ${PRGNAM} | grep $VERSION`" ]; then
 echo -e "\nNot found newest:\n> $VERSION - this version already installed\n"
else
 echo -e "\n\n\n\n\n\n\n"
 cd $CWD/_views/${PRGNAM}/
 pwd
 VERSION="$VERSION" ./${PRGNAM}.SlackBuild || exit 1
 cd -
fi
	
	
	
# qpdfview
PRGNAM=qpdfview
echo -e "\n\n\n\n${PRGNAM}"
wget -c ${WWWAAUR}=${PRGNAM} -O ${PRGNAM}.PKGBUILD || exit 1
. ./${PRGNAM}.PKGBUILD
VERSION="${pkgver}"
rm -fv "${PRGNAM}.PKGBUILD"
[ -z $VERSION ] && exit 1
if [ ! -z "`ls /var/log/packages/ | grep ${PRGNAM} | grep $VERSION`" ]; then
 echo -e "\nNot found newest:\n> $VERSION - this version already installed\n"
else
 echo -e "\n\n\n\n\n\n\n"
 cd $CWD/_views/${PRGNAM}/latest/ || exit 1
 pwd
 VERSION="$VERSION" ./${PRGNAM}.SlackBuild || exit 1
 cd -
fi
	
	
	
# PaleMoon
PRGNAM=palemoon
echo -e "\n\n\n\n${PRGNAM}"
cd $CWD/_net-browser/palemoon/ || exit 1
pwd
./palemoon-bin.SlackBuild 
	
	
	
	
	
	
# terminator
PRGNAM=terminator
echo -e "\n\n\n\n${PRGNAM}"
wget -c $WWWARCH/${PRGNAM} -O ${PRGNAM}.PKGBUILD || exit 1
. ./${PRGNAM}.PKGBUILD 
VERSION="${pkgver}"
rm -fv "${PRGNAM}.PKGBUILD"
[ -z $VERSION ] && exit 1
if [ ! -z "`ls /var/log/packages/ | grep ${PRGNAM} | grep $VERSION`" ]; then
 echo -e "\nNot found newest:\n> $VERSION - this version already installed\n"
else
 echo -e "\n\n\n\n\n\n\n"
 cd $CWD/terminator/ || exit 1
 pwd
 VERSION="$VERSION" ./${PRGNAM}.SlackBuild || exit 1
 cd -
fi
	
	
	
	
	
	
	
	
	
# nitrogen
echo -e "\n\n\n\n\n\n\n"
cd $CWD/nitrogen/ || exit 1
pwd
./nitrogen.SlackBuild
	
	
	
	
	
	
# fluxbox
echo -e "\n\n\n\n\n\n\n"
cd $CWD/_wm/fluxbox/latest/ || exit 1
pwd
./fluxbox.SlackBuild
	
	
	
	
	
	
d_engrampa (){
PRGNAM=engrampa
echo ${PRGNAM}
VERSION=$(\
 wget -qO- http://ppa.launchpad.net/ubuntu-mate-dev/trusty-mate/ubuntu/dists/devel/main/source/Sources.gz   \
 | gzip -d | grep -A2 -x "Package: engrampa"| grep "Version: " \
 | sed 's/Version: //'| sed -r 's/\+.+//' \
)
[ -z $VERSION ] && exit 1
if [ ! -z "`ls /var/log/packages/ | grep ${PRGNAM} | grep $VERSION`" ]; then
 echo -e "\nNot found newest:\n> $VERSION - this version already installed\n"
else
 echo -e "\n\n\n\n\nBuild $VERSION\n\n"
 cd $CWD/1ru-zip/engrampa_infozip/latest_infozip_thunar/ || exit 1
 pwd
 VERSION="$VERSION" OUTPUT="$OUTPUT" ./${PRGNAM}.SlackBuild || exit 1
 cd -
fi
}
#b_engrampa
	
	
	
# tor-browser
#echo -e "\n\n\n\n\n\n\n"
#cd $CWD/_net-browser/tor-browser/ || exit 1
#pwd
#./tor-browser.SlackBuild
	
	
	
	
	
	
	
	
	
	
# FBReader
echo -e "\n\n\n\n\n\n\n"
cd $CWD/_views/fbreader/ || exit 1
pwd
VERSION=" " ./fbreader.SlackBuild


# paper-icon-theme
echo -e "\n\n\n\n\n\n\n"
cd $CWD/_themes-icon/_snwh-icons/ || exit 1
pwd
./paper-icon-theme.SlackBuild
}


#===============================================================================
#
# Copyright since 2016-19 by NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#===============================================================================
