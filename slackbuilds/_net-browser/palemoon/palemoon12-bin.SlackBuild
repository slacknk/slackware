#!/bin/sh

# Slackware build script for PaleMoon-12(!)-bin32

# Copyright 2018 NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# Archived builds page // Pale Moon 12.0
# https://forum.palemoon.org/viewtopic.php?t=744
# http://www.palemoon.org/archived.shtml

# for 64flash-player need flash32 (convertpkg-compat32)
# http://bear.alienbase.nl/mirrors/people/alien/sbrepos/14.2/x86/flashplayer-plugin/


# add start $HOME/.moonchild productions/pale moon 12 bin
# start.sh --profile <path> | Start with profile at <path>.


SRCNAM=palemoon
PRGNAM=palemoon12-bin
BUILD=${BUILD:-1}
TAG=${TAG:-nk}

# from platform.ini [Build]
# BuildID=20120502151229
# Milestone=12.0
VERSION=${VERSION:-12.0_20120502}

# binary package are available only for i686 (32/x86).
ARCH=x86

[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`
TMP=${TMP:-/tmp/SBo/$PRGNAM}
PKG=$TMP/package-$PRGNAM
rm -rf $PKG || sudo su -c "rm -rf $PKG" || /bin/su -c "rm -rf $PKG"
mkdir -p $PKG 

set -e
cd $TMP
rm -rf ${SRCNAM}

# Archived versions http://www.palemoon.org/archived.shtml

# ftp://palemoon:get@archive.palemoon.org/
tar xvf ./palemoon-12.0.linux.tar.bz2

# ftp://pmlocales:get@archive.palemoon.org/
# Language Packs // http://addons.palemoon.org/language-packs/
#install -m 644 $TMP/ru.xpi $PKG/opt/${PKGNAM}/extensions/langpack-ru@palemoon.org.xpi
#cp -v $TMP/ru.xpi $PKG/opt/${PKGNAM}/extensions/langpack-ru@firefox.mozilla.org.xpi
XPI="`ls 12.x | grep .xpi`" ; for l in $XPI ; do 
 cp -v 12.x/${l} ${SRCNAM}/extensions/langpack-${l/.xpi/}@firefox.mozilla.org.xpi
done

cd ${SRCNAM}


test $(id -u) -eq 0 && chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;


mkdir -p $PKG/opt/${PRGNAM}
mv -v * $PKG/opt/${PRGNAM}


# icons
for i in 16 32 48 ; do mkdir -p $PKG/usr/share/icons/hicolor/${i}x${i}/apps
 ln -s /opt/${PRGNAM}/chrome/icons/default/default${i}.png $PKG/usr/share/icons/hicolor/${i}x${i}/apps/${PRGNAM}.png
done

mkdir -p $PKG/usr/share/{pixmaps,icons/hicolor/128x128/apps}
ln -s /opt/${PRGNAM}/icons/mozicon128.png $PKG/usr/share/pixmaps/${PRGNAM}.png
ln -s /opt/${PRGNAM}/icons/mozicon128.png $PKG/usr/share/icons/hicolor/128x128/apps/${PRGNAM}.png

# desktop
mkdir -p $PKG/usr/share/applications
cat << EOF > $PKG/usr/share/applications/${PRGNAM}.desktop
[Desktop Entry]
Name=Pale Moon 12.0_32 bin
Comment=Pale Moon bin-version
#Comment=Browse the World Wide Web
Type=Application
Categories=Network;WebBrowser;Internet
Keywords=Internet;WWW;Browser;Web;Explorer
Exec=${PRGNAM}
Icon=${PRGNAM}
EOF


# usr/bin
mkdir -p $PKG/usr/bin/
cat << EOF > $PKG/usr/bin/${PRGNAM}
#!/bin/sh

pm_dir="\$HOME/.moonchild productions/pale moon 12 bin32"
[ ! -d "\$pm_dir" ] && mkdir -p "\$pm_dir"

/opt/${PRGNAM}/palemoon --profile "\$pm_dir"/
EOF
chmod +x $PKG/usr/bin/${PRGNAM}



mkdir -p $PKG/install
cat << EOF > $PKG/install/slack-desc
       |-----handy-ruler------------------------------------------------------|
appname: palemoon12-bin (Pale Moon is an Open Source, Goanna-based web browser)
appname:
appname: Pale Moon offers you a browsing experience in a browser completely 
appname: built from its own, independently developed source that has been 
appname: forked off from Firefox/Mozilla code, with carefully selected 
appname: features and optimizations to improve the browser's speed*, resource
appname: use, stability and user experience, while offering full customization
appname: and a growing collection of extensions and themes to make the browser
appname: truly your own.
appname:
appname: Pale Moon's homepage: http://www.palemoon.org/
EOF
sed -e '1d' -e 's/appname:/'${PRGNAM}':/' -i $PKG/install/slack-desc


# Make the package:
OUTPUT=${OUTPUT:-$CWD}
PKGTYPE=${PKGTYPE:-txz}
PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
[ -f $PACKAGE ] && rm -vf $PACKAGE

ROOTCOMMAND1="cd $PKG ; /sbin/makepkg -l y -c n $PACKAGE"
ROOTCOMMAND2="cd $CWD ; /sbin/upgradepkg --install-new --reinstall $PACKAGE"
ROOTCOMMAND3="rm -rf $TMP/{${PKG},palemoon}"
ROOTCOMMAND4="[ "$TMP" != "/tmp" ] && rm -rf $TMP"

# root-build
ROOTCOMMANDS="$ROOTCOMMAND1 && $ROOTCOMMAND2 && $ROOTCOMMAND3 " #&& $ROOTCOMMAND4"

# user-build
ROOTCOMMANDSU="cd $PKG ; chown -R root:root . && $ROOTCOMMANDS"

# and now build
if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMANDS"
else
 for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ; echo
 echo -e "\n\n\n\e[1mPlease enter your admin password for makepkg and installpkg\e[0m"
#sudo su -c "$ROOTCOMMANDSU" || /bin/su -c "$ROOTCOMMANDSU"
 su_sudo (){ echo -e "\n1) Enter passw for sudo (sudo su -c)"; sudo su -c "$ROOTCOMMANDSU" ; }
 su_root (){ echo -e "\n2) Enter root's passw 	(/bin/su -c)"; /bin/su -c "$ROOTCOMMANDSU" ; }
 su_sudo || su_root
fi
