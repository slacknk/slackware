#!/bin/sh

# Slackware build script for Geany

# Copyright 2018 NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


PRGNAM=geany
#VERSION=${VERSION:-1.33}
BUILD=${BUILD:-1}
TAG=${TAG:-nk}


if [ -z "$ARCH" ]; then
 case "$( uname -m )" in
   i686) ARCH=i686 ;;
   i?86) ARCH=i586 ;;
      *) ARCH=$( uname -m ) ;;
 esac
fi

if [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi


[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`
TMP=${TMP:-/tmp/SBo/$PRGNAM}
PKG=$TMP/package-$PRGNAM
rm -rf $PKG || sudo su -c "rm -rf $PKG" || /bin/su -c "rm -rf $PKG"
mkdir -p $PKG 

set -e
cd $TMP


if [ -z $VERSION ]; then
 NAM=geany-plugins
 WWWARCH="https://projects.archlinux.org/svntogit/community.git/plain/trunk/PKGBUILD?h=packages"
 VERSION=$( wget -O- $WWWARCH/${NAM} |grep pkgver= |sed 's/pkgver=//' )
fi
if [ -f /var/log/packages/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG ]; then
 echo -e "\n\nNot found newest;"
 echo -e "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG - this version already installed\n"
 rm -rfv $PKG ; [ $TMP = /tmp/SBo/$PRGNAM ] && rm -rf $TMP ; exit 1
else
 echo -e "\n\n\n"; PKGLOG="/var/log/packages/$PRGNAM-$VERSION-*"
 if [ -f $PKGLOG ]; then
  echo -e "-`ls $PKGLOG | sed "s|/var/log/packages/$PRGNAM-||;s|-$ARCH-|_|"`"
 fi
 echo -e "+${VERSION}_$BUILD$TAG"
 echo -e "\n\n\n"
fi


rm -rf $PRGNAM-$VERSION
[ ! -f $PRGNAM-$VERSION.tar.bz2 ] && wget -c http://download.geany.org/$PRGNAM-$VERSION.tar.bz2
tar xvf $PRGNAM-$VERSION.tar.bz2
cd $PRGNAM-$VERSION


test $(id -u) -eq 0 && chown -R root:root .

find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;


# Note! _slack14.2

# VERSION:-1.33  --disable-debug \ --disable-systemd \ --with-gtk=gtk2
# configure: WARNING: unrecognized options: --disable-debug, --disable-systemd, --with-gtk

# --disable-gtkdoc-header
# generate the GtkDoc header suitable for GObject introspection [default=auto]

# --enable-gtk3
# compile with GTK3 support (experimental) [default=no]

# --enable-pdf-docs
# generate PDF documentation using rst2pdf [default=auto]

# --enable-binreloc
# compile with binary relocation support [default=no]


LDFLAGS="$SLKLDFLAGS" \
CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
 --prefix=/usr \
 --libdir=/usr/lib${LIBDIRSUFFIX} \
 --docdir=/usr/doc/$PRGNAM-$VERSION \
 --mandir=/usr/man \
 --infodir=/usr/info \
 --sysconfdir=/etc \
 --localstatedir=/var \
 --infodir=/usr/info \
 \
 --program-prefix= \
 --program-suffix= \
 \
 --disable-static \
 --enable-static=no \
 \
 --build=$ARCH-slackware-linux \
 --host=$ARCH-slackware-linux \
 \
 --enable-plugins \
 --enable-vte \
 --enable-html-docs \
 \
 --disable-gtk3 \
 --disable-binreloc \
 --disable-pdf-docs \
 --disable-gtkdoc-header #--help

x=$(cat /proc/cpuinfo | grep processor | wc -l)
[ -n "$x" ] && let "NUMJOBS=x+1" || NUMJOBS=2
make -j${NUMJOBS} || exit 1

make install DESTDIR=$PKG || exit 1


find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true


# man-dir
if [ -d $PKG/usr/share/man/ ]; then
 mkdir -p $PKG/usr/man
 mv $PKG/usr/share/man/* $PKG/usr/man
 rmdir -v $PKG/usr/share/man
fi

# man-pages
if [ -d $PKG/usr/man ]; then
 find $PKG/usr/man -type f -exec gzip -9 {} \;
 for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done
fi


# Thx SBo Team // https://slackbuilds.org/repository/14.2/development/geany/
# Enable support for global tags - make sure any .tags files are in $CWD.
TAGS=${TAGS:-no}

# Add global support for tags:
if [ "$TAGS" = "yes" ]; then
  mkdir -p $PKG/usr/share/$PRGNAM/tags
  for tagfile in $CWD/tags/*; do
    cat $tagfile > $PKG/usr/share/$PRGNAM/tags/${tagfile##/*/}
  done
fi


mkdir -pv $PKG/usr/doc/$PRGNAM-$VERSION ; D=""
D+="AUTHORS COPYING* copying* CHANGE* ?hange?og* CONTRIBUT* CREDIT* Credit* DEPENDENCIES "
D+="FAQ faq* gpl* lgpl* HACKING HISTORY* INSTALL KDE4FAQ LICENSE* *license* MAINTAINERS "
D+="NEWS *Preview* *preview* PORT* PKG-INFO README* readme* Resources* THANKS* Thanks* "
D+="TODO* ToDo* todo* TRANSLATORS VERSION version* WhatsNew* "
D+="*.rules debian/changelog debian/control debian/copyright "
DOCS=""; for d in ${D}; do [ ! -z "`ls $d`" ] && DOCS+="$d "; done
[[ ! -z $DOCS ]] && mv -v $DOCS $PKG/usr/doc/$PRGNAM-$VERSION/ || exit 1

[ ! -z "`ls *.md`" ] && md="`ls *.md`" && mv -v $md $PKG/usr/doc/$PRGNAM-$VERSION/

cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
find $PKG/usr/doc/$PRGNAM-$VERSION/ -type f -exec chmod 644 {} \;

find $PKG/usr/doc/$PRGNAM-$VERSION/ -type d -exec chmod 755 {} \;


mkdir -p $PKG/install
cat << EOF > $PKG/install/slack-desc
       |-----handy-ruler------------------------------------------------------|
appname: geany (A fast, light, small and lightweight GTK+ IDE)
appname:
appname: Geany is a small and lightweight Integrated Development Environment.
appname:
appname: It was developed to provide a small and fast IDE, which has only a
appname: few dependencies from other packages. Another goal was to be as 
appname: independent as possible from a special Desktop Environment like KDE 
appname: or GNOME - Geany only requires the GTK2 runtime libraries.
appname:
appname: Homepage: https://www.geany.org/ 
appname:
EOF
sed -e '1d' -e 's/appname:/'${PRGNAM}':/' -i $PKG/install/slack-desc


#cd $PKG
#/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}


# Build and install package // PKGTYPE=${PKGTYPE:-txz}
# PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}"
OUTPUT=${OUTPUT:-$CWD} ; [ $OUTPUT != $CWD ] && mkdir -pv $OUTPUT
PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}"
[ -f $PACKAGE ] && rm -vf $PACKAGE

ROOTCOMMAND1="cd $PKG ; /sbin/makepkg -l y -c n $PACKAGE"
ROOTCOMMAND2="cd $CWD ; /sbin/upgradepkg --install-new --reinstall $PACKAGE"
ROOTCOMMAND3="rm -rf $PKG"
ROOTCOMMAND4="[ "$TMP" != "/tmp" ] && rm -rf $TMP"

ROOTCOMMANDS="$ROOTCOMMAND1 && $ROOTCOMMAND2 && $ROOTCOMMAND3 && $ROOTCOMMAND4"
ROOTCOMMANDSU="cd $PKG ; chown -R root:root . && $ROOTCOMMANDS"

if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMANDS"
else
 for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ; echo
 echo -e "\n\n\n\e[1mPlease enter your admin password for makepkg and installpkg\e[0m"
#sudo su -c "$ROOTCOMMANDSU" || /bin/su -c "$ROOTCOMMANDSU"
 su_sudo (){ echo -e "\n1) Enter passw for sudo (sudo su -c)"; sudo su -c "$ROOTCOMMANDSU" ; }
 su_root (){ echo -e "\n2) Enter root's passw 	(/bin/su -c)"; /bin/su -c "$ROOTCOMMANDSU" ; }
 su_sudo || su_root
fi
