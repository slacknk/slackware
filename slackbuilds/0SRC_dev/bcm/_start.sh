#!/bin/sh

# 2017 Written by NK
# You are free to modify or redistribute this in any way you wish.


lspci -knn | grep -e Broadcom -e BCM -A2
sleep 3s


# sbopkg -i "b43-fwcutter b43-firmware broadcom-sta"

CWD=`dirname $0`

for BCM in b43-fwcutter b43-firmware broadcom-sta ; do
 cd $CWD
 ./${BCM}.Slackbuild || exit 1
done
