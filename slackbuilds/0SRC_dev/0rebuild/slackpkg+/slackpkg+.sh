#!/bin/sh

# build script for slackpkg+ with newest pathces
# You are free to modify or redistribute this in any way you wish.
# 2016 Written by NK


# History

# slackpkg+-1.7.0-noarch-4
# + search-duplicate-packages-fix
# http://www.slackware.ru/forum/viewtopic.php?p=12244#p12244
# http://www.linuxquestions.org/questions/slackware-14/slackpkg-vs-third-party-package-repository-4175427364/page43.html#post5616120
# Important : Even if the patch solve the issue, keep in mind that information returned by slackpkg search <pattern> when different repositories offer the same package (ie. with exact name) depend on the defined priorities and thus, could be wrong. For instance, if you have PKGS_PRIORITY=(alienbob restricted), then you install vlc from alienbob, and later you set PKGS_PRIORITY=(restricted alienbob ), then, slackpkg search will show that vlc is from restricted instead of alienbob


[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`


mkdir -p /tmp/SBo
cd /tmp/SBo
rm -rf slackpkgplus-code
git clone git://git.code.sf.net/p/slackpkgplus/code slackpkgplus-code
cd slackpkgplus-code


TXZ=$(head -2 ChangeLog.txt|cut -f1 -d:|cut -f2 -d/|tail -1)
VERSION=$(echo $TXZ|cut -f2 -d-)
BUILD=$(echo $TXZ|cut -f4 -d-|sed 's/.txz//')


if [[ $VERSION = 1.7.0 && $BUILD = 4mt ]]; then
 cd src
 patch -p0 --verbose < "$CWD"/${VERSION}-${BUILD}_search-duplicate-packages-fix.patch || exit 1
fi

[ ! -x slackpkg+.SlackBuild ] && chmod +x slackpkg+.SlackBuild
./slackpkg+.SlackBuild
