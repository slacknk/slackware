#!/bin/sh

# This script uses the Pat's SlackBuild scripts present here to build a
# miny complete set of kernel packages: kernel-huge -generic -modules -source
# for the x86_32-smp-pae architecture.


if [ $USER != "root" ]; then
 echo -e "\n> Login in root\n> for build kernel in this sh-script.\n\n"
 exit 1
fi


BUILD=${BUILD:-5}_nk

#VERSION=${VERSION:-3.10.108}
VERSION=${VERSION:-3.18.131} ; BUILD=6_nk ; VERSION_CONFIG="3.18.131-${BUILD/_nk/}"

x=$(cat /proc/cpuinfo | grep processor | wc -l)
[ -n "$x" ] && let "NUMJOBS=x+1" || NUMJOBS=2

CWD=`dirname $0`
TMP=${TMP:-/tmp/SBo/kernel-$VERSION}
mkdir -p $TMP ; chown 1000:100 $TMP
cd $TMP
set -e

# https://cdn.kernel.org/pub/linux/kernel/v3.x/linux-3.16.60.tar.xz
# http://slackware.osuosl.org/slackware-14.1/source/k/config-x86/
# rsync.osuosl.org::slackware/slackware-14.1/source/k/packaging-x86/


WWWs="rsync.osuosl.org::slackware"
SLACK_VER=
SLACK_DESC=
SLACK_KERNEL=
#VERSION_CONFIG=


s_get (){
#
 #
 if [ ! -f slack-desc ]; then
  rsync -avrh --delete --progress $WWWs/${SLACK_VER}/../../slackware/k/${SLACK_DESC} slack-desc
 fi
 #
 #
 for c in config-generic-smp config-huge-smp ; do
 if [ ! -f $CWD/${VERSION_CONFIG}_${c} ]; then
  if [ ! -f ${VERSION_CONFIG}_${c} ]; then
   if [ ! -f ${VERSION}_${c} ]; then
    if [ ! -f ${c}-${SLACK_KERNEL}-smp ]; then
     rsync -avrh --delete --progress $WWWs/${SLACK_VER}/config-x86/${c}-${SLACK_KERNEL}-smp .
    fi
   fi
  fi
 fi
 done
 #
 #
 for s in kernel-generic-smp kernel-huge-smp kernel-modules-smp ; do
 [ -d $s ] || rsync -avrh --delete --progress $WWWs/${SLACK_VER}/packaging-x86/$s .
 done
 #
#
}


s_src (){
 cd /usr/src
 rm -rfv linux-$VERSION
 tar xvf $TMP/linux-$VERSION.tar.xz
}


build_kernel (){
 echo -e "\n\n\n"
 cd /usr/src/linux-$VERSION

 build_kernel_config (){
  if [ -f $CWD/${VERSION_CONFIG}_config-${KERNEL_NAME}-smp ]; then
   cat $CWD/${VERSION_CONFIG}_config-${KERNEL_NAME}-smp > .config
  elif [ -f $TMP/${VERSION_CONFIG}_config-${KERNEL_NAME}-smp ]; then
   cat $TMP/${VERSION_CONFIG}_config-${KERNEL_NAME}-smp > .config
  else
   cat $TMP/config-${KERNEL_NAME}-smp-${SLACK_KERNEL}-smp > .config
  fi
  make oldconfig
 }
 
 if [ ${KERNEL_NAME} = huge ]; then
  build_kernel_config
  make -j${NUMJOBS} bzImage || exit 1
 elif [ ${KERNEL_NAME} = generic ]; then
  make mrproper && build_kernel_config
  make -j${NUMJOBS} bzImage && echo -e "\n" || exit 1
  make -j${NUMJOBS} modules && echo -e "\n" || exit 1
  make -j${NUMJOBS} modules_install && echo -e "\n" || exit 1
  cd $TMP/kernel-modules-smp
  KERNELRELEASE=${VERSION}-smp ./kernel-modules-smp.SlackBuild
 fi

 cd $TMP/kernel-${KERNEL_NAME}-smp
 ./kernel-${KERNEL_NAME}-smp.SlackBuild

 if [ ! -f $TMP/${VERSION}_config-${KERNEL_NAME}-smp ]; then
  cat /usr/src/linux-$VERSION/.config > $TMP/${VERSION}_config-${KERNEL_NAME}-smp
 fi
}


build_kernel_source (){
 if [[ ! -z "`ls $TMP | grep -e config -e _${VERSION}`" ]]; then
  rm -rf /tmp/package-kernel*

  # Thx bormant < slackware.ru >
  cd /usr/src/linux-$VERSION
  make clean && make prepare || exit 1
  [ -f .version ] && rm -v .version

  # Thx Pat (The Founder and Project Coordinator) < slackware.com >
  # Don't package the kernel in the sources: and # No need for these:
  find . -name "*Image" -exec rm "{}" \;
  find . -name "*.cmd" -exec rm -f "{}" \; 
  #rm -f .config.old
  rm -f .*.d
  # Still some dotfiles laying around... probably fine though

  cd ..
  [ -e linux ] && rm -v linux
  ln -sfv linux-$VERSION linux

  KERNEL_SRCDIR=/tmp/package-kernel-source
  mkdir -p ${KERNEL_SRCDIR}/{install,usr/src}
  cd ${KERNEL_SRCDIR}

  cat $TMP/slack-desc > install/slack-desc
  cp -av /usr/src/linux-$VERSION /usr/src/linux usr/src

  mkdir -p usr/doc/kernel-source-${VERSION}_smp/
  cp -av $TMP/*config* $TMP/kernel-*-smp usr/doc/kernel-source-${VERSION}_smp/

  /sbin/makepkg -l y -c n /tmp/kernel-source-${VERSION}_smp-noarch-${BUILD}.txz
  cd ..
  rm -rf ${KERNEL_SRCDIR}
 else
  exit 1
 fi
}


s_install (){
 #installpkg /tmp/kernel-$p-smp-4.6.3-smp-*t?z
 CWDDIR="`dirname $0`/kernel-${VERSION}_smp-`uname -m`-${BUILD}"
 [ -d $CWDDIR ] && rm -rf $CWDDIR
 mkdir -p $CWDDIR
 cd $CWDDIR
 cp -av /tmp/kernel-*-${VERSION}_smp-*-${BUILD}.t?z $CWDDIR
 chown -R 1000:100 $CWDDIR

 # modules and sources
 upgradepkg --reinstall --install-new $CWDDIR/kernel-source-${VERSION}_smp-*-${BUILD}.t?z
 upgradepkg --reinstall --install-new $CWDDIR/kernel-modules-smp-${VERSION}_smp-*-${BUILD}.t?z

 # 4huge
 upgradepkg --reinstall --install-new $CWDDIR/kernel-huge-smp-${VERSION}_smp-*-${BUILD}.t?z

 # 4generic
 #upgradepkg --reinstall --install-new $CWDDIR/kernel-g*-${VERSION}_smp-*-${BUILD}.t?z
 #/usr/share/mkinitrd/mkinitrd_command_generator.sh -k ${VERSION}-smp | sh

 rm -rf /tmp/kernel-*-${VERSION}_smp-*-${BUILD}.t?z

 lilo -v
 ls -l /boot
}


K_SRC_DIR="/tmp/SBo/_build/${VERSION}"

s_ln (){
 rm -rf $K_SRC_DIR
 mkdir -p $K_SRC_DIR
 if [[ `readlink /usr/src` != "$K_SRC_DIR" ]]; then
  mv /usr/src /usr/src_
  ln -sv $K_SRC_DIR /usr/src
 fi
}

s_ln_rm (){
 rm -rf $K_SRC_DIR 
 rm -v /usr/src
 mv -v /usr/src_ /usr/src
}


S_SMP (){
 #s_ln
 s_get
 s_src
 VERSION=$VERSION BUILD=${BUILD} KERNEL_NAME=huge build_kernel
 VERSION=$VERSION BUILD=${BUILD} KERNEL_NAME=generic build_kernel
 build_kernel_source
 #s_ln_rm
 s_install
}


# START Here
#
removepkg kernel-modules-smp kernel-source
removepkg kernel-huge-smp kernel-generic-smp

	echo -e "\n\n"

rm -rf /tmp/package-kernel*
rm -rf /tmp/kernel-*-${VERSION}_smp-*-${BUILD}.t?z

#
# Cont.
#
WWWk="https://cdn.kernel.org/pub/linux/kernel"

if [ -z $VERSION ]; then
 TMP=${TMP:-/tmp/SBo/_build}
 [ -d $TMP ] && mkdir -p $TMP
 DIR="/tmp/kernel-current_`date +%Y%m`"
 rsync -avrh --delete --progress ${WWWs}/slackware-current/source/k/ $DIR/
 cd $DIR
 TMP=$TMP RECIPES="IA32_SMP" ./build-all-kernels.sh

elif [ ! -z `echo $VERSION | grep ^3.18` ]; then
 [ ! -f linux-$VERSION.tar.xz ] && wget -c $WWWk/v3.x/linux-$VERSION.tar.xz
 SLACK_VER="slackware-14.1/source/k"
 SLACK_KERNEL="3.10.17"
 SLACK_DESC="kernel-source-3.10.17_smp-noarch-3.txt"
 S_SMP

elif [ ! -z `echo $VERSION | grep ^3` ]; then
 [ ! -f linux-$VERSION.tar.xz ] && wget -c $WWWk/v3.x/linux-$VERSION.tar.xz
 SLACK_VER="slackware-14.1/source/k"
 SLACK_KERNEL="3.10.17"
 SLACK_DESC="kernel-source-3.10.17_smp-noarch-3.txt"
 S_SMP

else
 exit 1
fi

#
# END


# ============================================================================= 
# Copyright 2018 NK. All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# ============================================================================= 



# ============================================================================= 
# OLD Notes!
#
#KERNEL_NAME=huge
 #cat $TMP/config-huge-smp-4.6-smp > .config
 #make oldconfig
 #make bzImage
 #cd /tmp/k/packaging-x86/kernel-huge-smp/
 #VERSION=$VERSION bash *Build
 # lilo -v
#
#KERNEL_NAME=generic
 #cd /usr/src/linux-$VERSION
 #make mrproper
 #cat $TMP/config-generic-smp-4.6-smp > .config
 #make oldconfig
 #make bzImage modules modules_install
 #cd /tmp/k/packaging-x86/kernel-generic-smp/
 #VERSION=$VERSION bash *Build
#
 #cd /tmp/k/packaging-x86/kernel-modules-smp/
 #VERSION=$VERSION KERNELRELEASE=$VERSION-smp bash *Build
 # /usr/share/mkinitrd/mkinitrd_command_generator.sh -r -k $VERSION-smp | bash && lilo -v
#

# =============================================================================
# WARNING! w/ 4.4.14_smp.config14.2!
# Don't work this method for kernel3
# 
# $ uname -rm > 3.16.61-smp i686
# $ glxgears > intel_do_flush_locked failed: Invalid argument
#
#elif [ ! -z `echo $VERSION | grep ^3.16` ]; then
# [ ! -f linux-$VERSION.tar.xz ] && wget -c $WWWk/v3.x/linux-$VERSION.tar.xz
# SLACK_VER="slackware-14.2/source/k"
# SLACK_KERNEL="4.4.14"
# SLACK_DESC="kernel-source-4.4.14_smp-noarch-1.txt"
# VERSION_CONFIG=3.16.60
# S_SMP

# =============================================================================
