# 3.18_NOTES! - Building CLOSED - Don't Work
## WARNING! Kernel Panic with VirtualMachine in VirtualBox.

* Based on config from [3.10.17-smp_slack14.1](http://slackware.osuosl.org/slackware-14.1/source/k/config-x86/) +/- [4.4.14-smp_slack14.2](http://slackware.osuosl.org/slackware-14.2/source/k/config-x86/) (CONFIG_keys).
* Build on slackware-14.2:
  * Before build - removed: kernel-`source` -`generic` -`huge` -`modules-smp`
  * With installed: `kernel-firmware-*noarch`, `kernel-headers-4.4.*_smp-x86`
```
scripts/kconfig/conf --oldconfig Kconfig
.config:615:warning: symbol value 'm' invalid for HOTPLUG_PCI_PCIE
.config:5502:warning: symbol value 'm' invalid for USB_DWC2
```

### current_build for 14.2: 
#### _build-006 - m-and-n based_on [4.4.14-smp_slack14.2](http://slackware.osuosl.org/slackware-14.2/source/k/config-x86/)
```
Kernel compression mode
  1. Gzip (KERNEL_GZIP)
  2. Bzip2 (KERNEL_BZIP2)
> 3. LZMA (KERNEL_LZMA)
  4. XZ (KERNEL_XZ)
  5. LZO (KERNEL_LZO)
  6. LZ4 (KERNEL_LZ4)
choice[1-6?]: 3
add # CONFIG_KERNEL_LZ4 is not set
for auto choise from 3.10: CONFIG_KERNEL_LZMA=y

uselib syscall (USELIB) [Y/n/?] 
# CONFIG_USELIB is not set

Enable legacy fbdev support for the modesetting intel driver (DRM_I915_FBDEV) [Y/n/?]
my CONFIG_DRM_I915_FBDEV=y

SLUB per cpu partial cache (SLUB_CPU_PARTIAL) [Y/n/?]
4.4.14 CONFIG_SLUB_CPU_PARTIAL=y

Support for uevent helper (UEVENT_HELPER) [Y/n/?]
4.4.14 CONFIG_UEVENT_HELPER=y

Supported bus types
  > 1. BCMA and SSB (B43_BUSES_BCMA_AND_SSB) (NEW)
    2. BCMA only (B43_BUSES_BCMA) (NEW)
    3. SSB only (B43_BUSES_SSB) (NEW)
4.4.14 CONFIG_B43_BUSES_BCMA_AND_SSB=y
def CONFIG_B43_BUSES_BCMA_AND_SSB=y
# CONFIG_B43_BUSES_BCMA is not set
# CONFIG_B43_BUSES_SSB is not set


"l2tp" match support (NETFILTER_XT_MATCH_L2TP) [M/n/?]
4.4.14 CONFIG_NETFILTER_XT_MATCH_L2TP=m

Open vSwitch GRE tunneling support (OPENVSWITCH_GRE) [Y/n/?]
Open vSwitch VXLAN tunneling support (OPENVSWITCH_VXLAN) [Y/n/?]
3.10 CONFIG_OPENVSWITCH_GRE=m
3.10 CONFIG_OPENVSWITCH_VXLAN=m
4.4.14 CONFIG_OPENVSWITCH_GRE=m
4.4.14 CONFIG_OPENVSWITCH_VXLAN=m
my # CONFIG_OPENVSWITCH_GRE is not set
# CONFIG_OPENVSWITCH_VXLAN is not set

ARP packet logging (NF_LOG_ARP) [N/m/y]
4.4.14 CONFIG_NF_LOG_ARP=m
# from _b4 to _b6 -my
# CONFIG_NF_LOG_ARP is not set -> CONFIG_NF_LOG_ARP=m

IPv4 packet logging (NF_LOG_IPV4) [M/y]
4.4.14 CONFIG_NF_LOG_IPV4=m

IPv4 packet rejection (NF_REJECT_IPV4) [M/y]
4.4.14 CONFIG_NF_REJECT_IPV4=m


Agere devices (NET_VENDOR_AGERE) [Y/n/?]
ARC devices (NET_VENDOR_ARC) [Y/n/?]
4.4.14 CONFIG_NET_VENDOR_AGERE=y
4.4.14 CONFIG_NET_VENDOR_ARC=y
my # CONFIG_NET_VENDOR_AGERE is not set
# CONFIG_NET_VENDOR_ARC is not set

Qualcomm devices (NET_VENDOR_QUALCOMM) [Y/n/?]
Samsung Ethernet devices (NET_VENDOR_SAMSUNG) [Y/n/?]
4.4.14 CONFIG_NET_VENDOR_QUALCOMM=y
4.4.14 CONFIG_NET_VENDOR_SAMSUNG=y
my # CONFIG_NET_VENDOR_QUALCOMM is not set
# CONFIG_NET_VENDOR_SAMSUNG is not set

VXLAN offload support on be2net driver (BE2NET_VXLAN) [Y/n/?]
4.4.14 CONFIG_BE2NET_VXLAN=y
my # CONFIG_BE2NET_VXLAN is not set

VXLAN offloads Support (MLX4_EN_VXLAN) [Y/n/?]
4.4.14 CONFIG_MLX4_EN_VXLAN=y
my # CONFIG_MLX4_EN_VXLAN is not set

Virtual eXtensible Local Area Network (VXLAN) offload support (QLCNIC_VXLAN) [N/y/?] 
QLOGIC QLCNIC 82XX and 83XX family HWMON support (QLCNIC_HWMON) [Y/n/?]
4.4.14 CONFIG_QLCNIC_HWMON=y
# CONFIG_QLCNIC_HWMON is not set


Support for G-PHY (802.11g) devices (B43_PHY_G) [Y/n/?]
4.4.14 CONFIG_B43_PHY_G=y

USB Network Adapters (USB_NET_DRIVERS) [Y/m]
4.4.14 CONFIG_USB_NET_DRIVERS=y
my CONFIG_USB_NET_DRIVERS=m

OHCI support for PCI-bus USB controllers (USB_OHCI_HCD_PCI) [M/n/?]
4.4.14 CONFIG_USB_OHCI_HCD_PCI=m


wil6210 tracing support (WIL6210_TRACING) [Y/n/?]
4.4.14 # CONFIG_WIL6210_TRACING is not set

Realtek rtlwifi family of devices (RTL_CARDS) [M/n/?]
4.4.14 CONFIG_RTL_CARDS=m

Microsoft Synthetic Keyboard driver (HYPERV_KEYBOARD) [M/n/?]
4.4.14 CONFIG_HYPERV_KEYBOARD=m

X86 package temperature thermal driver (X86_PKG_TEMP_THERMAL) [M/n/?]
4.4.14 CONFIG_X86_PKG_TEMP_THERMAL=m

Enable IR raw decoder for the Sharp protocol (IR_SHARP_DECODER) [M/n/?]
4.4.14 CONFIG_IR_SHARP_DECODER=m

Enable IR raw decoder for the XMP protocol (IR_XMP_DECODER) [M/n/?]
4.4.14 CONFIG_IR_XMP_DECODER=m


PCIe-based Platforms (USB_DWC3_PCI) [M/n/?]
4.4.14 CONFIG_USB_DWC3_PCI=m

Enable USB3 LPM Capability (DWC3_HOST_USB3_LPM_ENABLE) [N/y/?]
# CONFIG_DWC3_HOST_USB3_LPM_ENABLE is not set

DesignWare USB2 DRD Core Support (USB_DWC2) [N/y/?]
4.4.14_Platform Glue # CONFIG_USB_DWC2 is not set
3.10_ANDROID > CONFIG_USB_DWC2=m
3.18_USB > # CONFIG_USB_DWC2 is not set

Synopsys DesignWare AHB DMA support (DW_DMAC_CORE) [M/y]
4.4.14 CONFIG_DW_DMAC_CORE=m

Broadcom Kona USB2 PHY Driver (BCM_KONA_USB2_PHY) [N/m/y/?]
4.4.14 # CONFIG_BCM_KONA_USB2_PHY is not set


Enable HPET MMAP access by default (HPET_MMAP_DEFAULT) [Y/n/?]
4.4.14 CONFIG_HPET_MMAP=y -> CONFIG_HPET_MMAP_DEFAULT=y

VP-3054 Secondary I2C Bus Support (VIDEO_CX88_ENABLE_VP3054) [Y/n/?]
3.10 CONFIG_VIDEO_CX88_VP3054=m
4.4.14 CONFIG_VIDEO_CX88_ENABLE_VP3054=y


RPC over RDMA Client Support (SUNRPC_XPRT_RDMA_CLIENT) [M/n/?]
RPC over RDMA Server Support (SUNRPC_XPRT_RDMA_SERVER) [M/n/?]
4.4.14 CONFIG_SUNRPC_XPRT_RDMA=m
my CONFIG_SUNRPC_XPRT_RDMA=m
CONFIG_SUNRPC_XPRT_RDMA_CLIENT=m
CONFIG_SUNRPC_XPRT_RDMA_SERVER=m

Integrity subsystem (INTEGRITY) [Y/n/?]
Enables integrity auditing support  (INTEGRITY_AUDIT) [Y/n/?]
4.4.14 # CONFIG_INTEGRITY is not set
my # CONFIG_INTEGRITY is not set
# CONFIG_INTEGRITY_AUDIT is not set

MCB support (MCB) [N/m/y/?]
4.4.14 # CONFIG_MCB is not set

Generic powercap sysfs driver (POWERCAP) [N/y/?]
4.4.14 CONFIG_POWERCAP=y
my # CONFIG_POWERCAP is not set
```

### History/Logs:

## _build-001 - default <Enter-Enter>

## _build-002 - m -> CONFIG_HOTPLUG_PCI_PCIE=y

## _build-003 - only_values_choise based_on 14.2_config-x86 and cont.builds...
```
CONFIG_LOG_CPU_MAX_BUF_SHIFT=12
CONFIG_SND_MAX_CARDS=32 - Max number of sound cards
CONFIG_FWTTY_MAX_TOTAL_PORTS=64 - Maximum number of serial ports supported
CONFIG_FWTTY_MAX_CARD_PORTS=32 - Maximum number of serial ports supported per adapter
CONFIG_MESSAGE_LOGLEVEL_DEFAULT=4 - Default message log level (1-7)
CONFIG_MAGIC_SYSRQ_DEFAULT_ENABLE=0x1 - Enable magic SysRq key functions by default
CONFIG_PANIC_TIMEOUT=0 - panic timeout
```

## _build-004 - Nno by DEFAULT
```
Compile also drivers which will not load (COMPILE_TEST) [N/y/?]
Task_based RCU implementation using voluntary context switch (TASKS_RCU) [N/y/?]
User namespace (USER_NS) [N/y/?]
Provide system-wide ring of trusted keys (SYSTEM_TRUSTED_KEYRING) [N/y/?]
Compress modules on installation (MODULE_COMPRESS) [N/y/?]
Block device command line partition parser (BLK_CMDLINE_PARSER) [N/y/?]
AIX basic partition table support (AIX_PARTITION) [N/y/?]
Command line partition support (CMDLINE_PARTITION) [N/y/?]
Intel SoC IOSF Sideband support for SoC platforms (IOSF_MBI) [N/m/y/?]
Common API for compressed memory storage (ZPOOL) [N/m/y/?]
Low density storage for compressed pages (ZBUD) [N/m/y/?]
Enable workqueue power-efficient mode by default (WQ_POWER_EFFICIENT_DEFAULT) [N/y/?]
Extended Error Log support (ACPI_EXTLOG) [N/m/y/?]
Mark VGA/VBE/EFI FB as generic system framebuffer (X86_SYSFB) [N/y/?]
IP: Foo (IP protocols) over UDP (NET_FOU) [N/m/y/?]
Generic Network Virtualization Encapsulation (Geneve) (GENEVE) [N/m/y/?]
Virtual (secure) IPv6: tunneling (IPV6_VTI) [N/m/?]
Netfilter nf_tables support (NF_TABLES) [N/m/y/?]
"SNAT and DNAT" targets support (NETFILTER_XT_NAT) [N/m/?]
"control group" match support (NETFILTER_XT_MATCH_CGROUP) [N/m/?]
"ipcomp" match support (NETFILTER_XT_MATCH_IPCOMP) [N/m/?]
hash:ip,mark set support (IP_SET_HASH_IPMARK) [N/m/?]
hash:mac set support (IP_SET_HASH_MAC) [N/m/?]
hash:net,port,net set support (IP_SET_HASH_NETPORTNET) [N/m/?]
hash:net,net set support (IP_SET_HASH_NETNET) [N/m/?]
weighted failover scheduling (IP_VS_FO) [N/m/?]
ARP packet logging (NF_LOG_ARP) [N/m/y]
IPv4 masquerade support (NF_NAT_MASQUERADE_IPV4) [N/m/?]
SYNPROXY target support (IP_NF_TARGET_SYNPROXY) [N/m/?]
iptables NAT support (IP_NF_NAT) [N/m/?]
IPv6 masquerade support (NF_NAT_MASQUERADE_IPV6) [N/m/?]
SYNPROXY target support (IP6_NF_TARGET_SYNPROXY) [N/m/?]
ip6tables NAT support (IP6_NF_NAT) [N/m/?]
6LoWPAN Support (6LOWPAN) [N/m/?]
Fair Queue (NET_SCH_FQ) [N/m/y/?]
Heavy-Hitter Filter (HHF) (NET_SCH_HHF) [N/m/y/?]
Proportional Integral controller Enhanced (PIE) scheduler (NET_SCH_PIE) [N/m/y/?]
BPF-based classifier (NET_CLS_BPF) [N/m/y/?]
Multicast optimisation (BATMAN_ADV_MCAST) [N/y/?]
MPLS: GSO support (NET_MPLS_GSO) [N/m/y/?]
High-availability Seamless Redundancy (HSR) (HSR) [N/m/y/?]
Network priority cgroup (CGROUP_NET_PRIO) [N/y/?]
Bosch M_CAN devices (CAN_M_CAN) [N/m/?]
Geschwister Schneider UG interfaces (CAN_GS_USB) [N/m/?]
NFC Digital Protocol stack support (NFC_DIGITAL) [N/m/?]
NFC hardware simulator driver (NFC_SIM) [N/m/?]
Marvell NFC driver support (NFC_MRVL) [N/m/?]
STMicroelectronics ST21NFCA NFC driver (NFC_ST21NFCA) [N/m/?]
STMicroelectronics ST21NFCB NFC driver (NFC_ST21NFCB) [N/m/?]
Fallback user-helper invocation for firmware loading (FW_LOADER_USER_HELPER_FALLBACK) [N/y/?]
Enable verbose FENCE_TRACE messages (FENCE_TRACE) [N/y/?]
GPIO NAND Flash driver (MTD_NAND_GPIO) [N/m/?]
SPI-NOR device support (MTD_SPI_NOR) [N/m/?]
Read-only block devices on top of UBI volumes (MTD_UBI_BLOCK) [N/y/?]
Null test block driver (BLK_DEV_NULL_BLK) [N/m/y]
Intel Trusted Execution Environment with ME Interface (INTEL_MEI_TXE) [N/m/y/?]
SCSI: use blk-mq I/O path by default (SCSI_MQ_DEFAULT) [N/y/?]
ATTO Technology's ExpressSAS RAID adapter driver (SCSI_ESAS2R) [N/m/y/?]
Era target (EXPERIMENTAL) (DM_ERA) [N/m/y/?]
Switch target support (EXPERIMENTAL) (DM_SWITCH) [N/m/y/?]
TCM/USER Subsystem Plugin for Linux (TCM_USER) [N/m/?]
Virtual netlink monitoring device (NLMON) [N/m/y/?]
Marvell 88E6171 ethernet switch chip support (NET_DSA_MV88E6171) [N/m/y/?]
Broadcom Starfighter 2 Ethernet switch support (NET_DSA_BCM_SF2) [N/m/y/?]
Altera Triple-Speed Ethernet MAC support (ALTERA_TSE) [N/m/y/?]
APM X-Gene SoC Ethernet Driver (NET_XGENE) [N/m/y/?]
Beckhoff CX5020 EtherCAT master support (CX_ECAT) [N/m/y/?]
Intel(R) Ethernet Controller XL710 Family support (I40E) [N/m/y/?]
Intel(R) XL710 X710 Virtual Function Ethernet support (I40EVF) [N/m/y/?]
Intel(R) FM10000 Ethernet Switch Host Interface Support (FM10K) [N/m/y/?]
Virtual eXtensible Local Area Network (VXLAN) offload support (QLCNIC_VXLAN) [N/y/?]
Samsung 10G/2.5G/1G SXGBE Ethernet driver (SXGBE_ETH) [N/m/y/?]
SMSC LAN911x/LAN921x families embedded ethernet support (SMSC911X) [N/m/y/?]
Drivers for Broadcom 7xxx SOCs internal PHYs (BCM7XXX_PHY) [N/m/?]
Broadcom UniMAC MDIO bus controller (MDIO_BCM_UNIMAC) [N/m/?]
Huawei NCM embedded AT channel support (USB_NET_HUAWEI_CDC_NCM) [N/m/?]
CoreChip-sz SR9700 based USB 1.1 10/100 ethernet devices (USB_NET_SR9700) [N/m/?]
CoreChip-sz SR9800 based USB 2.0 10/100 ethernet devices (USB_NET_SR9800) [N/m/?]
Atheros ath9k ACK timeout estimation algorithm (EXPERIMENTAL) (ATH9K_DYNACK) [N/y/?]
Wake on Wireless LAN support (EXPERIMENTAL) (ATH9K_WOW) [N/y/?]
Channel Context support (ATH9K_CHANNEL_CONTEXT) [N/y/?]
Atheros 802.11ac wireless cards support (ATH10K) [N/m/?]
Qualcomm Atheros WCN3660/3680 support (WCN36XX) [N/m/?]
PCIE bus interface support for FullMAC driver (BRCMFMAC_PCIE) [N/y/?]
Enable broadcast filtering (IWLWIFI_BCAST_FILTERING) [N/y/?]
enable U-APSD by default (IWLWIFI_UAPSD) [N/y/?]
rt2800usb - Include support for rt3573 devices (EXPERIMENTAL) (RT2800USB_RT3573) [N/y/?]
Realtek RTL8723BE PCIe Wireless Network Adapter (RTL8723BE) [N/m/?]
Realtek RTL8192EE Wireless Network Adapter (RTL8192EE) [N/m/?]
Realtek RTL8821AE/RTL8812AE Wireless Network Adapter (RTL8821AE) [N/m/?]
CW1200 WLAN support (CW1200) [N/m/?]
Redpine Signals Inc 91x WLAN driver support (RSI_91X) [N/m/?]
Verbose reason code reporting (ISDN_CAPI_CAPIDRV_VERBOSE) [N/y/?]
Cypress TrueTouch Gen4 Touchscreen Driver (TOUCHSCREEN_CYTTSP4_CORE) [N/m/y/?]
Samsung SUR40 (Surface 2.0/PixelSense) touchscreen (TOUCHSCREEN_SUR40) [N/m/y/?]
Neonode zForce infrared touchscreens (TOUCHSCREEN_ZFORCE) [N/m/?]
Generic GPIO Beeper support (INPUT_GPIO_BEEPER) [N/m/y/?]
IdeaPad Laptop Slidebar (INPUT_IDEAPAD_SLIDEBAR) [N/m/y/?]
Windows-compatible SoC Button Array (INPUT_SOC_BUTTON_ARRAY) [N/m/?]
TI DRV260X haptics support (INPUT_DRV260X_HAPTICS) [N/m/?]
TI DRV2667 haptics support (INPUT_DRV2667_HAPTICS) [N/m/?]
Support for Fintek F81216A LPC to 4 UART (SERIAL_8250_FINTEK) [N/m/y/?]
SC16IS7xx serial support (SERIAL_SC16IS7XX) [N/m/?]
Freescale lpuart serial port support (SERIAL_FSL_LPUART) [N/m/y/?]
Probe for all possible IPMI system interfaces by default (IPMI_SI_PROBE_DEFAULTS) [N/y/?]
TPM Interface Specification 1.2 Interface (I2C - Atmel) (TCG_TIS_I2C_ATMEL) [N/m/?]
TPM Interface Specification 1.2 Interface (I2C - Nuvoton) (TCG_TIS_I2C_NUVOTON) [N/m/?]
Xillybus generic FPGA interface (XILLYBUS) [N/m/y/?]
pinctrl-based I2C multiplexer (I2C_MUX_PINCTRL) [N/m/?]
RobotFuzz Open Source InterFace USB adapter (I2C_ROBOTFUZZ_OSIF) [N/m/?]
ChromeOS EC tunnel I2C bus (I2C_CROS_EC_TUNNEL) [N/m/?]
SPMI support (SPMI) [N/m/y/?]
Debug PINCTRL calls (DEBUG_PINCTRL) [N/y/?]
Intel Baytrail GPIO pin control (PINCTRL_BAYTRAIL) [N/y/?]
Synopsys DesignWare APB GPIO driver (GPIO_DWAPB) [N/m/y/?]
F71882FG and F71889F GPIO support (GPIO_F7188X) [N/m/y/?]
SMSC SCH311x SuperI/O GPIO (GPIO_SCH311X) [N/m/y/?]
Intel Mid GPIO support (GPIO_INTEL_MID) [N/y/?]
Dual Channel Addressable Switch 0x12 family support (DS2406) (W1_SLAVE_DS2406) [N/m/?] 
TI BQ24190 battery charger driver (CHARGER_BQ24190) [N/m/?]
TI BQ24735 battery charger support (CHARGER_BQ24735) [N/m/?]
Generic interrupt trigger (IIO_INTERRUPT_TRIGGER) [N/m/?] (NEW)
GMT G762 and G763 (SENSORS_G762) [N/m/?] 
Lattice POWR1220 Power Monitoring (SENSORS_POWR1220) [N/m/?]
Linear Technology LTC2945 (SENSORS_LTC2945) [N/m/?] 
Linear Technology LTC4222 (SENSORS_LTC4222) [N/m/?]
Linear Technology LTC4260 (SENSORS_LTC4260) [N/m/?]
Measurement Specialties HTU21D humidity/temperature sensors (SENSORS_HTU21) [N/m/?]
Nuvoton NCT6683D (SENSORS_NCT6683) [N/m/?]
TI TPS40422 (SENSORS_TPS40422) [N/m/?]
Sensiron humidity and temperature sensors. SHTC1 and compat. (SENSORS_SHTC1) [N/m/?]
Texas Instruments ADC128D818 (SENSORS_ADC128D818) [N/m/?]
Texas Instruments TMP103 (SENSORS_TMP103) [N/m/?]
Bang Bang thermal governor (THERMAL_GOV_BANG_BANG) [N/y/?]
ACPI INT340X thermal drivers (INT340X_THERMAL) [N/m/?]
Xilinx Watchdog timer (XILINX_WATCHDOG) [N/m/y/?]
Synopsys DesignWare watchdog (DW_WATCHDOG) [N/m/y/?]
MEN A21 VME CPU Carrier Board Watchdog Timer (MEN_A21_WDT) [N/m/y/?]
Support for BCMA in a SoC (BCMA_HOST_SOC) [N/y/?]
Broadcom BCM590xx PMUs (MFD_BCM590XX) [N/m/?]
Kontron module PLD device (MFD_KEMPLD) [N/m/y/?]
MEN 14F021P00 Board Management Controller Support (MFD_MENF21BMC) [N/m/?] 
Realtek USB card reader (MFD_RTSX_USB) [N/m/y/?]
Ricoh RN5T5618 PMIC (MFD_RN5T618) [N/m/?]
TI/National Semiconductor LP3943 MFD Driver (MFD_LP3943) [N/m/?]
TI TPS65218 Power Management chips (MFD_TPS65218) [N/m/?]
Wolfson Microelectronics WM8997 (MFD_WM8997) [N/y/?]
Wolfson Microelectronics WM8994 (MFD_WM8994) [N/m/?]
Active-semi act8865 voltage regulator (REGULATOR_ACT8865) [N/m/?]
Dialog Semiconductor DA9210 regulator (REGULATOR_DA9210) [N/m/?]
Dialog Semiconductor DA9211/DA9212/DA9213/DA9214 regulator (REGULATOR_DA9211) [N/m/?]
Intersil ISL9305 regulator (REGULATOR_ISL9305) [N/m/?]
TI/National Semiconductor LP8720/LP8725 voltage regulators (REGULATOR_LP872X) [N/m/?]
LTC3589 8-output voltage regulator (REGULATOR_LTC3589) [N/m/?]
Freescale PFUZE100/PFUZE200 regulator driver (REGULATOR_PFUZE100) [N/m/?]
Software defined radio support (MEDIA_SDR_SUPPORT) [N/y/?]
Hisilicon hix5hd2 IR remote control (IR_HIX5HD2) [N/m/?]
ImgTec IR Decoder (IR_IMG) [N/m/?]
DTCS033 (Scopium) USB Astro-Camera Driver (USB_GSPCA_DTCS033) [N/m/?]
Syntek STK1135 USB Camera Driver (USB_GSPCA_STK1135) [N/m/?]
USBTV007 video capture support (VIDEO_USBTV) [N/m/?]
STK1160 USB video capture support (VIDEO_STK1160_COMMON) [N/m/?]
AU0828 Remote Controller support (VIDEO_AU0828_RC) [N/y/?]
DVBSky USB support (DVB_USB_DVBSKY) [N/m/?]
Empia EM28xx analog TV, video capture and/or webcam support (VIDEO_EM28XX_V4L2) [N/m/?]
Bluecherry / Softlogic 6x10 capture cards (MPEG-4/H.264) (VIDEO_SOLO6X10) [N/m/?]
Techwell tw68x Video For Linux (VIDEO_TW68) [N/m/?]
Earthsoft PT3 cards (DVB_PT3) [N/m/?]
Virtual Video Test Driver (VIDEO_VIVID) [N/m/?]
Silicon Labs Si4713 FM Radio Transmitter support with USB (USB_SI4713) [N/m/?]
Silicon Labs Si4713 FM Radio Transmitter support with I2C (PLATFORM_SI4713) [N/m/?]
Thanko's Raremono AM/FM/SW radio support (USB_RAREMONO) [N/m/?]
PTN3460 DP/LVDS bridge (DRM_PTN3460) [N/m/?]
Enable preliminary support for prerelease Intel hardware by default (DRM_I915_PRELIMINARY_HW_SUPPORT) [N/y/?]
DRM Support for bochs dispi vga interface (qemu stdvga) (DRM_BOCHS) [N/m/?]
OpenCores VGA/LCD core 2.0 framebuffer support (FB_OPENCORES) [N/m/y/?]
Simple framebuffer support (FB_SIMPLE) [N/y/?]
Generic GPIO based Backlight Driver (BACKLIGHT_GPIO) [N/m/y/?]
Sanyo LV5207LP Backlight (BACKLIGHT_LV5207LP) [N/m/?]
Rohm BD6107 Backlight (BACKLIGHT_BD6107) [N/m/?]
M2Tech hiFace USB-SPDIF driver (SND_USB_HIFACE) [N/m/?]
Behringer BCD2000 MIDI driver (SND_BCD2000) [N/m/?]
DICE-based DACs (EXPERIMENTAL) (SND_DICE) [N/m/?]
Echo Fireworks board module support (SND_FIREWORKS) [N/m/?] 
BridgeCo DM1000/DM1100/DM1500 with BeBoB firmware (SND_BEBOB) [N/m/?]
Silicon Labs CP2112 HID USB-to-SMBus Bridge support (HID_CP2112) [N/m/?]
ELO USB 4000/4500 touchscreen (HID_ELO) [N/m/?]
MSI GT68xR LED support (HID_GT683R) [N/m/?]
Huion tablets (HID_HUION) [N/m/?]
Logitech HID++ devices support (HID_LOGITECH_HIDPP) [N/m/?]
Penmount touch device (HID_PENMOUNT) [N/m/?]
Synaptics RMI4 device support (HID_RMI) [N/m/?]
Xin-Mo non-fully compliant devices (HID_XINMO) [N/m/?]
Rely on OTG and EH Targeted Peripherals List (USB_OTG_WHITELIST) [N/y/?]
USB 2.0 OTG FSM implementation (USB_OTG_FSM) [N/m/y/?]
FUSBH200 HCD support (USB_FUSBH200_HCD) [N/m/y/?]
FOTG210 HCD support (USB_FOTG210_HCD) [N/m/y/?]
HCD test mode support (USB_HCD_TEST_MODE) [N/y/?]
USB Attached SCSI (USB_UAS) [N/m/?]
Inventra Highspeed Dual Role Controller (TI, ADI, ...) (USB_MUSB_HDRC) [N/m/y/?]
ChipIdea host controller (USB_CHIPIDEA_HOST) [N/y/?]
USB Serial Simple Driver (USB_SERIAL_SIMPLE) [N/m/?]
USB Moxa UPORT Serial Driver (USB_SERIAL_MXUPORT) [N/m/?]
USB EHSET Test Fixture driver (USB_EHSET_TEST_FIXTURE) [N/m/y/?]
USB Link Layer Test driver (USB_LINK_LAYER_TEST) [N/m/y/?]
Tahvo USB transceiver driver (TAHVO_USB) [N/m/?]
USB LED Triggers (USB_LED_TRIG) [N/y/?]
Renesas USDHI6ROL0 SD/SDIO Host Controller support (MMC_USDHI6ROL0) [N/m/?]
MemoryStick Standard device driver (MS_BLOCK) [N/m/?]
LED Support for TI LP8501 LED driver chip (LEDS_LP8501) [N/m/?]
Mellanox Connect-IB HCA support (MLX5_INFINIBAND) [N/m/?]
Verbs support for Cisco VIC (INFINIBAND_USNIC) [N/m/?]
Intersil ISL12057 (RTC_DRV_ISL12057) [N/m/?]
NXP PCF2127 (RTC_DRV_PCF2127) [N/m/?]
nxp PCF85063 (RTC_DRV_PCF85063) [N/m/?]
APM X-Gene RTC (RTC_DRV_XGENE) [N/m/y/?]
Synopsys DesignWare AHB DMA PCI driver (DW_DMAC_PCI) [N/m/y/?]
Humusoft MF624 DAQ PCI card driver (UIO_MF624) [N/m/?] 
Realtek RTL8188EU Wireless LAN NIC driver (R8188EU) [N/m/y/?]
Realtek RTL8723AU Wireless LAN NIC driver (R8723AU) [N/m/?]
Realtek PCI-E Card Reader RTS5208/5288 support (RTS5208) [N/m/y/?]
Broadcom BCM2048 FM Radio Receiver support (I2C_BCM2048) [N/m/?]
TCM825x camera sensor support (DEPRECATED) (VIDEO_TCM825X) [N/m/?]
GCT GDM724x LTE support (LTE_GDM724X) [N/m/?]
Lustre file system client support (LUSTRE_FS) [N/m/?]
Digi Neo and Classic PCI Products (DGNC) [N/m/y/?]
Digi EPCA PCI products (DGAP) [N/m/y/?]
Xilinx FPGA firmware download module (GS_FPGABOOT) [N/m/y/?]
Alienware Special feature control (ALIENWARE_WMI) [N/m/?]
Dell Latitude freefall driver (ACPI SMO8800/SMO8810) (DELL_SMO8800) [N/m/y/?]
HP wireless button (HP_WIRELESS) [N/m/y/?]
Toshiba HDD Active Protection Sensor (TOSHIBA_HAPS) [N/m/y/?]
Intel Rapid Start Technology Driver (INTEL_RST) [N/m/y/?]
Intel Smart Connect disabling driver (INTEL_SMARTCONNECT) [N/m/y/?]
Platform support for Chrome hardware (CHROME_PLATFORMS) [N/y/?]
TI SOC drivers support (SOC_TI) [N/y]
RT8973A EXTCON support (EXTCON_RT8973A) [N/m/?]
SM5502 EXTCON support (EXTCON_SM5502) [N/m/?]
Bosch BMA180/BMA250 3-Axis Accelerometer Driver (BMA180) [N/m/?]
Bosch BMC150 Accelerometer Driver (BMC150_ACCEL) [N/m/?]
Freescale MMA8452Q Accelerometer Driver (MMA8452) [N/m/?]
Kionix 3-Axis Accelerometer Driver (KXCJK1013) [N/m/?]
Microchip Technology MCP3422/3/4/6/7/8 driver (MCP3422) [N/m/?]
Nuvoton NAU7802 ADC driver (NAU7802) [N/m/?]
BOSCH BMG160 Gyro Sensor (BMG160) [N/m/?]
AL3320A ambient light sensor (AL3320A) [N/m/?]
APDS9300 ambient light sensor (APDS9300) [N/m/?]
CM32181 driver (CM32181) [N/m/?]
CM36651 driver (CM36651) [N/m/?]
Sharp GP2AP020A00F Proximity/ALS sensor (GP2AP020A00F) [N/m/?]
Intersil ISL29125 digital color light sensor (ISL29125) [N/m/?]
HID PROX (HID_SENSOR_PROX) [N/m/?]
LTR-501ALS-01 light sensor (LTR501) [N/m/?]
TAOS TCS3414 digital color sensor (TCS3414) [N/m/?]
TAOS TCS3472 color light-to-digital converter (TCS3472) [N/m/?]
TAOS TSL4531 ambient light sensors (TSL4531) [N/m/?]
Asahi Kasei AK09911 3-axis Compass (AK09911) [N/m/?]
Freescale MAG3110 3-Axis Magnetometer (MAG3110) [N/m/?]
MLX90614 contact-less infrared sensor (MLX90614) [N/m/?]
TMP006 infrared thermopile sensor (TMP006) [N/m/?]
Intel Non-Transparent Bridge support (NTB) [N/m/y/?]
FMC support (FMC) [N/m/y/?]
Btrfs assert support (BTRFS_ASSERT) [N/y/?]
Overlay filesystem support (OVERLAY_FS) [N/m/y/?]
HFS+ POSIX Access Control Lists (HFSPLUS_FS_POSIX_ACL) [N/y/?]
F2FS Security Labels (F2FS_FS_SECURITY) [N/y/?]
F2FS consistency checking feature (F2FS_CHECK_FS) [N/y/?]
Provide Security Label support for NFSv4 server (NFSD_V4_SECURITY_LABEL) [N/y/?]
Enable Ceph client caching support (CEPH_FSCACHE) [N/y/?]
Ceph POSIX Access Control Lists (CEPH_FS_POSIX_ACL) [N/y/?]
9P Security Labels (9P_FS_SECURITY) [N/y/?]
Detect stack corruption on calls to schedule() (SCHED_STACK_END_CHECK) [N/y/?]
Wait/wound mutex debugging: Slowpath testing (DEBUG_WW_MUTEX_SLOWPATH) [N/y/?]
torture tests for locking (LOCK_TORTURE_TEST) [N/m/y/?]
Debug priority linked list manipulation (DEBUG_PI_LIST) [N/y/?]
Add tracepoint that benchmarks tracepoints (TRACEPOINT_BENCHMARK) [N/y/?]
Per cpu operations test (PERCPU_TEST) [N/m/?]
Perform selftest on resizable hash table (TEST_RHASHTABLE) [N/y/?]
Test module loading with 'hello world' module (TEST_LKM) [N/m/?]
Test user/kernel boundary protections (TEST_USER_COPY) [N/m/?]
Test BPF filter functionality (TEST_BPF) [N/m/?]
Test firmware loading via userspace interface (TEST_FIRMWARE) [N/m/y/?]
udelay test driver (TEST_UDELAY) [N/m/y/?]
Early printk via the EFI framebuffer (EARLY_PRINTK_EFI) [N/y/?]
Debug alternatives (X86_DEBUG_STATIC_CPU_HAS) [N/y/?]
Enable register of persistent per-UID keyrings (PERSISTENT_KEYRINGS) [N/y/?]
Large payload keys (BIG_KEYS) [N/y/?]
Digital signature verification using multiple keyrings (INTEGRITY_SIGNATURE) [N/y/?]
EVM support (EVM) [N/y/?] 
Software async multi-buffer crypto daemon (CRYPTO_MCRYPTD) [N/m/y/?]
LZ4 compression algorithm (CRYPTO_LZ4) [N/m/y/?]
LZ4HC compression algorithm (CRYPTO_LZ4HC) [N/m/y/?]
NIST SP800-90A DRBG (CRYPTO_DRBG_MENU) [N/m/y/?]
Support for AMD Cryptographic Coprocessor (CRYPTO_DEV_CCP) [N/y/?]
Support for Intel(R) DH895xCC (CRYPTO_DEV_QAT_DH895xCC) [N/m/y/?] 
PKCS#7 message parser (PKCS7_MESSAGE_PARSER) [N/m/?]
PRNG perform self test on init (RANDOM32_SELFTEST) [N/y/?]
glob self-test on init (GLOB_SELFTEST) [N/y/?]
Medium-size 6x10 font (FONT_6x10) [N/y/?]
```

## _build-005 - m2y & m/y based_on [14.2_4.4.14_32-smp](http://slackware.osuosl.org/slackware-14.2/source/k/config-x86/)

```
Wacom protocol 4 serial tablet support (TABLET_SERIAL_WACOM4) [N/m/y/?]
Lenovo / Thinkpad devices (HID_LENOVO) [N/m/?]
Sony PS2/3/4 accessories force feedback support (SONY_FF) [N/y/?]
HID Inclinometer 3D (HID_SENSOR_INCLINOMETER_3D) [N/m/?]
HID Device Rotation (HID_SENSOR_DEVICE_ROTATION) [N/m/?]
LED support for PCA963x I2C chip (LEDS_PCA963X) [N/m/?]
Honeywell HMC5843/5883/5883L 3-Axis Magnetometer (I2C) (SENSORS_HMC5843_I2C) [N/m/?]
DHT11 (and compatible sensors) driver (DHT11) [N/m/?]
SI7005 relative humidity and temperature sensor (SI7005) [N/m/?]
HID PRESS (HID_SENSOR_PRESS) [N/m/?]
Freescale MPL115A2 pressure sensor driver (MPL115) [N/m/?]
Freescale MPL3115A2 pressure sensor driver (MPL3115) [N/m/?]
STMicroelectronics pressure sensor Driver (IIO_ST_PRESS) [N/m/?]
EPCOS T5403 digital barometric pressure sensor driver (T5403) [N/m/?]
Thunderbolt support for Apple devices (THUNDERBOLT) [N/m/y/?]
Clock driver for SiLabs 5351A/B/C (COMMON_CLK_SI5351) [N/m/?]
#
# <_-=Added=-_>
#
CONFIG_TABLET_SERIAL_WACOM4=m
CONFIG_HID_LENOVO=m
CONFIG_SONY_FF=y
CONFIG_HID_SENSOR_INCLINOMETER_3D=m
CONFIG_HID_SENSOR_DEVICE_ROTATION=m
CONFIG_LEDS_PCA963X=m
CONFIG_SENSORS_HMC5843_I2C=m
CONFIG_DHT11=m
CONFIG_SI7005=m
CONFIG_HID_SENSOR_PRESS=m
CONFIG_MPL115=m
CONFIG_MPL3115=m
CONFIG_IIO_ST_PRESS=m
CONFIG_T5403=m
CONFIG_THUNDERBOLT=m
CONFIG_COMMON_CLK_SI5351=m
```

***
#### _build-999
```
Stack Protector buffer overflow detection
> 1. None (CC_STACKPROTECTOR_NONE) (NEW)
  2. Regular (CC_STACKPROTECTOR_REGULAR) (NEW)
  3. Strong (CC_STACKPROTECTOR_STRONG) (NEW)
4.4.14 CONFIG_CC_STACKPROTECTOR_REGULAR=y - Regular
def CONFIG_CC_STACKPROTECTOR_NONE=y
# CONFIG_CC_STACKPROTECTOR_REGULAR is not set
# CONFIG_CC_STACKPROTECTOR_STRONG is not set

File decompression options
    > 1. Decompress file data into an intermediate buffer (SQUASHFS_FILE_CACHE) (NEW)
      2. Decompress files directly into the page cache (SQUASHFS_FILE_DIRECT) (NEW)
4.4.14 CONFIG_SQUASHFS_FILE_DIRECT=y
def CONFIG_SQUASHFS_FILE_CACHE=y
# CONFIG_SQUASHFS_FILE_DIRECT is not set

Decompressor parallelisation options
    > 1. Single threaded compression (SQUASHFS_DECOMP_SINGLE) (NEW)
      2. Use multiple decompressors for parallel I/O (SQUASHFS_DECOMP_MULTI) (NEW)
      3. Use percpu multiple decompressors for parallel I/O (SQUASHFS_DECOMP_MULTI_PERCPU) (NEW)
4.4.14 CONFIG_SQUASHFS_DECOMP_MULTI=y
def CONFIG_SQUASHFS_DECOMP_SINGLE=y
# CONFIG_SQUASHFS_DECOMP_MULTI is not set
# CONFIG_SQUASHFS_DECOMP_MULTI_PERCPU is not set
```

