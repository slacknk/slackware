#!/bin/bash

SM="http://slackware.osuosl.org"
SV="14.1"
SIT="http://repository.slacky.eu/slackware-$SV/system/webcore-fonts"
DIR="/tmp/fonts-freetype.subpixel.rendering"

mkdir -p $DIR
cd $DIR

#lftp -c 'open http://slackware.osuosl.org ; mirror slackware-14.1/source/l/freetype'
#lftp -c 'open http://slackware.osuosl.org ; mirror slackware-14.1/source/x/fontconfig'
#lftp -c 'open http://slackware.osuosl.org ; mirror slackware-14.1/source/x/liberation-fonts-ttf'

# mirror
lftp -c 'open $SM ; mirror slackware-$SV/source/l/freetype'
lftp -c 'open $SM ; mirror slackware-$SV/source/x/fontconfig'
lftp -c 'open $SM ; mirror slackware-$SV/source/x/liberation-fonts-ttf'
wget -c $SIT/3.0/webcore-fonts-3.0-i486-8sl.txz 

# patch


# build

freetype 
- chmod +x freetype.SlackBuild
- Uncomment
# The line below enables code patented by Microsoft, so don't uncomment it
# unless you have a license to use the code and take all legal responsibility
# for doing so.
# Please see this web site for more details:
#   http://www.freetype.org/patents.html
#zcat $CWD/freetype.subpixel.rendering.diff.gz | patch -p1 --verbose || exit 1
- and
BUILD=${BUILD:-1nk_fonts}


fontconfig
10-antialias.conf \
10-hinting-slight.conf \
10-hinting.conf \
10-scale-bitmap-fonts.conf \
11-lcdfilter-default.conf \
20-unhint-small-dejavu-lgc-sans-mono.conf \
20-unhint-small-dejavu-lgc-sans.conf \
20-unhint-small-dejavu-lgc-serif.conf \
20-unhint-small-dejavu-sans-mono.conf \
20-unhint-small-dejavu-sans.conf \
20-unhint-small-dejavu-serif.conf \
20-unhint-small-vera.conf \
30-metric-aliases.conf \
30-urw-aliases.conf \
40-nonlatin.conf \
45-latin.conf \
49-sansserif.conf \
50-enable-terminus.conf \
50-user.conf \
51-local.conf \
57-dejavu-sans-mono.conf \
57-dejavu-sans.conf \
57-dejavu-serif.conf \
58-dejavu-lgc-sans-mono.conf \
58-dejavu-lgc-sans.conf \
58-dejavu-lgc-serif.conf \
60-latin.conf \
64-01-tlwg-kinnari.conf \
64-02-tlwg-norasi.conf \
64-11-tlwg-waree.conf \
64-12-tlwg-loma.conf \
64-13-tlwg-garuda.conf \
64-14-tlwg-umpush.conf \
64-21-tlwg-typo.conf \
64-22-tlwg-typist.conf \
64-23-tlwg-mono.conf \
65-droid-sans-fallback.conf \
65-fonts-persian.conf \
65-fonts-takao-gothic.conf \
65-fonts-takao-mincho.conf \
65-fonts-takao-pgothic.conf \
65-khmer.conf \
65-nonlatin.conf \
65-wqy-microhei.conf \
69-unifont.conf \
80-delicious.conf \
89-tlwg-garuda-synthetic.conf \
89-tlwg-kinnari-synthetic.conf \
89-tlwg-loma-synthetic.conf \
89-tlwg-umpush-synthetic.conf \
89-tlwg-waree-synthetic.conf \
90-fonts-nanum.conf \
90-synthetic.conf \
90-ttf-bengali-fonts.conf \
90-ttf-devanagari-fonts.conf \
90-ttf-gujarati-fonts.conf \
90-ttf-kannada-fonts.conf \
90-ttf-malayalam-fonts.conf \
90-ttf-oriya-fonts.conf \
90-ttf-punjabi-fonts.conf \
90-ttf-tamil-fonts.conf \
90-ttf-telugu-fonts.conf \



liberation-fonts-ttf
cat $CWD/60-liberation.conf > $PKG/etc/fonts/conf.avail/60-liberation.conf
#( cd $PKG/etc/fonts/conf.d && \
#  ln -sf ../conf.avail/60-liberation.conf
#


# webcore-fonts
upgradepkg --reinstall --install-new webcore-fonts-3.0-i486-8sl.txz




Uncomments
# The wonderful extended version of the font so generously
# opened up for free modification and distribution by one
# for the previously proprietary font founderies, and that
# Stepan Roh did such a marvelous job on getting the ball
# rolling with should clearly (IMHO) be the default font:
zcat $CWD/fontconfig.dejavu.diff.gz | patch -p1 --verbose || exit 1

# Hardcode the default font search path rather than having
# fontconfig figure it out (and possibly follow symlinks, or
# index ugly bitmapped fonts):
zcat $CWD/fontconfig.font.dir.list.diff.gz | patch -p1 --verbose --backup --suffix=.orig || exit 1



















