#!/bin/sh

# Copyright 2008, 2009, 2010, 2013  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


VERSION=${VERSION:-$(echo fontconfig-*.tar.?z* | rev | cut -f 3- -d . | cut -f 1 -d - | rev)}
VERSION=2.10.93

BUILD=${BUILD:-1nk}

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i486 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

NUMJOBS=${NUMJOBS:-" -j7 "}

CWD=$(pwd)
TMP=${TMP:-/tmp}
PKG=$TMP/package-fontconfig

if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

rm -rf $PKG
mkdir -p $TMP $PKG

cd $TMP
rm -rf fontconfig-$VERSION
tar xvf $CWD/fontconfig-$VERSION.tar.?z* || exit 1
cd fontconfig-$VERSION
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

# The wonderful extended version of the font so generously
# opened up for free modification and distribution by one
# for the previously proprietary font founderies, and that
# Stepan Roh did such a marvelous job on getting the ball
# rolling with should clearly (IMHO) be the default font:
zcat $CWD/fontconfig.dejavu.diff.gz | patch -p1 --verbose || exit 1

# Hardcode the default font search path rather than having
# fontconfig figure it out (and possibly follow symlinks, or
# index ugly bitmapped fonts):
zcat $CWD/fontconfig.font.dir.list.diff.gz | patch -p1 --verbose --backup --suffix=.orig || exit 1

CFLAGS=$SLKCFLAGS \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --mandir=/usr/man \
  --sysconfdir=/etc \
  --with-templatedir=/etc/fonts/conf.avail \
  --with-baseconfigdir=/etc/fonts \
  --with-configdir=/etc/fonts/conf.d \
  --with-xmldir=/etc/fonts \
  --localstatedir=/var \
  --enable-static=no \
  --build=$ARCH-slackware-linux

# Uses a currently non-functional sgml tool, thus '-i':
make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1

# Upstream has changed the default templatedir to /usr/share/fontconfig/conf.avail.
# This change, if accepted, would break any existing font package containing a
# conf.avail directory.  The safest thing to do is to keep things in the
# traditional location, but put a link in the new place so that font packages
# following the new standard location will work.  Let's hear it for being
# "more correct" at the expense of having things "just work"!
mkdir -p $PKG/usr/share/fontconfig
( cd $PKG/usr/share/fontconfig ; ln -sf /etc/fonts/conf.avail . )

mkdir -p $PKG/usr/doc/fontconfig-$VERSION
cp -a \
  AUTHORS COPYING* INSTALL NEWS README \
  $PKG/usr/doc/fontconfig-$VERSION
# You can shop for this kind of stuff in the source tarball.
rm -rf $PKG/usr/share/doc
rmdir $PKG/usr/share 2>/dev/null

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/*-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/var/log/setup
cat $CWD/setup.05.fontconfig > $PKG/var/log/setup/setup.05.fontconfig
chmod 755 $PKG/var/log/setup/setup.05.fontconfig

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

# Set up the default options in /etc/fonts/conf.d:
(  cd $PKG/etc/fonts/conf.d
   for fontconf in \
\
10-antialias.conf \
10-hinting-slight.conf \
10-hinting.conf \
10-scale-bitmap-fonts.conf \
11-lcdfilter-default.conf \
20-unhint-small-dejavu-lgc-sans-mono.conf \
20-unhint-small-dejavu-lgc-sans.conf \
20-unhint-small-dejavu-lgc-serif.conf \
20-unhint-small-dejavu-sans-mono.conf \
20-unhint-small-dejavu-sans.conf \
20-unhint-small-dejavu-serif.conf \
20-unhint-small-vera.conf \
30-metric-aliases.conf \
30-urw-aliases.conf \
40-nonlatin.conf \
45-latin.conf \
49-sansserif.conf \
50-enable-terminus.conf \
50-user.conf \
51-local.conf \
57-dejavu-sans-mono.conf \
57-dejavu-sans.conf \
57-dejavu-serif.conf \
58-dejavu-lgc-sans-mono.conf \
58-dejavu-lgc-sans.conf \
58-dejavu-lgc-serif.conf \
60-latin.conf \
64-01-tlwg-kinnari.conf \
64-02-tlwg-norasi.conf \
64-11-tlwg-waree.conf \
64-12-tlwg-loma.conf \
64-13-tlwg-garuda.conf \
64-14-tlwg-umpush.conf \
64-21-tlwg-typo.conf \
64-22-tlwg-typist.conf \
64-23-tlwg-mono.conf \
65-droid-sans-fallback.conf \
65-fonts-persian.conf \
65-fonts-takao-gothic.conf \
65-fonts-takao-mincho.conf \
65-fonts-takao-pgothic.conf \
65-khmer.conf \
65-nonlatin.conf \
65-wqy-microhei.conf \
69-unifont.conf \
80-delicious.conf \
89-tlwg-garuda-synthetic.conf \
89-tlwg-kinnari-synthetic.conf \
89-tlwg-loma-synthetic.conf \
89-tlwg-umpush-synthetic.conf \
89-tlwg-waree-synthetic.conf \
90-fonts-nanum.conf \
90-synthetic.conf \
90-ttf-bengali-fonts.conf \
90-ttf-devanagari-fonts.conf \
90-ttf-gujarati-fonts.conf \
90-ttf-kannada-fonts.conf \
90-ttf-malayalam-fonts.conf \
90-ttf-oriya-fonts.conf \
90-ttf-punjabi-fonts.conf \
90-ttf-tamil-fonts.conf \
90-ttf-telugu-fonts.conf \
\
        20-unhint-small-vera.conf \
        30-urw-aliases.conf \
        30-metric-aliases.conf \
        40-nonlatin.conf \
        45-latin.conf \
        49-sansserif.conf \
        50-user.conf \
        51-local.conf \
        60-latin.conf \
        65-fonts-persian.conf \
        65-nonlatin.conf \
        69-unifont.conf \
        80-delicious.conf \
        90-synthetic.conf ; do
     if [ -r ../conf.avail/$fontconf ]; then
       ln -sf ../conf.avail/$fontconf .
     else
       echo "ERROR:  unable to symlink ../conf.avail/$fontconf, file does not exist."
       exit 1
     fi
   done
   if [ ! $? = 0 ]; then
     exit 1
   fi
)
if [ ! $? = 0 ]; then
  echo "Missing /etc/fonts/$fontconf default.  Exiting"
  exit 1
fi

# Fix manpages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

mkdir $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n --prepend $TMP/fontconfig-$VERSION-$ARCH-$BUILD.txz

#upgradepkg --reinstall --install-new $TMP/fontconfig-$VERSION-$ARCH-$BUILD.txz
