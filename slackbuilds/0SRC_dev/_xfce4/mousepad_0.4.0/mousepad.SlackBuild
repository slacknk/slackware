#!/bin/sh
# Slackware build script for mousepad 

# Copyright 2014 Aaditya Bagga <aaditya_gnulinux@zoho.com>
# All rights reserved.
#
# Permission to use, copy, modify, and distribute this software for any purpose
# with or without fee is hereby granted, provided that the above copyright
# notice and this permission notice appear in all copies.
#
# THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# AUTHORS AND COPYRIGHT HOLDERS AND THEIR CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# 2016 Modified by NK

# Notes!
# Package version 0.4.0 requirements gtksourceview-3.0
# sbopkg -Bki gtksourceview3


PRGNAM=mousepad

#VERSION=${VERSION:-0.3.0}
 VERSION=${VERSION:-0.4.0}

BUILD=${BUILD:-1}
TAG=${TAG:-_SBo}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
  	i686) ARCH=i686 ;;
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi


#CWD=$(pwd)
[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`

#TMP=${TMP:-/tmp/SBo}
TMP=${TMP:-/tmp/SBo/$PRGNAM}

PKG=$TMP/package-$PRGNAM

#OUTPUT=${OUTPUT:-/tmp}
OUTPUT=${OUTPUT:-$CWD}

PKGTYPE=${PKGTYPE:-txz}
PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
[ -f $PACKAGE ] && rm -vf $PACKAGE


if [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi



set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP



[ -d $PKG/install ] && rm -rfv $PKG/install
mkdir -p $PKG/install
for f in slack-desc doinst.sh ; do
 [ ! -f $f ] && wget -c https://slackbuilds.org/slackbuilds/14.1/desktop/$PRGNAM/$f
 cat $f > $PKG/install/$f
done


rm -rf $PRGNAM-$VERSION


#tar xvf $CWD/$PRGNAM-$VERSION.tar.bz2
WWWSRC=http://archive.xfce.org/src/apps/mousepad
[ ! -f $PRGNAM-$VERSION.tar.bz2 ] && wget $WWWSRC/${VERSION%.*}/$PRGNAM-$VERSION.tar.bz2
tar xvf $PRGNAM-$VERSION.tar.bz2



cd $PRGNAM-$VERSION


#chown -R root:root .
test $(id -u) -eq 0 && chown -R root:root .


find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
 -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;


CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --docdir=/usr/doc/$PRGNAM-$VERSION \
  --build=$ARCH-slackware-linux \
  \
  --disable-debug || exit 1
# --disable-gtk3

#make 
x=$(cat /proc/cpuinfo | grep processor | wc -l)
[ -n "$x" ] && let "NUMJOBS=x+1" || NUMJOBS=2
make -j${NUMJOBS} || make -j`nproc` || make || exit 1

make install DESTDIR=$PKG


find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true



mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
DOCS="AUTHORS COPYING ChangeLog INSTALL NEWS README"
cp -a $DOCS $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

#mkdir -p $PKG/install
#cat $CWD/slack-desc > $PKG/install/slack-desc
#cat $CWD/doinst.sh > $PKG/install/doinst.sh

#cd $PKG
#/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}



ROOTCOMMAND1="cd $PKG ; /sbin/makepkg -l y -c n $PACKAGE"
ROOTCOMMAND2="cd $CWD ; /sbin/upgradepkg --install-new --reinstall $PACKAGE"
ROOTCOMMAND3="rm -rf $PKG"
ROOTCOMMAND4="[ "$TMP" != "/tmp" ] && rm -rf $TMP"

# root-build
ROOTCOMMANDS="$ROOTCOMMAND1 && $ROOTCOMMAND2 && $ROOTCOMMAND3 && $ROOTCOMMAND4"

# user-build
ROOTCOMMANDSU="cd $PKG ; chown -R root:root . && $ROOTCOMMANDS"

# build
if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMANDS"
else
 for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ; echo
 echo -e "\n\n\n\e[1mPlease enter your admin password for makepkg and installpkg\e[0m"
 #sudo su -c "$ROOTCOMMANDSU" || /bin/su -c "$ROOTCOMMANDSU"
 su_sudo (){ echo -e "\n1) Enter passw for sudo (sudo su -c)"; sudo su -c "$ROOTCOMMANDSU" ; }
 su_root (){ echo -e "\n2) Enter root's passw (/bin/su -c)"  ; /bin/su -c "$ROOTCOMMANDSU" ; }
 su_sudo || su_root
fi
