#!/bin/sh

# Slackware build script for Maximus

# Copyright 2015 NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# Notes!
# libfakekey
# libgnomecanvas gnome-mime-data ORBit2 libbonobo gnome-vfs libgnome libbonoboui libgnomeui


PRGNAM=maximus
VERSION=${VERSION:-0.4.14}
BUILD=${BUILD:-1nk}
PKGTYPE=${PKGTYPE:-txz}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i686) ARCH=i686 ;;
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi


#CWD=$(pwd)
[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`

TMP=${TMP:-/tmp/SBo/$PRGNAM}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-$CWD}

set  -e

[ -d "$PKG" ] && rm -rf $PKG || sudo su -c "rm -rf $PKG" || /bin/su -c "rm -rf $PKG"
mkdir -p $TMP $PKG $OUTPUT
[ -d "$TMP/$PRGNAM-$VERSION" ] && rm -rf $TMP/$PRGNAM-$VERSION


cd $TMP

if [ ! -f ${PRGNAM}-${VERSION}.tar.gz ]; then
 wget -c https://launchpad.net/maximus/${VERSION%.*}/ubuntu-9.10/+download/${PRGNAM}-${VERSION}.tar.gz
fi
tar xfv ${PRGNAM}-${VERSION}.tar.*

cd $PRGNAM-$VERSION


test $(id -u) -eq 0 && chown -R root:root .

find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;


set -e


patch -R -n configure ${CWD}/configure.patch
patch -p1 < ${CWD}/1.patch


LDFLAGS="$SLKLDFLAGS" \
CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
--prefix=/usr \
--libdir=/usr/lib${LIBDIRSUFFIX} \
--docdir=/usr/doc/$PRGNAM-$VERSION \
--mandir=/usr/man \
--sysconfdir=/etc \
--localstatedir=/var \
--program-prefix= \
--program-suffix= \
--disable-static \
--build=$ARCH-slackware-linux \
--host=$ARCH-slackware-linux \
--disable-schemas-install || exit 1

x=$(cat /proc/cpuinfo | grep processor | wc -l)
[ -n "$x" ] && let "NUMJOBS=x+1" || NUMJOBS=2
make -j${NUMJOBS}

make install DESTDIR=$PKG

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true


# man
if [ -d "$PKG/usr/man" ]; then
 find $PKG/usr/man -type f -exec gzip -9 {} \;
 for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done
fi

# docs
[ ! -d $PKG/usr/doc/$PRGNAM-$VERSION ] && mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
D="\
 AUTHORS COPYING* ChangeLog Changelog CONTRIBUTORS CREDITS DEPENDENCIES FAQ gpl.txt \
 INSTALL LICENSE license NEWS README* readme* Resources* THANKS* TODO ToDo VERSION "
for d in ${D}; do [ ! -z "`ls $d`" ] && DOC+="$d "; done
cp -av $DOC $PKG/usr/doc/$PRGNAM-$VERSION/
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
chmod 644 $PKG/usr/doc/$PRGNAM-$VERSION/*


# slack-desc
mkdir -p $PKG/install
cat << EOF > $PKG/install/slack-desc
       |-----handy-ruler------------------------------------------------------|
maximus: maximus (Maximus)
maximus:
maximus: A desktop daemon which will automatically maximise and, optionally, 
maximus: un-decorate windows. Has support for exclusion lists and will work 
maximus: with any EWMH-spec compliant window-manager.
maximus:
maximus:
maximus:
maximus:
maximus:
maximus: Homepage: https://launchpad.net/maximus
EOF
sed -e '1d' -i $PKG/install/slack-desc


# doinst.sh
[ -f "$CWD/doinst.sh" ] && cat $CWD/doinst.sh > $PKG/install/doinst.sh


PACKAGE=$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE


# root-build
ROOTCOMMAND1="cd $PKG ; /sbin/makepkg -l y -c n $PACKAGE"
ROOTCOMMAND2="cd $CWD ; /sbin/upgradepkg --install-new --reinstall $PACKAGE"
ROOTCOMMAND3="rm -rf $PKG"
ROOTCOMMAND4="[ "$TMP" != "/tmp" ] && rm -rf $TMP"
ROOTCOMMANDS="$ROOTCOMMAND1 && $ROOTCOMMAND2 && $ROOTCOMMAND3 && $ROOTCOMMAND4"

# user-build
ROOTCOMMANDSU="cd $PKG ; chown -R root:root . && $ROOTCOMMANDS"

# build
if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMANDS"
else
 echo -e "\n\e[1mPlease enter your admin password for makepkg and installpkg\e[0m"
 sudo su -c "$ROOTCOMMANDSU" || /bin/su -c "$ROOTCOMMANDSU"
fi
