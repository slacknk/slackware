#!/bin/sh

# Slackware build script for <appname>

# Copyright 2018 NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


PRGNAM=
VERSION=${VERSION:-}
#ARCH=noarch
BUILD=${BUILD:-1}
TAG=${TAG:-nk}


if [ -z "$ARCH" ]; then
 case "$( uname -m )" in
   i686) ARCH=i686 ;;
   i?86) ARCH=i586 ;;
   arm*) ARCH=arm  ;;
      *) ARCH=$( uname -m ) ;;
 esac
fi

if [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi


if [ -f /var/log/packages/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG ]; then
 echo -e "\n\nNot found newest;"
 echo -e "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG - this version already installed\n"
 rm -rfv $PKG ; [[ $TMP = /tmp/SBo/$PRGNAM ]] && rm -rf $TMP ; exit 1
else
 echo -e "\n\n\n"; PKGLOG="/var/log/packages/$PRGNAM-$VERSION-*"
 [ -f $PKGLOG ] && echo -e "-`ls $PKGLOG | sed "s|/var/log/packages/$PRGNAM-||;s|-$ARCH-|_|"`"
 echo -e "+${VERSION}_$BUILD$TAG\n\n\n"
fi


[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`
TMP=${TMP:-/tmp/SBo/$PRGNAM}
PKG=$TMP/package-$PRGNAM
rm -rf $PKG || sudo su -c "rm -rf $PKG" || /bin/su -c "rm -rf $PKG"
mkdir -p $PKG 

set -e
cd $TMP

SRCWWW=
SRCTAR=$VERSION.tar.gz
SRCDIR=$PRGNAM-$VERSION
[ -f $SRCTAR ] || wget --user-agent='WGET' -c $SRCWWW/$SRCTAR
rm -rf $SRCDIR
tar xvf $SRCTAR
cd $SRCDIR


test $(id -u) -eq 0 && chown -R root:root .

find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;


# autoreconf -fiv
[ -x autogen.sh ] && CFLAGS="$SLKCFLAGS" CXXFLAGS="$SLKCFLAGS" ./autogen.sh

LDFLAGS="$SLKLDFLAGS" \
CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
 --prefix=/usr \
 --libdir=/usr/lib${LIBDIRSUFFIX} \
 --docdir=/usr/doc/$PRGNAM-$VERSION \
 --mandir=/usr/man \
 --infodir=/usr/info \
 --sysconfdir=/etc \
 --localstatedir=/var \
 --infodir=/usr/info \
 \
 --program-prefix= \
 --program-suffix= \
 \
 --disable-static \
 --enable-static=no \
 \
 --build=$ARCH-slackware-linux \
 --host=$ARCH-slackware-linux \
 \
 --disable-debug \
 --disable-systemd \
 \
 --with-gtk=gtk2 \
 --disable-gtk3 || exit 1

x=$(cat /proc/cpuinfo | grep processor | wc -l)
[ -n "$x" ] && let "NUMJOBS=x+1" || NUMJOBS=2
make -j${NUMJOBS} || exit 1

make install DESTDIR=$PKG || exit 1


find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true


# man-dir
if [ -d $PKG/usr/share/man/ ]; then
 mkdir -p $PKG/usr/man
 mv $PKG/usr/share/man/* $PKG/usr/man
 rmdir -v $PKG/usr/share/man
fi

# man-pages
if [ -d $PKG/usr/man ]; then
 find $PKG/usr/man -type f -exec gzip -9 {} \;
 for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done
fi


mkdir -pv $PKG/usr/doc/$PRGNAM-$VERSION ; D=""
D+="AUTHORS COPYING* copying* CHANGE* ?hange?og* CONTRIBUT* CREDIT* Credit* DEPENDENCIES "
D+="FAQ faq* gpl* lgpl* HACKING HISTORY* INSTALL KDE4FAQ LICENSE* *license* MAINTAINERS "
D+="NEWS *Preview* *preview* PORT* PKG-INFO README* readme* Resources* THANKS* Thanks* "
D+="TODO* ToDo* todo* TRANSLATORS VERSION version* WhatsNew* "
D+="*.rules debian/changelog debian/control debian/copyright "
DOCS=""; for d in ${D}; do [ ! -z "`ls $d`" ] && DOCS+="$d "; done
[[ ! -z $DOCS ]] && mv -v $DOCS $PKG/usr/doc/$PRGNAM-$VERSION/ || exit 1

[[ ! -z "`ls *.md`" ]] && md="`ls *.md`" && mv -v $md $PKG/usr/doc/$PRGNAM-$VERSION/

cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
sed -e 's/for <appname>/for '${PRGNAM}'/' -i $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

find $PKG/usr/doc/$PRGNAM-$VERSION/ -type f -exec chmod 644 {} \;
find $PKG/usr/doc/$PRGNAM-$VERSION/ -type d -exec chmod 755 {} \;


mkdir -p $PKG/install
cat << EOF > $PKG/install/slack-desc
       |-----handy-ruler------------------------------------------------------|
appname:  ()
appname:
appname:
appname:
appname:
appname:
appname:
appname: Homepage: 
appname:
appname: Code repository: 
appname:
EOF
sed -e '1d' -e 's/appname:/'${PRGNAM}':/' -i $PKG/install/slack-desc


#cd $PKG
#/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-tgz}


# Build and install package // PKGTYPE=${PKGTYPE:-txz}
# PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}"
OUTPUT=${OUTPUT:-$CWD} ; [ $OUTPUT != $CWD ] && mkdir -pv $OUTPUT
PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}"
[ -f $PACKAGE ] && rm -vf $PACKAGE

ROOTCOMMAND1="cd $PKG ; /sbin/makepkg -l y -c n $PACKAGE"
ROOTCOMMAND2="cd $CWD ; /sbin/upgradepkg --install-new --reinstall $PACKAGE"
ROOTCOMMAND3="rm -rf $PKG"
ROOTCOMMAND4="[ "$TMP" != "/tmp" ] && rm -rf $TMP"

ROOTCOMMANDS="$ROOTCOMMAND1 && $ROOTCOMMAND2 && $ROOTCOMMAND3 && $ROOTCOMMAND4"
ROOTCOMMANDSU="cd $PKG ; chown -R root:root . && $ROOTCOMMANDS"

if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMANDS"
else
 for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ; echo
 echo -e "\n\n\n\e[1mPlease enter your admin password for makepkg and installpkg\e[0m"
#sudo su -c "$ROOTCOMMANDSU" || /bin/su -c "$ROOTCOMMANDSU"
 su_sudo (){ echo -e "\n1) Enter passw for sudo (sudo su -c)"; sudo su -c "$ROOTCOMMANDSU" ; }
 su_root (){ echo -e "\n2) Enter root's passw 	(/bin/su -c)"; /bin/su -c "$ROOTCOMMANDSU" ; }
 su_sudo || su_root
fi
