#!/bin/sh

# Slackware build script for Icons: moka, faba and faba-mono
# from the open source projects of Sam Hewitt < https://snwh.org/ >

# Copyright 2016 NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



[ -z $PRGNAM ] && exit 1



#PRGNAM=faba-icon-theme
#VERSION=${VERSION:-4.0.8}

ARCH=noarch
BUILD=${BUILD:-1}

#TAG=${TAG:-nk}
 TAG=${TAG:-_SBo}

PKGTYPE=${PKGTYPE:-txz}


[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`
[ -f "`pwd`/snwh-icons.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`

TMP=${TMP:-/tmp/SBo/$PRGNAM}
PKG=$TMP/package-$PRGNAM
rm -rf $PKG || sudo su -c "rm -rf $PKG" || /bin/su -c "rm -rf $PKG"
mkdir -pv $PKG


set -e
cd $TMP


# git
GIT=https://github.com/moka-project/$PRGNAM
if [ ! -d "${PRGNAM}.git" ]; then
 git clone $GIT ${PRGNAM}.git
 cd ${PRGNAM}.git
else 
 cd ${PRGNAM}.git
 git fetch origin && rm -rf * && git reset --hard origin/master
fi


# git-version
VERGIT=`git tag -l | sort -V | tail -n1 | sed 's/v//'`
[ -z $VERGIT ] && [ $PRGNAM == faba-mono-icons ] && VERGIT="4.2"
VERDAT=`git log -n1 --date=short --pretty=format:'%ad' | sort -r | head -n1 | sed 's/-//g'`
VERSION=${VERGIT}_git${VERDAT}

if [ ! -z "`ls /var/log/packages/ | grep $PRGNAM | grep $VERSION`" ]; then
 echo -e "\n\nNot found newest;"
 echo -e "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG - this version already installed\n"
 rmdir -v $PKG
 exit 1
else
 echo -e "\n\n$VERSION\n\n"
fi



#find ./Moka -name "*@*" -type d -exec rm -rf {} ";"
[ $PRGNAM == moka-icon-theme ] && rm -rfv ./Moka/*@2x/



# permissions
test $(id -u) -eq 0 && chown -R root:root .

find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;


mkdir -p $PKG/usr/share/icons/
[ $PRGNAM == moka-icon-theme ] && mv ./Moka $PKG/usr/share/icons/
[ $PRGNAM == faba-icon-theme ] && mv ./Faba $PKG/usr/share/icons/
[ $PRGNAM == faba-mono-icons ] && mv ./Faba-* $PKG/usr/share/icons/


# slack-desc
mkdir -p $PKG/install

if [ $PRGNAM == moka-icon-theme ]; then
cat << EOF > $PKG/install/slack-desc
       |-----handy-ruler------------------------------------------------------|
appname: moka-icon-theme (Moka Icon Theme)
appname:
appname: Moka is a stylized Linux desktop icon set, designed to be clear,
appname: simple and consistent.
appname:
appname: Moka Icon Theme (the icon assets and sources) are licensed under a 
appname: Creative Commons Attribution-ShareAlike 4.0 license.
appname:
appname: Official Moka Icon Theme source repository:
appname: 	https://github.com/moka-project/moka-icon-theme
appname:
EOF
elif [ $PRGNAM == faba-icon-theme ]; then
cat << EOF > $PKG/install/slack-desc
       |-----handy-ruler------------------------------------------------------|
appname: faba-icon-theme (Faba Icon Theme)
appname:
appname: Faba is a sexy and modern icon theme with Tango influences. All 
appname: variations and supplementary themes for Faba, require this base 
appname: theme. 
appname:
appname: Faba Icon Theme is distributed under the terms of the GNU LGPL-3.0+ 
appname: or CC-BY-SA-4.0
appname:
appname:
appname: Homepage: https://github.com/moka-project/faba-icon-theme
EOF
elif [ $PRGNAM == faba-mono-icons ]; then
cat << EOF > $PKG/install/slack-desc
       |-----handy-ruler------------------------------------------------------|
appname: faba-mono-icons (Faba Mono Icons)
appname:
appname: Faba Mono Icons is a supplementary theme to Faba Icon Theme. It 
appname: consists only of monochrome panel icons. Faba Mono Icons are 
appname: distributed under the terms of the GNU GPL v.3.
appname:
appname:
appname:
appname: Official Faba Icon Theme (monochrome icons) source repository: 
appname:  https://github.com/moka-project/faba-mono-icons
appname:
EOF
fi

sed -e '1d' -e 's/appname:/'${PRGNAM}':/' -i $PKG/install/slack-desc


# docs
mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
[ ! -d $PKG/usr/doc/$PRGNAM-$VERSION ] && mkdir -pv $PKG/usr/doc/$PRGNAM-$VERSION
D="AUTHORS COPYING* CHANGELOG* copying* ChangeLog Changelog changelog* CONTRIBUTORS "
D+="CREDITS DEPENDENCIES FAQ gpl* lgpl* HACKING INSTALL KDE4FAQ LICENSE* license* "
D+="MAINTAINERS NEWS README* readme* Resources* THANKS* TODO ToDo VERSION version* "
D+="debian/changelog debian/control debian/copyright "
DOC=""; for d in ${D}; do [ ! -z "`ls $d`" ] && DOC+="$d "; done
cp -av $DOC $PKG/usr/doc/$PRGNAM-$VERSION/
#cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
 cat $CWD/*icon*.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
find $PKG/usr/doc/$PRGNAM-$VERSION/ -type f -exec chmod 644 {} \;



#OUTPUT=${OUTPUT:-$CWD}
 OUTPUT=${OUTPUT:-$TMP/..}


PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
ROOTCOMMAND1="cd $PKG ; /sbin/makepkg -l y -c n $PACKAGE"

if [ ! -z `echo $PRGNAM | grep -e theme -e icons` ]; then
 BUILD=3
 PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
 [ ! -d $PKG/usr/doc/$PRGNAM-$VERSION ] && mkdir -pv $PKG/usr/doc/$PRGNAM-$VERSION
  echo -e "WARNING! Package without doinst.sh" 	  >  $PKG/usr/doc/$PRGNAM-$VERSION/doinst.sh-WARNING
  echo -e "\n\nWARNING! For symbolic  links:\n\n" >> $PKG/usr/doc/$PRGNAM-$VERSION/doinst.sh-WARNING
  find $PKG -type l | sed 's|'$PKG'/||' 		  >> $PKG/usr/doc/$PRGNAM-$VERSION/doinst.sh-WARNING
 TAR=`cat /sbin/makepkg  | grep ^TAR= | sed 's/TAR=//'`
 ROOTCOMMAND1="cd $PKG ; $TAR cvf - . | xz -c > $PACKAGE"
 cd /usr/share/icons/
	[ $PRGNAM == moka-icon-theme ] && ( sudo su -c "rm -rfv Moka" 	|| /bin/su -c "rm -rfv Moka" 	)
	[ $PRGNAM == faba-icon-theme ] && ( sudo su -c "rm -rfv Faba" 	|| /bin/su -c "rm -rfv Faba" 	)
	[ $PRGNAM == faba-mono-icons ] && ( sudo su -c "rm -rfv Faba-*" || /bin/su -c "rm -rfv Faba-*" 	)
 cd -
fi

echo -e "\n\n\e[1mBuild\e[0m: ${PACKAGE/$OUTPUT\//}\n\n"
[ -f $PACKAGE ] && rm -vf $PACKAGE

ROOTCOMMAND2="cd $CWD ; /sbin/upgradepkg --install-new --reinstall $PACKAGE"
ROOTCOMMAND3="rm -rf $PKG"
ROOTCOMMAND4="[ "$TMP" != "/tmp" ] && rm -rf $TMP"

# root-build
ROOTCOMMANDS="$ROOTCOMMAND1 && $ROOTCOMMAND2 && $ROOTCOMMAND3" #&& $ROOTCOMMAND4"

# user-build
ROOTCOMMANDSU="cd $PKG ; chown -R root:root . && $ROOTCOMMANDS"

# build
if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMANDS"
else
 for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ; echo
 echo -e "\n\n\n\e[1mPlease enter your admin password for makepkg and installpkg\e[0m"
 #sudo su -c "$ROOTCOMMANDSU" || /bin/su -c "$ROOTCOMMANDSU"
 su_sudo (){ echo -e "\n1) Enter passw for sudo (sudo su -c)"; sudo su -c "$ROOTCOMMANDSU" ; }
 su_root (){ echo -e "\n2) Enter root's passw 	(/bin/su -c)"; /bin/su -c "$ROOTCOMMANDSU" ; }
 su_sudo || su_root
fi
