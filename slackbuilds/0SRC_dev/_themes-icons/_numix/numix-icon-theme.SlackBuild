#!/bin/sh

# Slackware build script for Numix Icon themes

# Copyright 2015-16 NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 PRGNAM=numix-icon-theme
#PRGNAM=numix-icons


#VERSION=${VERSION:-0.3}

# https://launchpad.net/~numix/+archive/ubuntu/ppa/+packages
VERSION=$(\
	wget -qO- http://ppa.launchpad.net/numix/ppa/ubuntu/dists/devel/main/source/Sources.gz \
	| gzip -d | grep -A2 -x "Package: numix-icon-theme" \
	| grep "Version: " | sed 's/Version: //'| sed -r 's/\+.+//' \
)

ARCH=noarch
BUILD=${BUILD:-1}


#TAG=${TAG:-nk}
# https://slackbuilds.org/repository/14.1/desktop/numix-icon-theme/
 TAG=${TAG:-_SBo}


PKGTYPE=${PKGTYPE:-txz}


[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`

TMP=${TMP:-/tmp/SBo/$PRGNAM} #-`date +'%Y%m'`}
mkdir -pv $TMP

PKG=$TMP/_package-$PRGNAM
rm -rf $PKG &> /dev/null || sudo su -c "rm -rf $PKG" || /bin/su -c "rm -rf $PKG"
mkdir -pv $PKG 


set -e

cd $TMP


# files from git Numix Project Ltd.
get_numix_git (){
 echo -e "\n\n$DIR"
 cd $TMP
 GIT=https://github.com/numixproject/$DIR.git
 
 [ ! -d "$DIR" ] && git clone $GIT $DIR && cd $DIR \
 || cd $DIR && git fetch origin && rm -rf * && git reset --hard origin/master

 GITVER=`git log | head -1 | cut -d " " -f 2 | cut -c 1-10`
 GITDAT=`git log -n1 --date=short --pretty=format:'%ad' | sort -r | head -n1 | sed 's/-//g'`

 cp_docs (){
  mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION/$DIRVER
  D="AUTHORS COPYING* CHANGELOG* copying* ChangeLog Changelog changelog* CONTRIBUTORS "
  D+="CREDITS DEPENDENCIES FAQ gpl* lgpl* HACKING INSTALL KDE4FAQ LICENSE* license* "
  D+="MAINTAINERS NEWS README* readme* Resources* THANKS* TODO ToDo VERSION version* "
  D+="debian/changelog debian/control debian/copyright "
  DOC=""; for d in ${D}; do [ ! -z "`ls $d`" ] && DOC+="$d "; done
  mv -v $DOC $PKG/usr/doc/$PRGNAM-$VERSION/$DIRVER
 }
 
 ##
 # first numix-icon-them
###
 if [ "$DIR" == "numix-icon-theme" ]; then
 ##
 #
 #
 
  #VERSION=${GITVER}_git${GITDAT}
  #VERSION=${GITDAT}
  #VERSION=0.0.0_git${GITDAT}_${GITVER}
  #VERSION+=_git${GITDAT}
   VERSION+=.${GITDAT}
  
  #if [ ! -z "`ls /var/log/packages/ | grep numix | grep ${GITDAT}`" ]; then
   if [ ! -z "`ls /var/log/packages/ | grep numix | grep ${VERSION}`" ]; then
    echo -e "\nNot found newest;"
    echo -e "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG - this version already installed\n"
    rm -rfv $PKG
    exit 1
   else
    echo -e "\n\nVERSION: $VERSION\n\n"
   fi
  
  #mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION/
  #mv -v license readme.md $PKG/usr/doc/$PRGNAM-$VERSION
  cp_docs
   
##
 #
 ###
 else
 ###
 #
##
 
  #DIRVER=${DIR/numix-icon-theme-//}-${GITDAT}_${GITVER}
   DIRVER=${DIR/numix-icon-theme-//}_git${GITDAT}
  cp_docs
   
 #
 #
 #
 fi
###

 mkdir -p $PKG/usr/share/icons
 mv * $PKG/usr/share/icons
}


# Icon themes
# -utouch-old,-utouch
for i in numix-icon-theme{,-circle,-shine,-bevel,-square-legacy}; do
 DIR=$i
 get_numix_git
 #echo "${DIR}: ${GITVER}_git${GITDAT}" >> $PKG/usr/doc/$PRGNAM-$VERSION/VERSIONS
 #echo "${GITVER}  git${GITDAT}  ${DIR}">> $PKG/usr/doc/$PRGNAM-$VERSION/VERSIONS
  echo "_git${GITDAT}  ${DIR}"			>> $PKG/usr/doc/$PRGNAM-$VERSION/VERSIONS
done



cd $PKG


# permissions
if test $(id -u) -eq 0; then
 chown -R root:root .
fi

find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;


cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
find $PKG/usr/doc/$PRGNAM-$VERSION/ -type f -exec chmod 644 {} \;


# Del ln -s @2x and any docs
find . -name "*@*" -exec rm -v {} ";"
rm -vf ./usr/share/icons/Numix/128

if [ -f./usr/share/icons/Numix-uTouch/LICENSE ]; then
 #rm -v ./usr/share/icons/Numix-uTouch/LICENSE
 mv -v ./usr/share/icons/Numix-uTouch/LICENSE $PKG/usr/doc/$PRGNAM-$VERSION/license.Numix-uTouch
fi


# slack-desc
#numix-icon-theme: * -uTouch - taking design ques from Ubuntu Touch;
mkdir -p $PKG/install
cat << EOF > $PKG/install/slack-desc
                |-----handy-ruler------------------------------------------------------|
numix-icon-theme: numix-icon-theme (an icon themes from the Numix project)
numix-icon-theme:
numix-icon-theme: Numix Icon Themes: 
numix-icon-theme: *  Numix - Official icon theme from the Numix project;
numix-icon-theme: * -Circle - Numix Circle icon theme;
numix-icon-theme: * -Shine - a glossy design;
numix-icon-theme: * -Bevel - Legacy version of the old Base and Circle icons;
numix-icon-theme: * -Square-Legacy - Legacy version of the old Base and Square icons.
numix-icon-theme:
numix-icon-theme:
numix-icon-theme: Homepage: https://www.numixproject.org/
EOF
sed '1d' -i $PKG/install/slack-desc



#OUTPUT=${OUTPUT:-$CWD}
 OUTPUT=${OUTPUT:-$TMP/..}


PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
ROOTCOMMAND1="cd $PKG ; /sbin/makepkg -l y -c n $PACKAGE"

if [ ! -z `echo $PRGNAM | grep -e theme -e icons` ]; then
 BUILD=3
 PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
 [ ! -d $PKG/usr/doc/$PRGNAM-$VERSION ] && mkdir -pv $PKG/usr/doc/$PRGNAM-$VERSION
  echo -e "WARNING! Package without doinst.sh" 	  >  $PKG/usr/doc/$PRGNAM-$VERSION/doinst.sh-WARNING
  echo -e "\n\nWARNING! For symbolic  links:\n\n" >> $PKG/usr/doc/$PRGNAM-$VERSION/doinst.sh-WARNING
  find $PKG -type l | sed 's|'$PKG'/||' 		  >> $PKG/usr/doc/$PRGNAM-$VERSION/doinst.sh-WARNING
 TAR=`cat /sbin/makepkg  | grep ^TAR= | sed 's/TAR=//'`
 ROOTCOMMAND1="cd $PKG ; $TAR cvf - . | xz -c > $PACKAGE"
 cd /usr/share/icons/
  sudo su -c "rm -rfv Numix*" || /bin/su -c "rm -rfv Numix*"
 cd -
fi

echo -e "\n\n\e[1mBuild\e[0m: ${PACKAGE/$OUTPUT\//}\n\n"
[ -f $PACKAGE ] && rm -vf $PACKAGE


ROOTCOMMAND2="cd $CWD ; /sbin/upgradepkg --install-new --reinstall $PACKAGE"
ROOTCOMMAND3="rm -rf $PKG"
ROOTCOMMAND4="[ "$TMP" != "/tmp" ] && rm -rf $TMP"


# root-build
ROOTCOMMANDS="$ROOTCOMMAND1 && $ROOTCOMMAND2 && $ROOTCOMMAND3" #&& $ROOTCOMMAND4"

# user-build
ROOTCOMMANDSU="cd $PKG ; chown -R root:root . && $ROOTCOMMANDS"

# build
if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMANDS"
else
 for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ; echo
 echo -e "\n\n\n\e[1mPlease enter your admin password for makepkg and installpkg\e[0m"
 #sudo su -c "$ROOTCOMMANDSU" || /bin/su -c "$ROOTCOMMANDSU"
 su_sudo (){ echo -e "\n1) Enter passw for sudo (sudo su -c)"; sudo su -c "$ROOTCOMMANDSU" ; }
 su_root (){ echo -e "\n2) Enter root's passw 	(/bin/su -c)"; /bin/su -c "$ROOTCOMMANDSU" ; }
 su_sudo || su_root
fi
