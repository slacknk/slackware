#!/bin/sh

PRGNAM=mame
BUILD=${BUILD:-1}
TAG=${TAG:-cbz}
PKGTYPE=${PKGTYPE:-txz}

#VERSION=${VERSION:-0.169}
VERSION=${VERSION:-0.185}


case "$( uname -m )" in
 i?86) 	 ARCH=i686 ;;
 x86_64) ARCH=x86_64  ;;
esac

PKG=$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE


if [ -f /var/log/packages/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG ]; then
 echo -e "\n\nNot found newest;"
 echo -e "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG - this version already installed\n"
 exit 1
else
 echo -e "\n\nInstalled: `ls /var/log/packages/$PRGNAM-* | sed 's|/var/log/packages/||'`"
 echo -e "Get and Install: $VERSION\n\n"
 sleep 2s
fi


TMP=${TMP:-/tmp/SBo/mame_for_slack}
mkdir -p $TMP
cd $TMP

WWW=http://downloads.sourceforge.net/project/mameforslack/mame/$VERSION/$ARCH
wget -c $WWW/$PKG.md5 $WWW/$PKG 


ROOTCOMMAND="/sbin/upgradepkg --install-new --reinstall $PKG"

if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMAND"
else
 for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ; echo
 su_sudo (){ echo -e "\n1) Enter passw for sudo (sudo su -c)"; sudo su -c "$ROOTCOMMAND" ; }
 su_root (){ echo -e "\n2) Enter root's passw 	(/bin/su -c)"; /bin/su -c "$ROOTCOMMAND" ; }
 su_sudo || su_root
fi
