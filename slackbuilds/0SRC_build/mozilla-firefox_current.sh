#!/bin/bash


#lftp -c 'open ftp://ftp.slackware.com/pub/slackware/slackware-current/source/xap/ ; mirror mozilla-firefox'
#rsync -r --delete --progress --delete-excluded rsync://taper.alienbase.nl/mirrors/slackware/slackware-current/source/xap/mozilla-firefox


[ -d "/tmp/mozilla-firefox" ] && rm -rf /tmp/mozilla-firefox 
mkdir -p /tmp/mozilla-firefox/ 
cd /tmp/mozilla-firefox/ 
cp -av /home/ftp/slackmirror32/current_slackware/source/xap/mozilla-firefox .
rsync -avrh --delete --progress rsync://taper.alienbase.nl/mirrors/slackware/slackware-current/source/xap/mozilla-firefox .
cd mozilla-firefox


set -e


#-e 's|i?86) export ARCH=i486|i686) export ARCH=i686|' \
#-e 's|export CFLAGS="$SLKCFLAGS"|SLKCFLAGS="-O2 -march=i686 -mtune=i686"|' \
#-e 's|export CXXFLAGS="$SLKCFLAGS"|SLKCFLAGS="-O2 -march=i686 -mtune=i686"|' \
#-e 's|BUILD=${BUILD:-1}|BUILD=${BUILD:-1nk}|' \
#-e 's|TMP=${TMP:-/tmp}|TMP=${TMP:-/Full/PATH/}|' \
#-e 's|NUMJOBS=${NUMJOBS:-" -j7 "}|NUMJOBS="-j`nproc`"|' \
#-e 's|NUMJOBS=${NUMJOBS:-" -j7 "}|NUMJOBS=" -j'${numjobs}' "|' \
#-e 's|-c n $TMP/mozilla-firefox-$VERSION-$ARCH-$BUILD.txz|-c n $CWD/mozilla-firefox-$VERSION-$ARCH-$BUILD.txz|' \

#mkdir -p $PKG/usr/doc/mozilla-firefox-$VERSION/ || exit 1
##cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/mozilla-firefox-$VERSION/mozilla-firefox-.SlackBuild
#cd $CWD
# tar Jcfv $PKG/usr/doc/mozilla-firefox-$VERSION/mozilla-firefox-$VERSION-src-$BUILD.tar.xz \
#  mozilla-firefox.SlackBuild slack-desc \
#  gold/ mozilla-firefox.desktop \
#  mimeTypes.rdf.gz firefox.moz_plugin_path.diff.gz mozilla-firefox-mimeTypes-fix.diff.gz \
#  || exit 1
#cd -


sed \
-e '/BUILD=${BUILD:-/s/}/nk}/' \
-e 's|i586|i686|' \
-e 's|SLKCFLAGS=""|SLKCFLAGS="-O2 -march=i686 -mtune=i686"|' \
-e 's|chown -R root:root .|chown -R root:root . \&\& rm -rf \$CWD/firefox-\$VERSION.source.tar.bz2|' \
-i mozilla-firefox.SlackBuild || exit 1

#echo "" >> mozilla-firefox.SlackBuild

cat << EOF >> mozilla-firefox.SlackBuild
cd \$CWD
upgradepkg --install-new --reinstall \$TMP/mozilla-firefox-\$VERSION-\$ARCH-\$BUILD.txz
rm -rf \$PKG \$TMP/mozilla-\$MOZVERS
EOF

#echo "" >> mozilla-firefox.SlackBuild


#NEWVERSION=""
#NEWVERSION="38.0.1"
#if [ ! -z "$NEWVERSION" ]; then
# rm -v firefox-*.source.tar.bz2
# wget -c 
#fi


x=$(cat /proc/cpuinfo | grep processor | wc -l)
[ -n "$x" ] && let "numjobs=x+1" || numjobs=2

TMP="/tmp/mozilla-firefox/" NUMJOBS=" -j${numjobs} " ./mozilla-firefox.SlackBuild
