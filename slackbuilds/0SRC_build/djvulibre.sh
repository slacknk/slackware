#!/bin/sh

# build script for djvulibre by NK // thx bormant
# http://www.slackware.ru/forum/viewtopic.php?f=34&t=1870


# wget -r -nH --cut-dirs=4 ftp://ftp.osuosl.org/pub/slackware/slackware64-14.2/source/l/djvulibre


rm -r /tmp/djvulibre-*/ /tmp/package-djvulibre /tmp/djvulibre /tmp/djvulibre-*t?z
WWW_slack="rsync.osuosl.org::slackware"
VER_slack=`(. /etc/os-release; echo $VERSION)`
rsync -avrh --delete --progress ${WWW_slack}/slackware-${VER_slack}/source/l/djvulibre /tmp/

cd /tmp/djvulibre
 ARCH="$( uname -m )" BUILD=1nk ./djvulibre.SlackBuild
 upgradepkg --install-new --reinstall /tmp/djvulibre-*t?z
cd -

rm -r /tmp/djvulibre-*/ /tmp/package-djvulibre /tmp/djvulibre
