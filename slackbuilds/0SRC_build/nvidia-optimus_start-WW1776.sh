#!/bin/sh

# SlackBuilds (Slackware build scripts) for Bumblebee and related dependencies
# https://github.com/WhiteWolf1776/Bumblebee-SlackBuilds


[ -d "$HOME/Bumblebee-SlackBuilds" ] && rm -rf $HOME/Bumblebee-SlackBuilds
[ -d "/tmp/Bumblebee-SlackBuilds"  ] && rm -rf /tmp/Bumblebee-SlackBuilds

curl https://raw.githubusercontent.com/ryanpcmcquen/linuxTweaks/master/slackware/crazybee.sh | sh
