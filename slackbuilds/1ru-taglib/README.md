# taglib-rusxmms
This is a directory contents different build/packages LibRCC (library is part of rusxmms patch) with or without [Enca](https://cihar.com/software/enca/) and LibRCD (library used by [RusXMMS project](http://rusxmms.sf.net) for encoding auto-detection).

## build1
need/slack-required: 14.2 > Libraries > [librcd or enca] (librc build w/o installed librcd & enca)
* dynamic - librcd (0.1.11) https://slackbuilds.org/repository/14.2/libraries/librcd/
* dynamic - enca (1.19) https://slackbuilds.org/repository/14.2/libraries/enca/
```
Dynamic Engine Loading Support:        yes
Enca Charset Detection Support:        dynamic
LibRCD Charset Detection Support:      dynamic
```

## build2
need/slack-required: don't work w/o librcd
* librcc build with librcd from this dir and builds w/o enca
```
Enca Charset Detection Support:        dynamic
LibRCD Charset Detection Support:      yes
```

## build3
need/slack-required: don't work w/o enca
* librcc build with enca from _SBo-14.2 and w/o librcd
```
Enca Charset Detection Support:        yes
LibRCD Charset Detection Support:      dynamic
```

## build4
in progress...

```
Configuration:
  POSIX Threading Support:               yes

  External IConv Library:                no
  LibCharset Library:                    no

  Dynamic Engine Loading Support:        yes
  Enca Charset Detection Support:        yes
  LibRCD Charset Detection Support:      yes
  LibGUESS Charset Detection Support:    no

  Multilanguage support with DB4:        no
  Language autodetection using aspell:   yes
  Libtranslate support:                  no
  Libtranslate Timed Translate:          no

User Interfaces:
  GTK User Interface:                    no
  GTK2 User Interface:                   no
  GTK3 User Interface:                   no

Directories:
  RCC Data Directory:                    /usr/lib${LIBDIRSUFFIX}/rcc/
```
