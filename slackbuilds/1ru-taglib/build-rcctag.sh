#!/bin/sh

# Copyright 2018 NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# This script uses the SlackBuild scripts present here to build a
# complete set of taglib-rusxmms (part of rusxmms patch by RusXMMS project).


# Enca Charset Detection Support:
# LibRCD Charset Detection Support:
[ "$( uname -m )" = "x86_64" ] && LIBDIRSUFFIX="64" || LIBDIRSUFFIX=""
SYSDIR="/usr/lib${LIBDIRSUFFIX}"
ENC=dynamic
RCD=dynamic
if [ -e $SYSDIR/librcd.so ] && [ -e $SYSDIR/libenca.so ]; then
 BUILD=4 ; ENC=yes ; RCD=yes
elif [ -e $SYSDIR/libenca.so ]; then
 BUILD=3 ; ENC=yes
elif [ -e $SYSDIR/librcd.so ]; then
 BUILD=2 ; RCD=yes
else
 # dynamic - Enca Charset Detection Support
 # dynamic - LibRCD Charset Detection Support
 BUILD=1
fi

echo -e "\n*************************************************"
echo -e "* reBuilding taglib (BUILD=$BUILD) with LibRCC...\n"
echo -e "*  Enca Charset Detection Support:   $ENC"
echo -e "*  LibRCD Charset Detection Support: $RCD"
echo -e "*************************************************\n"
sleep 3s

cd $(dirname $0) ; CWD0=$(pwd)

cd $CWD0/1librcc && OUTPUT=$CWD0 BUILD=$BUILD ./librcc.SlackBuild || exit 1
cd $CWD0/2taglib && OUTPUT=$CWD0 BUILD=$BUILD ./taglib.SlackBuild || exit 1

echo -e "\n\n\n> rusxmms-taglib packages done!\n"
