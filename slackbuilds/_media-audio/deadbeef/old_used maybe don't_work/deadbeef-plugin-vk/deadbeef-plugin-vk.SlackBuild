#!/bin/sh

# Slackware build script for db-vk

# Copyright 2015 NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# Notes!
# sbopkg -Bki json-glib


PRGNAM=deadbeef-plugin-vk
BUILD=${BUILD:-1nk}
PKGTYPE=${PKGTYPE:-txz}


ARCH="`uname -m`"

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ "$ARCH" = "i686" ]; then
 SLKCFLAGS="-O2 -march=i686 -mtune=i686"
 LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
 SLKCFLAGS="-O2 -fPIC"
 LIBDIRSUFFIX="64"
elif [ "$ARCH" = "i486" ]; then
 SLKCFLAGS="-O2 -march=i486 -mtune=i686"
 LIBDIRSUFFIX=""
else
 SLKCFLAGS="-O2"
 LIBDIRSUFFIX=""
fi


#CWD=$(pwd)
[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`

TMP=/tmp/SBo/$PRGNAM
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-$CWD}


set -e


rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP


# sources
#SRCVER="`date +'%Y%m'`"
 SRCVER="git"
if [ ! -d "$PRGNAM-$SRCVER" ]; then
 git clone https://github.com/scorpp/db-vk $PRGNAM-$SRCVER
 cd $PRGNAM-$SRCVER
else 
 cd $PRGNAM-$SRCVER
 git fetch origin && rm -rf * && sleep 2s && git reset --hard origin/master
fi
GITVER=$(git describe --tags --long | sed 's|v||;s|[_-]|.|g')
GITDAT=$(git log -n1 --date=short --pretty=format:'%ad' | sort -r | head -n1 | sed 's_-__g')
VERSION="${GITVER}_git${GITDAT}"

#if [ -f /var/log/packages/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG ]; then
# echo -e "\nNot found newest;"
# echo -e "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG - this version already installed\n"
# exit 1
#fi


test $(id -u) -eq 0 && chown -R root:root .
chmod -R a-s,u+w,go+r-w .


# cmake
# -DWITH_GTK3=OFF \
CMAKEPREFIX="`kde4-config --prefix`" || CMAKEPREFIX="/usr"
cmake \
-DCMAKE_INSTALL_PREFIX=$CMAKEPREFIX \
-DCMAKE_C_FLAGS:STRING="$SLKCFLAGS" \
-DCMAKE_CXX_FLAGS:STRING="$SLKCFLAGS" \
-DLIB_DIR=lib${LIBDIRSUFFIX} \
-DLIB_SUFFIX=${LIBDIRSUFFIX} \
-DMAN_INSTALL_DIR=/usr/man \
-DCMAKE_BUILD_TYPE=Release \
./

# make
x=$(cat /proc/cpuinfo | grep processor | wc -l)
[ -n "$x" ] && let "NUMJOBS=x+1" || NUMJOBS=2
make -j${NUMJOBS} || make -j`nproc` || make || exit 1

# install
install -D -v -c ./vkontakte_gtk2.so $PKG/usr/lib${LIBDIRSUFFIX}/deadbeef/vkontakte_gtk2.so
install -D -v -c ./vkontakte_gtk3.so $PKG/usr/lib${LIBDIRSUFFIX}/deadbeef/vkontakte_gtk3.so


# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null


# Compress manual
[ -d $PKG/usr/man ] && gzip -9 $PKG/usr/man/man?/*.? 
[ -d $PKG/usr/share/man ] && gzip -9 $PKG/usr/share/man/man?/*.?


# Add a documentation directory:
[ ! -d $PKG/usr/doc/$PRGNAM-$VERSION ] && mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
D="\
 AUTHORS COPYING* ChangeLog Changelog CONTRIBUTORS CREDITS DEPENDENCIES FAQ gpl.txt \
 INSTALL LICENSE license NEWS README* readme* Resources* THANKS* TODO ToDo VERSION "
for d in ${D}; do [ ! -z "`ls $d`" ] && DOC+="$d "; done
cp -av $DOC $PKG/usr/doc/$PRGNAM-$VERSION/
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
chmod 644 $PKG/usr/doc/$PRGNAM-$VERSION/*


# desc
mkdir -p $PKG/install
cat << EOF > $PKG/install/slack-desc
                  |-----handy-ruler------------------------------------------------------|
deadbeef-plugin-vk: deadbeef-plugin-vk (db-vk)
deadbeef-plugin-vk:
deadbeef-plugin-vk: DeadBeef plugin for listening musing from vkontakte.com
deadbeef-plugin-vk:
deadbeef-plugin-vk:
deadbeef-plugin-vk:
deadbeef-plugin-vk:
deadbeef-plugin-vk:
deadbeef-plugin-vk:
deadbeef-plugin-vk:
deadbeef-plugin-vk: Homepage: https://github.com/scorpp/db-vk
EOF
sed '1d' -i $PKG/install/slack-desc


# root-build
ROOTCOMMAND1="cd $PKG ; /sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
ROOTCOMMAND2="cd $CWD ; /sbin/upgradepkg --install-new --reinstall $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
ROOTCOMMAND3="rm -rf $PKG"
ROOTCOMMAND4="[ "$TMP" != "/tmp" ] && rm -rf $TMP"
ROOTCOMMANDS="$ROOTCOMMAND1 && $ROOTCOMMAND2 && $ROOTCOMMAND3"

# user-build
ROOTCOMMANDSU="cd $PKG ; chown -R root:root . && $ROOTCOMMANDS"

# build
if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMANDS"
else
 echo -e "\n\e[1mPlease enter your admin password for makepkg and installpkg\e[0m"
 sudo su -c "$ROOTCOMMANDSU" || /bin/su -c "$ROOTCOMMANDSU"
fi
