#!/bin/sh

# Slackware build script for ddb_vu_meter

# Copyright 2017 NK
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


PRGNAM=deadbeef-plugin-vu-meter
GITNAM=ddb_vu_meter

BUILD=${BUILD:-1}
TAG=${TAG:-nk}


if [ -z "$ARCH" ]; then
 case "$( uname -m )" in
   i686) ARCH=i686 ;;
   i?86) ARCH=i586 ;;
      *) ARCH=$( uname -m ) ;;
 esac
fi

if [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi


#CWD=$(pwd)
[ -f "`pwd`/$PRGNAM.SlackBuild" ] && CWD=$(pwd) || CWD=`dirname $0`
TMP=/tmp/SBo/$PRGNAM
PKG=$TMP/package-$PRGNAM

set -e

rm -rf $PKG || sudo su -c "rm -rf $PKG" || /bin/su -c "rm -rf $PKG"
mkdir -p $TMP $PKG
cd $TMP


# sources
if [ -z $VERSION ]; then
 DIR=${PRGNAM}.git ;  echo -e "\n\n$DIR"
 GIT=https://github.com/cboxdoerfer/$GITNAM
 [ -d "$TMP/$DIR" ] && cd $DIR && git fetch origin && rm -rf * && git reset --hard origin/master
 [ ! -d "$TMP/$DIR" ] && git clone $GIT $DIR && cd $DIR
 GITDAT=`git log -n1 --date=short --pretty=format:'%ad' | sort -r | head -n1 | sed 's/-//g'`
 VERSION="0.0.0_git${GITDAT}"
fi

deadbeef --version &> version_ddb
VERDDB=`cat version_ddb | grep 2009 | awk {'print $2'}`
VERSION+=_$VERDDB ; rm -v version_ddb

if [ -f /var/log/packages/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG ]; then
 echo -e "\nNot found newest;"
 echo -e "$PRGNAM-$VERSION-$ARCH-$BUILD$TAG - this version already installed\n"
 exit 1
else
 echo -e "\n\n$PRGNAM-$VERSION-$ARCH-$BUILD$TAG\n\n"
fi


#WWW=https://raw.githubusercontent.com/eimiss/arch_vu_meter_patch/master
#[ ! -f arch_vu_meter_patch ] && wget -c $WWW/arch_vu_meter_patch
#patch -p1 vumeter.c < arch_vu_meter_patch
#mkdir -pv $PKG/usr/doc/$PRGNAM-$VERSION
#mv -v arch_vu_meter_patch $PKG/usr/doc/$PRGNAM-$VERSION/${PRGNAM}.arch_vu_meter_patch


test $(id -u) -eq 0 && chown -R root:root .
chmod -R a-s,u+w,go+r-w .


# make
x=$(cat /proc/cpuinfo | grep processor | wc -l)
[ -n "$x" ] && let "NUMJOBS=x+1" || NUMJOBS=2
make -j${NUMJOBS} || make -j`nproc` || make || exit 1

# install
install -D -v -c ./gtk2/ddb_vis_vu_meter_GTK2.so $PKG/usr/lib${LIBDIRSUFFIX}/deadbeef/ddb_vis_vu_meter_GTK2.so
install -D -v -c ./gtk3/ddb_vis_vu_meter_GTK3.so $PKG/usr/lib${LIBDIRSUFFIX}/deadbeef/ddb_vis_vu_meter_GTK3.so
install -D -v -c ./vumeter.png $PKG/usr/lib${LIBDIRSUFFIX}/deadbeef/vumeter.png

# Strip binaries:
find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null


# desc
mkdir -p $PKG/install
cat << EOF > $PKG/install/slack-desc
       |-----handy-ruler------------------------------------------------------|
appname: $PRGNAM ($GITNAM)
appname:
appname: VU Meter Plugin for the DeaDBeeF audio player.
appname:
appname:
appname:
appname:
appname:
appname:
appname: Homepage: https://github.com/cboxdoerfer/ddb_vu_meter
appname:
EOF
sed -e '1d' -e 's/appname:/'${PRGNAM}':/' -i $PKG/install/slack-desc


# Add a documentation directory:
mkdir -pv $PKG/usr/doc/$PRGNAM-$VERSION ; D=""
D+="AUTHORS COPYING* CHANGELOG* copying* ChangeLog Changelog *changelog* CONTRIBUTORS "
D+="Credit* CREDIT* DEPENDENCIES FAQ faq* gpl* lgpl* HACKING HISTORY* INSTALL KDE4FAQ "
D+="LICENSE* license* MAINTAINERS NEWS *Preview* *preview* PKG-INFO README* readme*  "
D+="Resources* THANKS* Thanks* TODO ToDo TRANSLATORS VERSION version* WhatsNew* "
D+="*.rules debian/changelog debian/control debian/copyright "
DOCS=""; for d in ${D}; do [ ! -z "`ls $d`" ] && DOCS+="$d "; done
[[ ! -z $DOCS ]] && mv -v $DOCS $PKG/usr/doc/$PRGNAM-$VERSION/ || exit 1

cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild
find $PKG/usr/doc/$PRGNAM-$VERSION/ -type f -exec chmod 644 {} \;


# Make the package:
OUTPUT=${OUTPUT:-$CWD}
PKGTYPE=${PKGTYPE:-txz}
PACKAGE="$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
[ -f $PACKAGE ] && rm -vf $PACKAGE

ROOTCOMMAND1="cd $PKG ; /sbin/makepkg -l y -c n $PACKAGE"
ROOTCOMMAND2="cd $CWD ; /sbin/upgradepkg --install-new --reinstall $PACKAGE"
ROOTCOMMAND3="rm -rf $PKG"
ROOTCOMMAND4="[ "$TMP" != "/tmp" ] && rm -rf $TMP"

ROOTCOMMANDS="$ROOTCOMMAND1 && $ROOTCOMMAND2 && $ROOTCOMMAND3 && $ROOTCOMMAND4"
ROOTCOMMANDSU="cd $PKG ; chown -R root:root . && $ROOTCOMMANDS"

if [ "${UID}" == "0" ]; then
 /bin/su -c "$ROOTCOMMANDS"
else
 for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ; echo
 echo -e "\n\n\n\e[1mPlease enter your admin password for makepkg and installpkg\e[0m"
#sudo su -c "$ROOTCOMMANDSU" || /bin/su -c "$ROOTCOMMANDSU"
 su_sudo (){ echo -e "\n1) Enter passw for sudo (sudo su -c)"; sudo su -c "$ROOTCOMMANDSU" ; }
 su_root (){ echo -e "\n2) Enter root's passw 	(/bin/su -c)"; /bin/su -c "$ROOTCOMMANDSU" ; }
 su_sudo || su_root
fi
