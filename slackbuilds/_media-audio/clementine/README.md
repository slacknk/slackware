## Clementine_1.3.1

latest build_on slackware64-14.2 with
```
protobuf-2.5.0-x86_64-1alien
chromaprint-1.4.3-x86_64-1_SBo
cryptopp-5.6.2-x86_64-1alien
libechonest-2.3.1-x86_64-1_SBo
gst-plugins-ugly-1.6.2-x86_64-1_SBo
```
* result: clementine-1.3.1-x86_64, and deps (builds):
```
# slackpkg install alien:protobuf
# sbopkg -Bki chromaprint
# slackpkg install alien:cryptopp
# sbopkg -Bki libechonest
# sbopkg -Bki gst-plugins-ugly
```
