qbittorrent: qBittorrent (a C++ / Qt4 Bittorrent Client)
qbittorrent:
qbittorrent: The qBittorrent project was created in March 2006 with the idea
qbittorrent: of developing a new Bittorrent client for Linux (and possibly
qbittorrent: other systems) that would be easy to use, good looking, featureful
qbittorrent: but lightweight.
qbittorrent: qBittorrent is a Free Software released under the GNU GPL license.
qbittorrent: Original author is Christophe Dumez.  Currently, qBittorrent is
qbittorrent: being developed by volunteers on their spare time.
qbittorrent:
qbittorrent: Homepage: http://qbittorrent.sourceforge.net/
