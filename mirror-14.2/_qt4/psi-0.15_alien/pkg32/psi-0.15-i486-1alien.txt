psi: psi (A Jabber client for X Window.)
psi:
psi: Psi is the premiere Instant Messaging application designed for
psi: Microsoft Windows, Apple Mac OS X and GNU/Linux. Built upon an open
psi: protocol named Jabber, Psi is a fast and lightweight messaging client
psi: that utilises the best in open source technologies.
psi: Psi contains all the features necessary to chat, with no bloated
psi: extras that slow your computer down.
psi:
psi: Homepage is http://psi-im.org/
psi:
